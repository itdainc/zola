package com.itda.zola.batch.domain.service;

import com.itda.zola.batch.database.*;
import com.itda.zola.batch.domain.model.DailyReport;
import com.itda.zola.domain.model.ledger.Account;
import com.itda.zola.domain.model.ledger.LedgerType;
import com.itda.zola.domain.model.transaction.exchange.Exchange;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by dylan on 2017. 2. 20..
 */
@Slf4j
@Service
public class DailyReportService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private LedgerRepository ledgerRepository;
    @Autowired
    private ExchangeRepository exchangeRepository;
    @Autowired
    private ReserveRepository reserveRepository;

    @Autowired
    private DailyReportRepository dailyReportRepository;

    @Transactional
    public void setDailyReport(String dt) {

        long newUser = userRepository.getDailyNewUser(dt);
        long leaveUser = userRepository.getDailyLeaveUser(dt);
        long totUser = userRepository.getTotMemberCount(dt);

        long signupCash = ledgerRepository.getDailyBonusCash(LedgerType.B, Account.R_JOIN, dt);
        long attendanceCash = ledgerRepository.getDailyBonusCash(LedgerType.B, Account.R_ATT, dt);
        long recommendCash = ledgerRepository.getDailyRecommendBonusCash(LedgerType.B, Account.R_RECM, 50, dt);
        long beRecommendCash = ledgerRepository.getDailyRecommendBonusCash(LedgerType.B, Account.R_RECM, 100, dt);

        long buyRequest = exchangeRepository.getDailyBuyCount(Exchange.Status.REGISTERED, dt);
        long buyComplete = exchangeRepository.getDailyBuyCount(Exchange.Status.SUCCEED, dt);
        long buyCompleteCash = exchangeRepository.getDailyBuyAmount(Exchange.Status.SUCCEED, dt);
        long buyDeny = exchangeRepository.getDailyBuyCount(Exchange.Status.CANCEL, dt);

        long adIgaworks = reserveRepository.getDailyAdAmount(LedgerType.R, 1, dt) * 2;
        long adNasmedia = reserveRepository.getDailyAdAmount(LedgerType.R, 3, dt) * 2;
        long adTnkfactory = reserveRepository.getDailyAdAmount(LedgerType.R, 4, dt) * 2;

        long adUserCount = 0;

        DailyReport report = new DailyReport(dt, newUser, leaveUser, totUser, signupCash, attendanceCash, recommendCash, beRecommendCash, buyRequest+buyComplete, buyComplete, buyCompleteCash, buyDeny, adIgaworks, adNasmedia, adTnkfactory, adUserCount);

        System.out.println("report data " + report.toString());

        dailyReportRepository.save(report);
    }

}

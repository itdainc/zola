package com.itda.zola.batch.tasklet;

import com.itda.zola.batch.domain.service.DailyReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

/**
 * Created by dylan on 2017. 1. 10..
 */
@Component("dailyTime")
public class DailyReport {

    @Autowired
    DailyReportService dailyReportService;

    public void setDailyReport(){

        System.out.println("task start");
        dailyReportService.setDailyReport(LocalDate.now().minusDays(1).toString());
        System.out.println("task end");

    }
}

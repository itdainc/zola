package com.itda.zola.batch.database;

import com.itda.zola.domain.model.ledger.Account;
import com.itda.zola.domain.model.ledger.Ledger;
import com.itda.zola.domain.model.ledger.LedgerType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface LedgerRepository extends JpaRepository<Ledger, Long> {

    @Query("select COALESCE(SUM(l.price), 0) from Ledger l WHERE l.type=?1 AND l.account=?2 AND DATE_FORMAT(l.dt,'%Y-%m-%d') =?3 ")
    Long getDailyBonusCash(LedgerType type, Account account , String dt);

    @Query("select COALESCE(SUM(l.price), 0) from Ledger l WHERE l.type=?1 AND l.account=?2 AND l.price=?3 AND DATE_FORMAT(l.dt,'%Y-%m-%d') =?4 ")
    Long getDailyRecommendBonusCash(LedgerType type, Account account , long price, String dt);

}
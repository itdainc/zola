package com.itda.zola.batch.domain.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by dylan on 2017. 2. 20..
 */
@Getter
@Setter
public class ReportCount {

    private Long count;
}

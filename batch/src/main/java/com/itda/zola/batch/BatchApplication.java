package com.itda.zola.batch;

import com.itda.zola.CoreApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.annotation.EnableScheduling;

@EntityScan(basePackageClasses = CoreApplication.class)
@SpringBootApplication
@EnableScheduling
@ImportResource("/schedule.xml")
public class BatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(BatchApplication.class, args);
	}
}

package com.itda.zola.batch.database;

import com.itda.zola.domain.model.transaction.exchange.Exchange;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ExchangeRepository extends JpaRepository<Exchange, Long> {

    @Query("select count(e)  from Exchange e WHERE e.status =?1 AND DATE_FORMAT(e.registeredAt,'%Y-%m-%d') =?2 ")
    Long getDailyBuyCount(Exchange.Status status, String dt);

    @Query("select COALESCE(SUM(e.amount), 0)  from Exchange e WHERE e.status =?1 AND DATE_FORMAT(e.registeredAt,'%Y-%m-%d') =?2 ")
    Long getDailyBuyAmount(Exchange.Status status, String dt);
}
package com.itda.zola.batch.database;

import com.itda.zola.domain.model.ledger.LedgerType;
import com.itda.zola.domain.model.transaction.reserve.Reserve;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ReserveRepository extends JpaRepository<Reserve, Long> {

    @Query("select COALESCE(SUM(r.amount), 0) from Reserve r INNER JOIN r.history l where l.type=?1 AND cp_id=?2 AND DATE_FORMAT(l.dt,'%Y-%m-%d') =?3 ")
    Long getDailyAdAmount(LedgerType type, int offerwall, String dt );

}
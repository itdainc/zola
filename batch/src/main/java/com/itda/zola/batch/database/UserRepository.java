package com.itda.zola.batch.database;

import com.itda.zola.domain.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

/**
 * Created by dylan on 2016. 12. 23..
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("select count(u) from User u WHERE DATE_FORMAT(u.registeredAt,'%Y-%m-%d') =?1 ")
    Long getDailyNewUser(String dt);

    @Query("select count(u) from User u WHERE DATE_FORMAT(u.expiredAt,'%Y-%m-%d') =?1 ")
    Long getDailyLeaveUser(String dt);

    @Query("select count(u) from User u WHERE u.expiredAt is null AND DATE_FORMAT(u.registeredAt,'%Y-%m-%d') <= ?1")
    Long getTotMemberCount(String dt);

}
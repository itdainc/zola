package com.itda.zola.batch.database;

import com.itda.zola.batch.domain.model.DailyReport;
import com.itda.zola.domain.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Created by dylan on 2016. 12. 23..
 */
@Repository
public interface DailyReportRepository extends JpaRepository<DailyReport, Long> {

}
package com.itda.zola.domain.model.notice;

import com.itda.zola.domain.model.supplier.Supplier;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter

@NoArgsConstructor

@Entity
public class Notice {

    @Id
    @GeneratedValue
    private Long id;

    private String title;
    private String url;

    private LocalDateTime registeredAt;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Supplier supplier;


    public Notice(Supplier supplier, String title, String url) {
        this.supplier = supplier;
        this.title = title;
        this.url = url;

        this.registeredAt = LocalDateTime.now();
    }
}
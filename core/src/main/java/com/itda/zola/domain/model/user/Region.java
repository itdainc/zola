package com.itda.zola.domain.model.user;

import com.itda.zola.domain.model.supplier.Supplier;
import lombok.Getter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Set;

@Getter
@Entity
public class Region {

    @Id
    @GeneratedValue
    private Long id;

    private String code;

    @OneToMany(mappedBy = "region")
    private Set<Supplier> suppliers;
}
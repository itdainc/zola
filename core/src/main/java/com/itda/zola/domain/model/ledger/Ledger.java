package com.itda.zola.domain.model.ledger;

import com.itda.zola.domain.model.supplier.Supplier;
import com.itda.zola.domain.model.user.Region;
import com.itda.zola.domain.model.user.User;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Getter

@Entity
public class Ledger {

    @Id
    @GeneratedValue
    private Long id;

    private LocalDateTime dt;

    @Enumerated(EnumType.STRING)
    private LedgerType type;
    @Enumerated(EnumType.STRING)
    private Account account;

    private Long userId;
    private Long regionId;
    private Long supplierId;

    private Long amount;
    private Long price;

    private String memo;

//    private Long revenueId;

    private Ledger(LedgerType type, User user, Supplier supplier, Account account, long amount, long price, String memo) {

        this.dt = LocalDateTime.now();

        this.type = type;
        this.account = account;

        this.userId = user.getId();
        this.regionId = supplier.getRegion().getId();
        this.supplierId = supplier.getId();

        this.amount = amount;
        this.price = price;

        this.memo = memo;
    }

    public static Ledger reserve(User user, Supplier supplier, Account account, long amount, String memo) {
        return new Ledger(LedgerType.R, user, supplier, account, amount, amount, memo);
    }

    public static Ledger bonus(User user, Supplier supplier, Account account, long amount, String memo) {
        return new Ledger(LedgerType.B, user, supplier, account, amount, amount, memo);
    }

    public static Ledger exchange(User user, Supplier supplier, Account account, long amount, String memo) {
        return new Ledger(LedgerType.X, user, supplier, account, amount, -amount, memo);
    }

    public static Ledger cancelExchange(User user, Supplier supplier, Account account, long amount, String memo) {
        return new Ledger(LedgerType.C, user, supplier, account, amount, amount, memo);
    }


    // ----------
    // Getter - impl by annotation

    // ----------
    // Setter

    public void setMemo(String memo) {
        this.memo = memo;
    }
}
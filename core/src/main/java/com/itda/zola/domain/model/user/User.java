package com.itda.zola.domain.model.user;

import com.google.common.base.Charsets;
import com.google.common.base.MoreObjects;
import com.google.common.base.Strings;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@NoArgsConstructor(access = AccessLevel.PACKAGE)

@Entity
public class User {

    public static final int REJOIN_INTERVAL_DAYS = 30;

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @Embedded
    private ProviderToken providerToken;
    private String hash;  // NEVER UPDATE THIS VALUE

    private String firebaseToken;

    private LocalDateTime registeredAt, expiredAt;

    @ManyToOne(fetch = FetchType.LAZY)
    private User referrer;


    public User(ProviderToken providerToken, String name) {

        if(Strings.isNullOrEmpty(name)) {

        }
        this.name = name;

        this.providerToken = providerToken;
        this.hash = generateHash(providerToken);

        this.registeredAt = LocalDateTime.now();
    }

    public User(ProviderToken providerToken, String name, User referrer) {

        this(providerToken, name);
        this.referrer = referrer;
    }


    // ----------

    public void leave() {
        this.expiredAt = LocalDateTime.now();
    }


    // ----------

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public ProviderToken getProviderToken() {
        return providerToken;
    }

    public String getHash() {
        return hash;
    }

    public String getFirebaseToken() {
        return firebaseToken;
    }

    public LocalDateTime getRegisteredAt() {
        return registeredAt;
    }

    public boolean hasExpired() {
        return expiredAt != null;
    }

    public LocalDateTime getExpiredAt() {
        return expiredAt;
    }

    public User getReferrer() {
        return referrer;
    }


    // ----------

    public void setFirebaseToken(String firebaseToken) {
        this.firebaseToken = firebaseToken;
    }


    // ----------

    private static String generateHash(ProviderToken token) {
        HashCode hc = Hashing.md5().newHasher().putString(token.toString(), Charsets.UTF_8).hash();
        return hc.toString();
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("providerToken", providerToken)
                .add("hash", hash)
                .add("registeredAt", registeredAt)
                .add("expiredAt", expiredAt)
                .toString();
    }
}
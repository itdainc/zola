package com.itda.zola.domain.model.promotion;

public enum PromotionType {

    JOIN("회원가입"), ATTENDANCE("출석"), REFERRER("추천인");

    private String desc;

    PromotionType(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}
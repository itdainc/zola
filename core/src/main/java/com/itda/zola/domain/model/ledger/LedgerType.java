package com.itda.zola.domain.model.ledger;

public enum LedgerType {
    R("광고"),
    B("보너스"),
    W("적립 취소"),
    X("구매"),
    C("구매 취소");

    private String desc;

    LedgerType(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}
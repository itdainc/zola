package com.itda.zola.domain.model.cp;

import com.google.common.base.MoreObjects;
import lombok.Getter;
import lombok.ToString;
import org.hibernate.annotations.ColumnTransformer;

import javax.persistence.*;
import java.util.List;

@Getter

@Entity
public class Cp {

    @Id
    @GeneratedValue
    private Long id;

    private String code;

    @ElementCollection
    @CollectionTable(name = "cp_accept_ip")
    @ColumnTransformer(read = "INET_NTOA(ip)", write = "INET_ATON(?)")
    private List<String> acceptIps;


    public Long getId() {
        return id;

    }

    public String getCode() {
        return code;
    }

    public boolean isAccessible(String ip) {
        return acceptIps.contains(ip);
    }


    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("code", code)
                .toString();
    }
}
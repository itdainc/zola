package com.itda.zola.domain.model;

import com.itda.zola.domain.model.ledger.LedgerType;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class Total {

    private LedgerType type;
    private long amount;

    public Total(LedgerType type, long amount) {
        this.type = type;
        this.amount = amount;
    }
}
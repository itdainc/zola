package com.itda.zola.domain.model.supplier;

import com.itda.zola.domain.model.promotion.Promotion;
import com.itda.zola.domain.model.user.Region;
import lombok.Getter;

import javax.persistence.*;
import java.util.List;

@Getter

@Entity
public class Supplier {

    @Id
    @GeneratedValue
    private Long id;

    private String code;

    @ManyToOne(optional = false)
    private Region region;

    @OneToMany(mappedBy = "supplier")
    private List<Promotion> promotions;

    @OneToMany(mappedBy = "supplier", cascade = CascadeType.PERSIST)
    private List<SupplierRelease> releases;
}
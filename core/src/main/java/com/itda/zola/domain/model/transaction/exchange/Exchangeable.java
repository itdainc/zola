package com.itda.zola.domain.model.transaction.exchange;

public interface Exchangeable {

    Type getType();

    String getName();

    Long getPrice();

    enum Type {
        KAKAO_EMO
    }
}
package com.itda.zola.domain.model.report;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by dylan on 2017. 2. 20..
 */
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Entity
@Getter
public class DailyReport {

    @Id
    @GeneratedValue
    private Long id;

    private String dt;
    private long memberNew;
    private long memberLeave;
    private long memberTotal;

    private long bonusSignup;
    private long bonusAttendance;
    private long bonusRecommend;
    private long bonusBerecommended;

    private long buyRequest;
    private long buyComplete;
    private long buyCompletecash;
    private long buyDeny;

    private long adIgaworks;
    private long adNasmedia;
    private long adTnkfactory;
    private long adUserCount;

    public DailyReport(String dt, long memberNew, long memberLeave, long memberTotal, long bonusSignup, long bonusAttendance, long bonusRecommend, long bonusBerecommended, long buyRequest, long buyComplete, long buyCompletecash, long buyDeny, long adIgaworks, long adNasmedia, long adTnkfactory, long adUserCount) {
        this.dt = dt;
        this.memberNew = memberNew;
        this.memberLeave = memberLeave;
        this.memberTotal = memberTotal;
        this.bonusSignup = bonusSignup;
        this.bonusAttendance = bonusAttendance;
        this.bonusRecommend = bonusRecommend;
        this.bonusBerecommended = bonusBerecommended;
        this.buyRequest = buyRequest;
        this.buyComplete = buyComplete;
        this.buyCompletecash = buyCompletecash;
        this.buyDeny = buyDeny;
        this.adIgaworks = adIgaworks;
        this.adNasmedia = adNasmedia;
        this.adTnkfactory = adTnkfactory;
        this.adUserCount = adUserCount;
    }
}
package com.itda.zola.domain.service;

import com.itda.zola.domain.model.transaction.exchange.Exchangeable;

public interface ExchangeableParser {

    Exchangeable parse(String key);

    boolean support(String url);
}
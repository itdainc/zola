package com.itda.zola.domain.model.promotion;

import com.google.common.base.MoreObjects;
import com.itda.zola.domain.model.supplier.Supplier;
import lombok.Getter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter

@Entity
//@DiscriminatorColumn(name = "type")
//@Inheritance
public class Promotion {

    @Id
    @GeneratedValue
    private Long id;

    private LocalDateTime since, until;

    @Enumerated(EnumType.STRING)
    private PromotionType type;

    private Long amount;

    private Long amount2;

    @ManyToOne(optional = false)
    private Supplier supplier;

    public void setSince(LocalDateTime since) {
        this.since = since;
    }

    public void setUntil(LocalDateTime until) {
        this.until = until;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public void setAmount2(Long amount2) {
        this.amount2 = amount2;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("since", since)
                .add("until", until)
                .add("type", type)
                .add("amount", amount)
                .toString();
    }
}
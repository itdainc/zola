package com.itda.zola.domain.model.transaction.reserve;

import com.itda.zola.domain.model.ledger.Account;

public interface ReserveParams {

    Long getAmount();

    Long getUserId();

    String getSupplierCode();

    Account getAccount();

    String getTransactionId();

    String getMemo();
}
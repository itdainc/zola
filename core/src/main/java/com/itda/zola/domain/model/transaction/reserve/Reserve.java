package com.itda.zola.domain.model.transaction.reserve;

import com.itda.zola.domain.model.cp.Cp;
import com.itda.zola.domain.model.ledger.Account;
import com.itda.zola.domain.model.ledger.Ledger;
import com.itda.zola.domain.model.supplier.Supplier;
import com.itda.zola.domain.model.user.Region;
import com.itda.zola.domain.model.user.User;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Getter

@Entity
public class Reserve {

    @Id
    @GeneratedValue
    private Long id;

    private Long amount;

    @ManyToOne
    private Cp cp;
    private String transactionId;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private User user;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Region region;

    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "revenue_id")
    private List<Ledger> history = new ArrayList<>();


    private Reserve(Cp cp, String clientSeq, User user, Supplier supplier, Account account, Long amount, String memo, boolean bonus) {
        this.amount = amount;
        this.cp = cp;
        this.user = user;
        this.region = supplier.getRegion();
        this.transactionId = clientSeq;

        if (bonus) {
            this.history.add(Ledger.bonus(user, supplier, account, amount, memo));
        } else {
            this.history.add(Ledger.reserve(user, supplier, account, amount, memo));
        }

    }

    public static Reserve revenueShare(Cp cp, String clientSeq, User user, Supplier supplier, Account account, Long amount, String memo) {
        return new Reserve(cp, clientSeq, user, supplier, account, amount, memo, false);
    }

    public static Reserve bonus(Cp cp, String clientSeq, User user, Supplier supplier, Account account, Long amount, String memo) {
        return new Reserve(cp, clientSeq, user, supplier, account, amount, memo, true);
    }

    public void cancel() {

    }


    public Example<Reserve> getUniqueCheckExample() {
        return Example.of(this,
                ExampleMatcher.matching().withIgnorePaths("amount", "user", "region"));
    }

    // ----------
    // Getter - impl by annotation
}
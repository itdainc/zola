package com.itda.zola.domain.model.ledger;

public enum Account {

    R_RS("광고"), R_ATT("출석"), R_RECM("추천인"), R_JOIN("회원가입"), R_MAN("수기적립"), X_BUY("지급"), C_FAIL("실패");

    private final String desc;

    Account(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}
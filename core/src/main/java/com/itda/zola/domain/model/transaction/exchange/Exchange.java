package com.itda.zola.domain.model.transaction.exchange;

import com.itda.zola.domain.model.ledger.Account;
import com.itda.zola.domain.model.ledger.Ledger;
import com.itda.zola.domain.model.supplier.Supplier;
import com.itda.zola.domain.model.user.Region;
import com.itda.zola.domain.model.user.User;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Getter

@Entity
public class Exchange {

    @Id
    @GeneratedValue
    private Long id;

    // Exchangeable
    private String name;
    private Long amount;
    private String url;

    private String receiver;

    private Status status;

    private LocalDateTime registeredAt, sentDate;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private User user;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Region region;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Supplier supplier;

    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "revenue_id")
    private List<Ledger> history = new ArrayList<>();


    public Exchange(Supplier supplier, User user, Region region, Exchangeable exchangeable, String receiver, String url) {
        this.supplier = supplier;

        this.user = user;
        this.region = region;

        this.name = exchangeable.getName();
        this.amount = exchangeable.getPrice();

        this.receiver = receiver;
        this.url = url;

        this.status = Status.REGISTERED;

        this.history.add(Ledger.exchange(user, supplier, Account.X_BUY, exchangeable.getPrice(), exchangeable.getName() + " - 구매"));

        this.registeredAt = LocalDateTime.now();
    }

    public void done() {

        this.status = Status.SUCCEED;
        this.sentDate = LocalDateTime.now();
    }

    public void failure(String memo) {

        this.status = Status.FAILURE;

        this.history.add(Ledger.cancelExchange(user, supplier, Account.C_FAIL, amount, memo));
    }

    public void reject(String memo) {

        this.status = Status.CANCEL;
        this.history.add(Ledger.cancelExchange(user, supplier, Account.C_FAIL, amount, memo));
    }

    public enum Status {

        REGISTERED, SUCCEED, FAILURE, CANCEL
    }
}
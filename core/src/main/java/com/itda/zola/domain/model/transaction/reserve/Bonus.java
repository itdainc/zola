package com.itda.zola.domain.model.transaction.reserve;

import com.itda.zola.domain.model.cp.Cp;
import com.itda.zola.domain.model.ledger.Account;
import com.itda.zola.domain.model.ledger.Ledger;
import com.itda.zola.domain.model.supplier.Supplier;
import com.itda.zola.domain.model.user.Region;
import com.itda.zola.domain.model.user.User;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor

@Entity
public class Bonus {

    @Id
    @GeneratedValue
    private Long id;

    private Long amount;

    @ManyToOne
    private Cp cp;
    private String transactionId;

    @ManyToOne
    private User user;

    @ManyToOne
    private Region region;

    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "revenue_id")
    private List<Ledger> history = new ArrayList<>();


    public Bonus(Cp cp, String clientSeq, User user, Supplier supplier, Account account, Long amount, String memo) {
        this.amount = amount;
        this.cp = cp;
        this.user = user;
        this.region = supplier.getRegion();
        this.transactionId = clientSeq;

        this.history.add(Ledger.bonus(user, supplier, account, amount, memo));
    }


    public Long getId() {
        return id;
    }

    public Long getAmount() {
        return amount;
    }

    public Cp getCp() {
        return cp;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public User getUser() {
        return user;
    }

    public Region getRegion() {
        return region;
    }

    public List<Ledger> getHistory() {
        return history;
    }
}

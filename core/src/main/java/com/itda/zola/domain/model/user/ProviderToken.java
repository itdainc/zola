package com.itda.zola.domain.model.user;

import com.google.common.base.MoreObjects;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter
@Embeddable
public class ProviderToken {

    @Enumerated(EnumType.STRING)
    private Provider provider;
    private String providedId;


    public ProviderToken(Provider provider, String providedId) {
        this.provider = provider;
        this.providedId = providedId;
    }


    // ----------

    public Provider getProvider() {
        return provider;
    }

    public String getProvidedId() {
        return providedId;
    }


    // ----------

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("provider", provider.name())
                .add("providedId", providedId)
                .toString();
    }
}
package com.itda.zola.domain.model.supplier;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Setter
@Getter

@Entity
public class SupplierRelease {

    @Id
    @GeneratedValue
    private Long id;

    private Integer version;
    private boolean mandatory;

    private LocalDateTime releasedAt;

    private String memo;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Supplier supplier;
}
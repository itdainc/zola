package com.itda.zola.admin.external;

public interface NotificationBroker {

    void send(Notification notification);
}
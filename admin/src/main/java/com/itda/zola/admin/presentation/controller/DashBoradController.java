package com.itda.zola.admin.presentation.controller;

import com.itda.zola.admin.domain.service.ExchangeService;
import com.itda.zola.admin.domain.service.ReserveService;
import com.itda.zola.admin.domain.service.UserService;
import com.itda.zola.admin.presentation.response.Response;
import com.itda.zola.admin.presentation.response.dto.DashBoardResponse;
import com.itda.zola.domain.model.transaction.exchange.Exchange;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by dylan on 2017. 1. 25..
 */
@Slf4j
@RestController
@RequestMapping("/admin")
public class DashBoradController {

    @Autowired
    UserService userService;
    @Autowired
    ExchangeService exchangeService;
    @Autowired
    ReserveService reserveService;

    @RequestMapping("/dashboard/total")
    public Response count() {

        log.info("find DashBoard");
        DashBoardResponse response = new DashBoardResponse();

        response.setActiveUserCount(userService.getActiveUserCount());
        response.setExpireUserCount(userService.getExpireUserCount());
        response.setExchangeCount(exchangeService.getCount());
        response.setSuccessCount(exchangeService.getCount(Exchange.Status.SUCCEED));
        response.setRejectCount(exchangeService.getCount(Exchange.Status.CANCEL));
        response.setReserveCount(reserveService.getCount());

        return Response.of().addObject(response);
    }
}
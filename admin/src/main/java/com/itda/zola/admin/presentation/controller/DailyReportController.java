package com.itda.zola.admin.presentation.controller;

import com.itda.zola.admin.domain.entity.PromotionCreateRequest;
import com.itda.zola.admin.domain.service.DailyReportService;
import com.itda.zola.admin.domain.service.PromotionService;
import com.itda.zola.admin.domain.service.SupplierService;
import com.itda.zola.admin.presentation.response.Response;
import com.itda.zola.domain.model.promotion.Promotion;
import com.itda.zola.domain.model.report.DailyReport;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dylan on 2016. 12. 28..
 */
@Slf4j
@RestController
@RequestMapping("/admin")
public class DailyReportController {

    @Autowired
    DailyReportService dailyReportService;

    @RequestMapping(value = "/report", method = RequestMethod.GET)
    public Response find(@PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC, size = 5) Pageable pageable) {

        log.info("report find");

        Page<DailyReport> page = dailyReportService.findBy(pageable);

        log.info("report size={}", page.getTotalElements());
        return Response.of().addObject(page);
    }

    @RequestMapping(value = "/report/count", method = RequestMethod.GET)
    public Response count() {
        Map<String, Object> r = new HashMap<>();

        r.put("report", dailyReportService.getCount());

        return Response.of().addObject(r);
    }

    @RequestMapping(value = "/report/cp-pie/csv", method = RequestMethod.GET)
    public String pieData(@PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC, size = 5) Pageable pageable) {

        StringBuilder csv = new StringBuilder();
        csv.append("Date,IGA,NAS,TNK\n");

        Page<DailyReport> page = dailyReportService.findBy(pageable);
        for (DailyReport day : page) {
            csv.append(day.getDt()).append(",").append(day.getAdIgaworks()).append(",").append(day.getAdNasmedia()).append(",").append(day.getAdTnkfactory()).append("\n");
        }

        return csv.toString();
    }
}

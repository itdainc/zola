package com.itda.zola.admin.infrastructure.auth;

import com.itda.zola.admin.domain.model.AdminUser;
import com.itda.zola.admin.domain.service.AdminUserService;
import com.itda.zola.admin.infrastructure.exception.resource.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class AdminUserDetailService implements UserDetailsService {

    @Autowired
    private AdminUserService adminUserService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {

            System.out.println("loadUserByUsername :::> " + username );
            AdminUser user = adminUserService.findBy(username);
            return AdminUserPrincipal.of(user);
        } catch (ResourceNotFoundException e) {
            throw new UsernameNotFoundException(e.getMessage());
        }
    }
}
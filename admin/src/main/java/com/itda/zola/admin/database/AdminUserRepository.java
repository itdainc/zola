package com.itda.zola.admin.database;

import com.itda.zola.admin.domain.model.AdminUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by dylan on 2016. 12. 23..
 */
@RepositoryRestResource(collectionResourceRel = "adminuser", path = "adminusers")
public interface AdminUserRepository extends JpaRepository<AdminUser, String> {


}

package com.itda.zola.admin.domain.entity;

import lombok.Data;

/**
 * Created by dylan on 2016. 12. 12..
 */
@Data
public class AdminUserCreateRequest {
    private String username;
    private String password;
    private String adminRole;
}

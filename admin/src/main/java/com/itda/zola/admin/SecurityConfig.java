package com.itda.zola.admin;

import com.google.common.collect.ImmutableList;
import com.itda.zola.admin.infrastructure.auth.AdminAuthenticationEntryPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.ldap.DefaultSpringSecurityContextSource;
import org.springframework.security.ldap.authentication.BindAuthenticator;
import org.springframework.security.ldap.authentication.LdapAuthenticationProvider;
import org.springframework.security.ldap.search.FilterBasedLdapUserSearch;
import org.springframework.security.ldap.search.LdapUserSearch;
import org.springframework.security.ldap.userdetails.LdapAuthoritiesPopulator;
import org.springframework.security.ldap.userdetails.LdapUserDetailsService;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.sql.DataSource;

@Configuration
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    @Override
    public void configure(WebSecurity web) throws Exception {

        web.ignoring().antMatchers("/webjars/**", "/views/**", "/policy/**", "/health_check.html", "/v2/api-docs", "/configuration/ui", "/swagger-resources", "/configuration/security", "/swagger-ui.html");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        // basic
        http.httpBasic().authenticationEntryPoint(new AdminAuthenticationEntryPoint());

        // csrf disabled
        http.csrf().disable();

        // request matcher
        http.authorizeRequests()
                .antMatchers("/", "/css/**", "/js/**", "/img/**", "/fonts/**", "/index.html", "/login.html").permitAll()
                .antMatchers("/admin/**").hasAnyRole("ADMIN")
                .anyRequest().authenticated();

        // remember
        http.rememberMe()
                .rememberMeParameter("remember")
                .userDetailsService(getUserDetailsService())
                .tokenRepository(persistentTokenRepository());

        // logout
        http.logout().logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler(HttpStatus.NO_CONTENT));
    }

    //
    private PersistentTokenRepository persistentTokenRepository() {
        JdbcTokenRepositoryImpl tokenRepositoryImpl = new JdbcTokenRepositoryImpl();
        tokenRepositoryImpl.setDataSource(dataSource);
        return tokenRepositoryImpl;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder builder) throws Exception {

        // Search options
        LdapUserSearch search = new FilterBasedLdapUserSearch("ou=users", "(uid={0})", contextSource());

        // Authenticator
        BindAuthenticator authenticator = new BindAuthenticator(contextSource());
        authenticator.setUserSearch(search);

        // Provider
        AuthenticationProvider provider = new LdapAuthenticationProvider(authenticator, populator());

        builder.authenticationProvider(provider);
    }

    private UserDetailsService getUserDetailsService() {
        return new LdapUserDetailsService(new FilterBasedLdapUserSearch("ou=users", "(uid={0})", contextSource()), populator());
    }

    private LdapAuthoritiesPopulator populator() {

        return (userData, username) -> ImmutableList.of(new SimpleGrantedAuthority("ROLE_ADMIN"));
    }

    @Bean
    public DefaultSpringSecurityContextSource contextSource() {

        DefaultSpringSecurityContextSource source = new DefaultSpringSecurityContextSource("ldap://ldap.itda.co.kr");
        source.setBase("dc=itda,dc=com");

        return source;
    }
}
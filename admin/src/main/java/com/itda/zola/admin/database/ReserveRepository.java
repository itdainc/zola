package com.itda.zola.admin.database;

import com.itda.zola.domain.model.transaction.reserve.Reserve;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by dylan on 2016. 12. 23..
 */
public interface ReserveRepository extends JpaRepository<Reserve, Long> {


}

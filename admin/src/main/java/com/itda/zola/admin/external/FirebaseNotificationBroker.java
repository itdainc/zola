package com.itda.zola.admin.external;

import com.itda.zola.admin.infrastructure.util.JaxRsClientLoggingFilter;
import com.itda.zola.domain.model.user.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j

@Component
public class FirebaseNotificationBroker implements NotificationBroker {

    private Client client;
    private WebTarget target;

    @PostConstruct
    private void init() {
        client = ClientBuilder.newClient().register(JaxRsClientLoggingFilter.class);
        target = client.target("https://fcm.googleapis.com/fcm/send");
    }

    @PreDestroy
    private void destroy() {
        client.close();
    }

    public void send(Notification notification) {

        FirebaseNotification fn = new FirebaseNotification(notification.getTitle(), notification.getBody());

        FirebaseMessage message = new FirebaseMessage(fn);

        message.registration_ids = notification.getTo().parallelStream().map(User::getFirebaseToken).collect(Collectors.toList());

        String res = target.request(MediaType.APPLICATION_JSON_TYPE)
                .header("Authorization", "key=AAAApjdhgl0:APA91bEKlWeQovl_FYfkCl7B5wW3GAFQFBDQGDfJWOnr1wOUdtQbW9HD6RWsI0YOF1sS_266VZ2pERfb-mg2NWTIfMVtOBonOLSbRr77pRSOpOqphCoAcQosMzGBbjKVTPk4XgveNF3D")
                .post(Entity.entity(message, MediaType.APPLICATION_JSON_TYPE), String.class);

        log.info(res);
    }


    @ToString
    @Getter
    private static class FirebaseMessage {

        private String to;
        private List<String> registration_ids;

        private FirebaseNotification notification;

        private FirebaseMessage(FirebaseNotification notification) {
            this.notification = notification;
        }
    }


    @ToString
    @Getter
    @AllArgsConstructor
    private static class FirebaseNotification {

        private String title;
        private String body;
    }

}

package com.itda.zola.admin.domain.entity;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * Created by dylan on 2016. 12. 12..
 */
@Data
public class PromotionCreateRequest {

    private LocalDateTime since;
    private LocalDateTime until;
    private Long amount;
    private Long amount2;
}

package com.itda.zola.admin.presentation.response.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by dylan on 2017. 1. 25..
 */
@Getter
@Setter
public class DashBoardResponse {

    private long activeUserCount;
    private long expireUserCount;
    private long exchangeCount;
    private long successCount;
    private long rejectCount;
    private long reserveCount;

}

package com.itda.zola.admin.infrastructure.exception.logical;

public class BadArgumentException extends AbstractLogicalException {

    public BadArgumentException(String message) {
        super("01", message);
    }

    public BadArgumentException(String message, Throwable cause) {
        super("01", message, cause);
    }
}
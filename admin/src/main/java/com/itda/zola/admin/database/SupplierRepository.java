package com.itda.zola.admin.database;

import com.itda.zola.domain.model.supplier.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SupplierRepository extends JpaRepository<Supplier, Long>{
    Supplier findByCode(String code);
}

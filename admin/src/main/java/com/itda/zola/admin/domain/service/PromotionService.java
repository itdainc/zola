package com.itda.zola.admin.domain.service;

import com.itda.zola.admin.database.PromotionRepository;
import com.itda.zola.admin.infrastructure.exception.resource.ResourceNotFoundException;
import com.itda.zola.domain.model.promotion.Promotion;
import com.itda.zola.domain.model.supplier.Supplier;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class PromotionService {

    private PromotionRepository promotionRepository;

    @Autowired
    public PromotionService(PromotionRepository promotionRepository) {
        this.promotionRepository = promotionRepository;
    }

    public Page<Promotion> findBy(Supplier supplier, Pageable pageable) {

        return promotionRepository.findBySupplier(supplier, pageable);
    }

    public long getCount() {

        return promotionRepository.count();
    }

    @Transactional
    public Promotion update(Promotion promotion) {

        log.debug("promotion update = {}", promotion.toString());
        return promotionRepository.save(promotion);
    }

    public Promotion findBy(long id) {

        log.info("findBy promotion by id({})", id);

        Promotion promotion = promotionRepository.findOne(id);

        if (promotion == null) {
            log.error("promotion cannot be found. id = {}", id);
            throw new ResourceNotFoundException("요청 정보를 찾을 수 없습니다. id = " + id);
        }

        log.info("promotion = {}", promotion.toString());

        return promotion;
    }
}
package com.itda.zola.admin.external;

import com.google.common.collect.ImmutableList;
import com.itda.zola.domain.model.user.User;
import lombok.Getter;

import java.util.List;


@Getter
public class Notification {

    private final String title;
    private final String body;

    private List<User> to;

    public Notification(String title, String body) {
        this.title = title;
        this.body = body;
    }

    public void to(List<User> users) {
        this.to = users;
    }

    public void to(User user) {
        this.to = ImmutableList.of(user);
    }

}
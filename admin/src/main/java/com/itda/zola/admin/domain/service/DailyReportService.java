package com.itda.zola.admin.domain.service;

import com.itda.zola.admin.database.DailyReportRepository;
import com.itda.zola.domain.model.report.DailyReport;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class DailyReportService {

    private DailyReportRepository dailyReportRepository;

    @Autowired
    public DailyReportService(DailyReportRepository dailyReportRepository) {
        this.dailyReportRepository = dailyReportRepository;
    }

    public Page<DailyReport> findBy(Pageable pageable) {

        return dailyReportRepository.findAll(pageable);
    }

    public long getCount() {

        return dailyReportRepository.count();
    }
}

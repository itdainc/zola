package com.itda.zola.admin.domain.service;

import com.itda.zola.admin.database.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class UserService {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public long getActiveUserCount() {

        return userRepository.countByExpiredAtIsNull();
    }

    public long getExpireUserCount() {

        return userRepository.countByExpiredAtIsNotNull();
    }
}

package com.itda.zola.admin.presentation.controller;

import com.itda.zola.admin.domain.entity.ExchangeCreateRequest;
import com.itda.zola.admin.domain.service.ExchangeService;
import com.itda.zola.admin.external.Notification;
import com.itda.zola.admin.external.NotificationBroker;
import com.itda.zola.admin.presentation.response.Response;
import com.itda.zola.admin.response.dto.ExchangeResponse;
import com.itda.zola.domain.model.transaction.exchange.Exchange;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dylan on 2016. 12. 28..
 */
@Slf4j
@RestController
@RequestMapping("/admin")
@Api(value = "exchange admin ", description = "exchange swagger")
public class ExchangeController {

    @Autowired
    ExchangeService exchangeService;
    @Autowired
    private NotificationBroker notificationBroker;

    @RequestMapping(value = "/exchanges", method = RequestMethod.GET)
    public Response find(@RequestParam(required = false, defaultValue = "") Exchange.Status status,
                         @RequestParam(required = false, defaultValue = "") String type,
                         @RequestParam(required = false, defaultValue = "") String value,
                         @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC, size = 5) Pageable pageable) {

        log.info("find exchanges begin");

        String name = "";
        String receiver = "";
        String username = "";

        if(type.equals("1")) {
             name = value;
        } else if(type.equals("2")) {
            receiver = value;
        } else if(type.equals("3")) {
            username = value;
        }

        Page<Exchange> page = exchangeService.findBy(status, name, receiver, username, pageable);

        return Response.of().addObject(page);
    }

    @RequestMapping(value = "/exchanges/count", method = RequestMethod.GET)
    public Response count(@RequestParam(required = false, defaultValue = "") Exchange.Status status) {
        Map<String, Object> r = new HashMap<>();

        r.put("exchangesCount", exchangeService.getCount(status));

        return Response.of().addObject(r);
    }

    @RequestMapping(value = "/exchanges/{id:\\d+}", method = RequestMethod.GET)
    public Response findOne(@PathVariable long id) {

        Exchange exchange = exchangeService.findBy(id);

        return Response.of().addObject(exchange);
    }

    @RequestMapping(value = "/exchanges/{id:\\d+}", method = RequestMethod.POST)
    public Response done(@PathVariable Long id) {

        Exchange exchange = exchangeService.findBy(id);

        exchangeService.done(exchange);

        return Response.of().addObject(exchange.getId());
    }

    @RequestMapping(value = "/exchanges/reject/{id:\\d+}", method = RequestMethod.POST)
    public Response reject(@PathVariable Long id, @RequestBody ExchangeCreateRequest request) {

        Exchange exchange = exchangeService.findBy(id);

        String message = "";
        String data = "";

        if(request.getValue().equals("1")) {
            message = "「"+exchange.getName()+"」 은 "+exchange.getReceiver()+"님께서 이미 보유중인 이모티콘으로 구매 신청이 취소되었습니다";
            data = exchange.getName()+" - 구매취소 (이미 보유 중 이모티콘)";
        } else if(request.getValue().equals("2")) {
            message = "「"+exchange.getName()+"」 은 "+exchange.getReceiver()+"님의 휴대폰에서 지원하지 않는 이모티콘으로, 구매 신청이 취소되었습니다";
            data = exchange.getName()+" - 구매취소 (기기에서 지원하지 않는 이모티콘)";
        } else if(request.getValue().equals("3")) {
            message = exchange.getReceiver()+" 번호는 카카오톡에 미 등록된 휴대폰번호로 "+ "「"+exchange.getName()+"」 구매 신청이 취소되었습니다";
            data = exchange.getName()+" - 구매취소 (카카오톡에 미 등록된 휴대폰번호)";
        } else if(request.getValue().equals("4")) {
            message = "카카오톡의 이모티콘 서비스 오류, 기능/정책 변경으로「" + exchange.getName() + "」 을 선물할 수 없어,구매 신청이 취소되었습니다";
            data = exchange.getName()+" - 구매취소 (카톡 이모티콘 오류/정책 변경)";
        } else {
            message = "「"+exchange.getName()+"」 구매취소 ("+request.getMessage()+")";
            data = exchange.getName()+" - 구매취소 ("+request.getMessage()+")";
        }

        exchangeService.reject(exchange, data);

        try {
                Notification notification = new Notification("이모티콘 구매가 반려되었습니다", message);
                notification.to(exchange.getUser());
                notificationBroker.send(notification);
        } catch (Exception e) {
            log.error("Notificatoin Error" + id + " user : " + exchange.getUser().getName() );
        }

        return Response.of().addObject(exchange.getId());
    }
}

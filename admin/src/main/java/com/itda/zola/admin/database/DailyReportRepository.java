package com.itda.zola.admin.database;

import com.itda.zola.domain.model.report.DailyReport;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DailyReportRepository extends JpaRepository<DailyReport, Long> {

    Page<DailyReport> findAll(Pageable pageable);
}

package com.itda.zola.admin.domain.model;

import com.google.common.base.Strings;
import com.itda.zola.admin.infrastructure.exception.logical.BadArgumentException;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.regex.Pattern;

/**
 * Created by dylan on 2016. 12. 23..
 */
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@ToString(exclude = {"password"})
@Getter
@Entity
@DynamicUpdate
public class AdminUser {

    private static final Pattern PATTERN_USERNAME = Pattern.compile("[a-z\\d_-]{5,20}");
    private static final Pattern PATTERN_PASSWORD = Pattern.compile("(?=.*\\d)(?=.*[a-zA-Z]).{6,16}");

    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();

    @Id
    private String username;
    private String password;
    private String adminRole;
    private int failureCount =0;

    private LocalDateTime currentAccessAt;
    private LocalDateTime recentAccessAt;

    public AdminUser(String username, String password) {

        if (Strings.isNullOrEmpty(username) || Strings.isNullOrEmpty(password)) {
            throw new BadArgumentException("username, password 는 필수값 입니다.");
        }

        this.username = username;
//        this.password = password;
        this.password = ENCODER.encode(password);
    }

    public static AdminUser of(String username, String password) {
        return new AdminUser(username, password);
    }

    public void badCredentials() {
        failureCount++;
    }

    public void authenticationSuccess() {
        failureCount = 0;
        recentAccessAt = currentAccessAt;
        currentAccessAt = LocalDateTime.now();
    }
}

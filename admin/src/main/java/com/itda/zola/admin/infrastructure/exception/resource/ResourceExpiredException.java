package com.itda.zola.admin.infrastructure.exception.resource;

public class ResourceExpiredException extends AbstractResourceException {

    public ResourceExpiredException(String message) {
        super("02", message);
    }

    public ResourceExpiredException(String message, Throwable cause) {
        super("02", message, cause);
    }
}
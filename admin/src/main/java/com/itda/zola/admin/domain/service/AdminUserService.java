package com.itda.zola.admin.domain.service;

import com.itda.zola.admin.database.AdminUserRepository;
import com.itda.zola.admin.domain.model.AdminUser;
import com.itda.zola.admin.infrastructure.exception.resource.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Service;

import javax.persistence.LockModeType;
import javax.transaction.Transactional;

@Slf4j
@Service
public class AdminUserService {

    @Autowired
    private AdminUserRepository adminUserRepository;


    @Transactional
    public AdminUser create(AdminUser user) {

        log.debug("user.info ={}", user.toString() );

        return adminUserRepository.save(user);
    }

    public AdminUser findBy(String username) {

        log.debug("find admin by username({})", username);

        AdminUser user = adminUserRepository.findOne(username);

        if (user == null) {
            log.error("admin cannot be found. username = {}", username);
            throw new ResourceNotFoundException("유저 정보를 찾을 수 없습니다.");
        }

        log.debug("user = {}", user.toString());

        return user;
    }

    @Lock(LockModeType.PESSIMISTIC_READ)
    @Transactional
    public void recordBadCredentials(String username) {

        AdminUser user = findBy(username);

        // TODO usernameNotFound 를 캐치할수 있으면 select 안날려도 됨. hideUserNotFoundExceptions 프로퍼티 설정법 찾아보기
        if (user != null) {
            user.badCredentials();
        }
    }

    @Lock(LockModeType.PESSIMISTIC_READ)
    @Transactional
    public void recordAuthenticationSuccess(String username) {

        AdminUser user = findBy(username);

        user.authenticationSuccess();
    }

    public boolean exists(String username) {
        return adminUserRepository.exists(username);
    }
}

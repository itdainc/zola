package com.itda.zola.admin.infrastructure.exception.resource;

import com.itda.zola.admin.infrastructure.exception.ZolaException;

public abstract class AbstractResourceException extends ZolaException {

    AbstractResourceException(String code, String message) {
        super(Series.R, code, message);
    }

    AbstractResourceException(String code, String message, Throwable cause) {
        super(Series.R, code, message, cause);
    }
}
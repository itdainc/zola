package com.itda.zola.admin.infrastructure.exception.resource;

public class ResourceNotFoundException extends AbstractResourceException {

    public ResourceNotFoundException(String message) {
        super("04", message);
    }

    public ResourceNotFoundException(String message, Throwable cause) {
        super("04", message, cause);
    }
}
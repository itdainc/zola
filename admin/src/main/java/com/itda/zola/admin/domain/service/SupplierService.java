package com.itda.zola.admin.domain.service;

import com.itda.zola.admin.database.SupplierReleaseRepository;
import com.itda.zola.admin.database.SupplierRepository;
import com.itda.zola.domain.model.supplier.Supplier;
import com.itda.zola.domain.model.supplier.SupplierRelease;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class SupplierService {

    private final SupplierRepository supplierRepository;
    private final SupplierReleaseRepository supplierReleaseRepository;

    @Autowired
    public SupplierService(SupplierRepository supplierRepository, SupplierReleaseRepository srr) {
        this.supplierRepository = supplierRepository;
        this.supplierReleaseRepository = srr;
    }

    public Supplier find(long id) {

        return supplierRepository.findOne(id);
    }


    public Supplier findByCode(String code) {

        return supplierRepository.findByCode(code);
    }

    public SupplierRelease findTopBySupplierOrderByIdDesc(Supplier supplier) {

        return supplierReleaseRepository.findTopBySupplierOrderByIdDesc(supplier);
    }

    public List<SupplierRelease> findBySupplierOrderByIdDesc(Supplier supplier) {

        return supplierReleaseRepository.findBySupplierOrderByIdDesc(supplier);
    }

    @Transactional
    public void createRelease(SupplierRelease release) {

        supplierReleaseRepository.save(release);
    }
}

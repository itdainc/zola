package com.itda.zola.admin.infrastructure.exception.logical;

import com.itda.zola.admin.infrastructure.exception.ZolaException;

public abstract class AbstractLogicalException extends ZolaException {

    public AbstractLogicalException(String code, String message) {
        super(Series.L, code, message);
    }

    public AbstractLogicalException(String code, String message, Throwable cause) {
        super(Series.L, code, message, cause);
    }
}
package com.itda.zola.admin.domain.service;

import com.itda.zola.admin.database.ExchangeRepository;
import com.itda.zola.admin.infrastructure.exception.resource.ResourceNotFoundException;
import com.itda.zola.domain.model.notice.Notice;
import com.itda.zola.domain.model.supplier.Supplier;
import com.itda.zola.domain.model.transaction.exchange.Exchange;
import com.itda.zola.domain.model.transaction.exchange.Exchangeable;
import com.itda.zola.domain.model.user.Region;
import com.itda.zola.domain.model.user.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class ExchangeService {

    private ExchangeRepository exchangeRepository;

    @Autowired
    public ExchangeService(ExchangeRepository exchangeRepository){
        this.exchangeRepository = exchangeRepository;
    }

    public Page<Exchange> findBy(Exchange.Status status, String name, String receiver, String username, Pageable pageable) {

        return exchangeRepository.findByStatusAndNameContainingAndReceiverContainingAndUserNameContaining(status, name, receiver, username, pageable);
    }

    public long getCount(Exchange.Status status) {

        return exchangeRepository.countByStatus(status);
    }

    public long getCount() {

        return exchangeRepository.count();
    }

    @Transactional
    public Exchange update(Exchange exchange) {

        log.debug("exchange update = {}", exchange.toString());
        return exchangeRepository.save(exchange);
    }

    public Exchange findBy(long id) {

        log.info("findBy exchange by id({})", id);

        Exchange exchange = exchangeRepository.findOne(id);

        if (exchange == null) {
            log.error("exchange cannot be found. id = {}", id);
            throw new ResourceNotFoundException("요청 정보를 찾을 수 없습니다. id = " + id);
        }

        log.info("exchange = {}", exchange.toString());

        return exchange;
    }

    @Transactional
    public void done(Exchange exchange) {

        exchange.done();
    }

    @Transactional
    public void reject(Exchange exchange, String memo) {

        exchange.reject(memo);
    }
}

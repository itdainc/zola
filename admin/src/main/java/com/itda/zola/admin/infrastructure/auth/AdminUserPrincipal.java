package com.itda.zola.admin.infrastructure.auth;

import com.google.common.collect.ImmutableSet;
import com.itda.zola.admin.domain.model.AdminUser;
import lombok.Getter;
import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;

public class AdminUserPrincipal implements UserDetails, CredentialsContainer, Serializable {

    private static final int LOCK_THRESHOLD = 5;

    @Getter
    private final String username;
    @Getter
    private String password;
    @Getter
    private final LocalDateTime recentAccessAt;

    private final int failureCount;


    private AdminUserPrincipal(AdminUser user) {
        this.username = user.getUsername();
        this.password = user.getPassword();
        this.recentAccessAt = user.getRecentAccessAt();
        this.failureCount = user.getFailureCount();
    }

    public static AdminUserPrincipal of(AdminUser user) {
        return new AdminUserPrincipal(user);
    }

    // ----------

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        return ImmutableSet.of(new SimpleGrantedAuthority("ROLE_ADMIN"));
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return failureCount < LOCK_THRESHOLD;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }


    // ----------

    @Override
    public void eraseCredentials() {
        password = null;
    }
}
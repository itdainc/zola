package com.itda.zola.admin.response;


import com.itda.zola.admin.response.dto.ExchangeResponse;
import com.itda.zola.domain.model.transaction.exchange.Exchange;

import java.util.List;
import java.util.stream.Collectors;

public class ResponseFactory {

    public static List<ExchangeResponse> exchangelist(List<Exchange> exchanges){
        return exchanges.stream().map(ExchangeResponse::new).collect(Collectors.toList());
    }

}
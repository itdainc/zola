package com.itda.zola.admin.infrastructure.util;

import com.google.common.base.Joiner;
import com.google.common.io.ByteStreams;
import org.glassfish.jersey.message.MessageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MultivaluedMap;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.*;

public class JaxRsClientLoggingFilter implements ClientRequestFilter, ClientResponseFilter {

    private static final Logger LOG = LoggerFactory.getLogger(JaxRsClientLoggingFilter.class);
    private static final String JOINER = ", ";
    private static final String BODY_PREFIX = ", Body=[";
    private static final String BODY_POSTFIX = "]";

    private static final Comparator<Map.Entry<String, List<String>>> COMPARATOR =
            (o1, o2) -> o1.getKey().compareToIgnoreCase(o2.getKey());

    @Override
    public void filter(final ClientRequestContext context) throws IOException {
        StringBuilder log = new StringBuilder();

        printRequestHeader(log, context.getMethod(), context.getUri());
        printPrefixedHeaders(log, context.getStringHeaders());

        if (context.hasEntity()) {
            Object entity = context.getEntity();

            String body;
            if (entity instanceof Form) {
                body = Joiner.on(", ").withKeyValueSeparator("=").useForNull("null").join(((Form) entity).asMap());
            } else {
                body = entity.toString();
            }

            log.append(BODY_PREFIX).append(body).append(BODY_POSTFIX);
        }

        LOG.info(log.toString());
    }

    private void printRequestHeader(StringBuilder log, String method, URI uri) {
        log.append("Request: ").append(method).append("=[").append(uri.toASCIIString()).append("]");
    }

    @Override
    public void filter(ClientRequestContext requestContext, ClientResponseContext responseContext) throws IOException {
        StringBuilder log = new StringBuilder();

        printResponseHeader(log, responseContext.getStatus());
        printPrefixedHeaders(log, responseContext.getHeaders());

        if (responseContext.hasEntity()) {
            try (InputStream stream = responseContext.getEntityStream()) {
                byte[] entity = ByteStreams.toByteArray(stream);
                log.append(BODY_PREFIX)
                        .append(new String(entity, MessageUtils.getCharset(responseContext.getMediaType())))
                        .append(BODY_POSTFIX);
                responseContext.setEntityStream(new ByteArrayInputStream(entity));
            }
        }

        LOG.info(log.toString());
    }

    private void printResponseHeader(StringBuilder log, int status) {
        log.append("Response: Status=[").append(Integer.toString(status)).append("]");
    }

    private void printPrefixedHeaders(final StringBuilder b, final MultivaluedMap<String, String> headers) {
        for (final Map.Entry<String, List<String>> headerEntry : getSortedHeaders(headers.entrySet())) {
            final String header = headerEntry.getKey();
            final List<?> val = headerEntry.getValue();

            String message;
            if (val.size() == 1) {
                message = val.get(0).toString();
            } else {
                final StringBuilder sb = new StringBuilder();
                boolean add = false;
                for (final Object s : val) {
                    if (add) {
                        sb.append(',');
                    }
                    add = true;
                    sb.append(s);
                }

                message = sb.toString();
            }

            b.append(JOINER).append(header).append("=[").append(message).append("]");
        }
    }

    private Set<Map.Entry<String, List<String>>> getSortedHeaders(final Set<Map.Entry<String, List<String>>> headers) {
        final TreeSet<Map.Entry<String, List<String>>> sortedHeaders = new TreeSet<>(COMPARATOR);
        sortedHeaders.addAll(headers);
        return sortedHeaders;
    }
}

package com.itda.zola.admin.presentation.controller;

import com.itda.zola.admin.domain.service.SupplierService;
import com.itda.zola.domain.model.supplier.Supplier;
import com.itda.zola.domain.model.supplier.SupplierRelease;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequestMapping("/admin/api")
@RestController
public class ReleaseController {

    @Autowired
    private SupplierService supplierService;


    @RequestMapping(path="/supplier/{sId}/releases", method = RequestMethod.GET)
    public ResponseEntity<List<ReleaseResponse>> getReleases(@PathVariable String sId) {

        Supplier supplier = supplierService.findByCode(sId);
        List<SupplierRelease> releases = supplierService.findBySupplierOrderByIdDesc(supplier);

        return ResponseEntity.ok(releases.stream().map(ReleaseResponse::new).collect(Collectors.toList()));
    }

    @RequestMapping(path="/supplier/{sId}/releases", method = RequestMethod.POST)
    public ResponseEntity<Void> createReleases(@PathVariable String sId, @RequestBody ReleaseCreateRequest request) {

        Supplier supplier = supplierService.findByCode(sId);

        SupplierRelease release = new SupplierRelease();
        release.setVersion(request.version);
        release.setMandatory(request.mandatory);
        release.setMemo(request.memo);
        release.setReleasedAt(LocalDateTime.now());
        release.setSupplier(supplier);

        supplierService.createRelease(release);

        return ResponseEntity.ok().build();
    }

    public static class ReleaseCreateRequest {
        public Integer version;
        public boolean mandatory;
        public String memo;
    }

    @Getter
    public static class ReleaseResponse {

        private final Integer version;
        private final boolean mandatory;

        private final LocalDateTime releasedAt;

        private final String memo;

        public ReleaseResponse(SupplierRelease release) {
            this.version = release.getVersion();
            this.mandatory = release.isMandatory();
            this.releasedAt = release.getReleasedAt();
            this.memo = release.getMemo();
        }
    }
}
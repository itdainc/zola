package com.itda.zola.admin.domain.service;

import com.itda.zola.admin.database.ExchangeRepository;
import com.itda.zola.admin.database.NoticeRepository;
import com.itda.zola.admin.infrastructure.exception.resource.ResourceNotFoundException;
import com.itda.zola.domain.model.notice.Notice;
import com.itda.zola.domain.model.transaction.exchange.Exchange;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
public class NoticeService {

    private NoticeRepository noticeRepository;

    @Autowired
    public NoticeService(NoticeRepository noticeRepository){
        this.noticeRepository = noticeRepository;
    }

    public Page<Notice> findBy(String title, Pageable pageable) {

        return noticeRepository.findByTitleContaining(title, pageable);
    }

    public long getCount(String title) {

        return noticeRepository.countByTitle(title);
    }

    @Transactional
    public Notice create(Notice notice) {

        log.debug("notice created = {}", notice.toString());
        return noticeRepository.save(notice);
    }

    @Transactional
    public Notice update(Notice notice) {

        log.debug("notice update = {}", notice.toString());
        return noticeRepository.save(notice);
    }

    @Transactional
    public void remove(long id) {

        log.info("expire notice.id = {}", id);

        noticeRepository.delete(id);
    }

    public Notice findBy(long id) {

        log.info("findBy notice by id({})", id);

        Notice notice = noticeRepository.findOne(id);

        if (notice == null) {
            log.error("notice cannot be found. id = {}", id);
            throw new ResourceNotFoundException("공지사항 정보를 찾을 수 없습니다. id = " + id);
        }

        log.info("product = {}", notice.toString());

        return notice;
    }
}

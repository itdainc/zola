package com.itda.zola.admin.presentation.controller;

import com.itda.zola.admin.domain.entity.PromotionCreateRequest;
import com.itda.zola.admin.domain.service.PromotionService;
import com.itda.zola.admin.domain.service.SupplierService;
import com.itda.zola.admin.presentation.response.Response;
import com.itda.zola.domain.model.promotion.Promotion;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dylan on 2016. 12. 28..
 */
@Slf4j
@RestController
@RequestMapping("/admin")
@Api(value = "promotion admin ", description = "promotion swagger")
public class PromotionController {

    @Autowired
    PromotionService promotionService;
    @Autowired
    SupplierService supplierService;

    @RequestMapping(value = "/promotions", method = RequestMethod.GET)
    public Response find(@PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC, size = 5) Pageable pageable) {

        log.info("promotions find");

        Page<Promotion> page = promotionService.findBy(supplierService.findByCode("emo-adr"), pageable);

        log.info("promotions size={}", page.getTotalElements());
        return Response.of().addObject(page);
    }

    @RequestMapping(value = "/promotions/count", method = RequestMethod.GET)
    public Response count() {
        Map<String, Object> r = new HashMap<>();

        r.put("promotionsCount", promotionService.getCount());

        return Response.of().addObject(r);
    }

    @RequestMapping(value = "/promotions/{id:\\d+}", method = RequestMethod.GET)
    public Response findOne(@PathVariable long id) {

        Promotion promotion = promotionService.findBy(id);

        return Response.of().addObject(promotion);
    }

    @RequestMapping(value = "/promotions/{id:\\d+}", method = RequestMethod.POST)
    public Response update(@PathVariable Long id, @RequestBody PromotionCreateRequest request) {

        log.debug("update promotion id={}", id);
        log.debug("update PromotionCreateRequest info={}", request.toString());

        Promotion promotion = promotionService.findBy(id);
        promotion.setAmount(request.getAmount());
        promotion.setSince(request.getSince());
        promotion.setUntil(request.getUntil());
        promotion.setAmount2(request.getAmount2());

        log.debug("update promotion set info={}", promotion.toString());
        promotionService.update(promotion);

        return Response.of().addObject(promotion.getId());
    }
}

package com.itda.zola.admin.presentation.response;//package com.itda.hulk.report.presentation.response;
//
//import com.itda.hulk.admin.presentation.response.dto.BenefitResponse;
//import com.itda.hulk.admin.presentation.response.dto.CandidateResponse;
//import com.itda.hulk.admin.presentation.response.dto.SimpleBenefitResponse;
//import com.itda.hulk.admin.presentation.response.dto.SimpleCandidateResponse;
//import com.itda.hulk.domain.model.benefit.Benefit;
//import com.itda.hulk.domain.model.benefit.candidate.BenefitCandidate;
//
//import java.util.List;
//import java.util.stream.Collectors;
//
//public class ResponseFactory {
//
//    public static SimpleCandidateResponse simple(BenefitCandidate benefitCandidate) {
//        return new SimpleCandidateResponse(benefitCandidate);
//    }
//
//    public static CandidateResponse total(BenefitCandidate benefitCandidate) {
//        return new CandidateResponse(benefitCandidate);
//    }
//
//    public static List<SimpleCandidateResponse> candidateList(List<BenefitCandidate> benefitCandidates) {
//        return benefitCandidates.stream().map(SimpleCandidateResponse::new).collect(Collectors.toList());
//    }
//
//    public static List<SimpleBenefitResponse> benefitList(List<Benefit> benefits){
//        return benefits.stream().map(SimpleBenefitResponse::new).collect(Collectors.toList());
//    }
//
//    public static BenefitResponse benefit(Benefit benefit) {
//        return new BenefitResponse(benefit);
//    }
//}

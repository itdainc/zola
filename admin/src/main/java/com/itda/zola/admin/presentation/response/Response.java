package com.itda.zola.admin.presentation.response;


import com.itda.zola.admin.infrastructure.exception.ZolaException;

public class Response {

    private static final short STATUS_SUCCESS = 200;
    private static final short STATUS_FAILURE = -1;

    private final short status;
    private final String code;

    private String message;
    private Object data;


    private Response(short status, String code) {
        this.status = status;
        this.code = code;
    }

    private Response(short status, String code, String message) {
        this(status, code);
        this.message = message;
    }

    public static Response of() {
        return new Response(STATUS_SUCCESS, "OK");
    }

//    public static Response ok() {
//    }


    public static Response of(ZolaException e) {
        return new Response(STATUS_FAILURE, e.getCode(), e.getMessage());
    }

    public static Response of(Exception e) {
        return new Response(STATUS_FAILURE, "S500", "시스템 오류입니다. 잠시 후 다시 시도해주세요. 문제가 지속되는 경우 관리자에게 연락 부탁드립니다.");
    }


    // ----------

    public short getStatus() {
        return status;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public Object getData() {
        return data;
    }

    public Response addObject(Object data) {
        this.data = data;
        return this;
    }
}

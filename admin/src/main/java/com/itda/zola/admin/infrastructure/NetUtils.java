package com.itda.zola.admin.infrastructure;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.List;

@Slf4j
public class NetUtils {

	/**
	 * Remote 호스트의 HostName 반환<br />
	 */
	private final static String[] isInternalZone = {
		// private ipv4 ips
			"127.0.0.1/32", // localhost
			"203.247.143.20/24", // itpo dev
			"52.79.178.171/24", // itpo dev
			"52.78.61.160/24" // itpo real web
//			"52.78.176.65/24", // itpo real gateway2
//			"52.78.150.5/24", // itpo real gateway1
	};

	/**
	 * Remote 호스트의 Host Address 반환<br />
	 * 
	 * @return
	 */
	public static String getHostAddress(HttpServletRequest request) {
		String addr = request.getHeader("x-forwarded-for");
		if(Strings.isNullOrEmpty(addr)) {
			addr = request.getHeader("x-real-ip");
		}

		if(Strings.isNullOrEmpty(addr)) {
			addr = request.getRemoteAddr();
		}
        log.debug("getHostAddress addr : {}", addr);
		return addr;
	}

	/**
	 * Local host
	 * 
	 * @return
	 */
	public static String getLocalHostAddress() {
		String addr = "";
		try {
			InetAddress inet = Inet4Address.getLocalHost();
			addr = inet.getHostAddress();
		} catch (Exception e) {
		}

		return addr;
	}

	public static String getLocalHostName() {
		String hostname = "";
		try {
			InetAddress inet = Inet4Address.getLocalHost();
			hostname = inet.getHostName();
		} catch (Exception e) {
		}

		return hostname;

	}

	/**
	 * 
	 * @return
	 */
	public static boolean isIntenalNetwork(HttpServletRequest request) {
//		return true;
		
		String ipaddr = getHostAddress(request);
		log.debug("[@] Remote IP Address : {}", ipaddr);
		
		if (ipaddr == null)
			return false;

        return isInternalNetwork(ipaddr);
	}

    public static boolean isInternalNetwork(String ipaddr) {
        List<String> ips;
        if (ipaddr.indexOf(',') >= 0) {
            Iterable<String> result = Splitter.on(',').trimResults().omitEmptyStrings().split(ipaddr);
            ips = Lists.newArrayList(result);
        } else {
            ips = Lists.newArrayList(ipaddr.trim());
        }

        for (String item: ips) {
            try {
                long longRemoteAddr = ip2Long(item);

                for (String ip : isInternalZone) {
                    List<String> x = Lists.newArrayList(Splitter.on("/").omitEmptyStrings().trimResults().split(ip));
                    long longAddr = ip2Long(x.get(0));
                    int mask = Integer.parseInt(x.get(1));
                    mask = 0xFFFFFFFF << (32 - mask);

					log.debug("longRemoteAddr ={} ", longRemoteAddr);
					log.debug("longAddr ={} ", longAddr);
					log.debug("mask ={} ", mask);
                    if ((longAddr & mask) == (longRemoteAddr & mask)) {
                        return true;
                    }
                }
            } catch (Exception e) {
            }
        }

        return false;
    }

    /**
	 * IP2Long translation<br />
	 * 
	 * @param addr
	 * @return
	 */
	public static long ip2Long(String addr) {
		String[] addrArray = addr.split("\\.");

		long num = 0;
		for (int i = 0; i < addrArray.length; i++) {
			int power = 3 - i;

			num += ((Integer.parseInt(addrArray[i]) % 256 * Math.pow(256, power)));
		}

		return num;
	}

	/**
	 * Long2IP Translation <br />
	 * 
	 * @param l
	 * @return
	 */
	public static String logn2Ip(long l) {
		return ((l >> 24) & 0xFF) + "." + ((l >> 16) & 0xFF) + "." + ((l >> 8) & 0xFF) + "." + (l & 0xFF);
	}

	public static String getScheme(HttpServletRequest request) {
		String scheme = request.getHeader("x-forwarded-proto");
		log.debug("X-Forwarded-Proto : " + scheme);

		if(Strings.isNullOrEmpty(scheme)) {
			scheme = request.getScheme();
		}

		return scheme;
	}

}

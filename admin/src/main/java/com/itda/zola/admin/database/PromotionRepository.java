package com.itda.zola.admin.database;

import com.itda.zola.domain.model.promotion.Promotion;
import com.itda.zola.domain.model.supplier.Supplier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PromotionRepository extends JpaRepository<Promotion, Long> {

    Page<Promotion> findBySupplier(Supplier supplier, Pageable pageable);
}

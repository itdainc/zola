package com.itda.zola.admin.presentation.controller;

import com.itda.zola.admin.domain.entity.AdminUserCreateRequest;
import com.itda.zola.admin.domain.model.AdminUser;
import com.itda.zola.admin.domain.service.AdminUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Created by dylan on 2016. 12. 26..
 */
@Slf4j
@Api(value = "admin " , description = "admin swagger")
@RequestMapping("/admin/api")
@RestController
public class AdminController {

    @Autowired
    AdminUserService adminUserService;

    @ApiOperation("1.Admin User 추가")
    @RequestMapping(path="/user", method = RequestMethod.POST)
    public ResponseEntity<Void> createAdminUser(@Valid @RequestBody AdminUserCreateRequest request) {

        log.debug("request data={}", request.toString());

        AdminUser user = new AdminUser(request.getUsername(), request.getPassword());

        adminUserService.create(user);

        return ResponseEntity.ok().build();
    }

}

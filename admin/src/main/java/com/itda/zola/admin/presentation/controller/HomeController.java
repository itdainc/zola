package com.itda.zola.admin.presentation.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@Slf4j
@RestController
public class HomeController {

    @RequestMapping("/user")
    public Principal user(Principal user) {
        return user;
    }

}

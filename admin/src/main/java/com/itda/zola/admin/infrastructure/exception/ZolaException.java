package com.itda.zola.admin.infrastructure.exception;

/**
 * Created by dylan on 2016. 12. 6..
 */
public class ZolaException extends RuntimeException {

    private Series series;
    private String code;
    private int status;


    public ZolaException() {
    }

    public ZolaException(Series series, String code, String message) {
        super(message);
        this.series = series;
        this.code = code;
    }

    public ZolaException(Series series, String code, String message, Throwable cause) {
        super(message, cause);
        this.series = series;
        this.code = code;
    }

    public String getCode() {
        return series.name() + code;
    }


    public enum Series {

        R, // Resource
        A, // Authentication
        L, // Logical
        C, // Client
        S  // Service Unavailable
    }
}

package com.itda.zola.admin.presentation.controller;

import com.itda.zola.admin.domain.entity.NoticeCreateRequest;
import com.itda.zola.admin.domain.service.NoticeService;
import com.itda.zola.admin.domain.service.SupplierService;
import com.itda.zola.admin.presentation.response.Response;
import com.itda.zola.domain.model.notice.Notice;
import com.itda.zola.domain.model.supplier.Supplier;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by dylan on 2016. 12. 28..
 */
@Slf4j
@RestController
@RequestMapping("/admin")
@Api(value = "notice admin ", description = "notice swagger")
public class NoticeController {

    @Autowired
    NoticeService noticeService;
    @Autowired
    SupplierService supplierService;

    @RequestMapping(value = "/notices", method = RequestMethod.GET)
    public Response find(@RequestParam(required = false, defaultValue = "") String title,
                         @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC, size = 5) Pageable pageable) {

        log.info("find notice begin");

        Page<Notice> page = noticeService.findBy(title, pageable);
        log.info("result={}", page.getContent());

        return Response.of().addObject(page);
    }

    @RequestMapping(value = "/notices/count", method = RequestMethod.GET)
    public Response count(@RequestParam(required = false, defaultValue = "") String title) {
        Map<String, Object> r = new HashMap<>();

        r.put("noticesCount", noticeService.getCount(title));

        return Response.of().addObject(r);
    }

    @RequestMapping(value = "/notices", method = RequestMethod.POST)
    public Response create(@Valid @RequestBody NoticeCreateRequest request) {

        log.debug("notice info ={}", request.toString());

        Supplier supplier = supplierService.findByCode("emo-adr");

        Notice notice = new Notice(supplier, request.getTitle(), request.getUrl());

        noticeService.create(notice);

        return Response.of().addObject(notice.getId());
    }

    @RequestMapping(value = "/notices/{id:\\d+}", method = RequestMethod.GET)
    public Response findOne(@PathVariable long id) {

        Notice notice = noticeService.findBy(id);

        return Response.of().addObject(notice);
    }

    @RequestMapping(value = "/notices/{id:\\d+}", method = RequestMethod.DELETE)
    public Response remove(@PathVariable Long id) {

        log.debug("remove notice id={}", id);

        noticeService.remove(id);

        return Response.of();
    }

    @RequestMapping(value = "/notices/{id:\\d+}", method = RequestMethod.POST)
    public Response update(@PathVariable Long id, @RequestBody NoticeCreateRequest request) {

        log.debug("update notice id={}", id);
        log.debug("update NoticeCreateRequest info={}", request.toString());

        Notice notice = noticeService.findBy(id);
        notice.setTitle(request.getTitle());
        notice.setUrl(request.getUrl());

        log.debug("update notice set info={}", notice.toString());
        noticeService.update(notice);

        return Response.of().addObject(notice.getId());
    }
}

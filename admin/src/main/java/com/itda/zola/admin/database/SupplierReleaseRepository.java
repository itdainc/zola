package com.itda.zola.admin.database;

import com.itda.zola.domain.model.supplier.Supplier;
import com.itda.zola.domain.model.supplier.SupplierRelease;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SupplierReleaseRepository extends JpaRepository<SupplierRelease, Long>{

    SupplierRelease findTopBySupplierOrderByIdDesc(Supplier supplier);

    List<SupplierRelease> findBySupplierOrderByIdDesc(Supplier supplier);
}
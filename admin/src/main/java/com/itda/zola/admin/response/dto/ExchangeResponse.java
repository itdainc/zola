package com.itda.zola.admin.response.dto;

import com.itda.zola.domain.model.transaction.exchange.Exchange;
import lombok.Getter;

@Getter
public class ExchangeResponse {

    private String name;
    private String username;
    private String phoneNo;
    private String status;

    public ExchangeResponse(Exchange exchange) {
        this.name = exchange.getName();
        this.username = exchange.getUser().getName();
        this.phoneNo = exchange.getReceiver();
        this.status = exchange.getStatus().toString();
    }
}

package com.itda.zola.admin.presentation.response.dto;//package com.itda.hulk.report.presentation.response.dto;
//
//import com.itda.hulk.domain.model.benefit.Benefit;
//import com.itda.hulk.infrastructure.util.ImageUtil;
//import lombok.Getter;
//
//import java.util.Arrays;
//import java.util.List;
//import java.util.stream.Collectors;
//
//@Getter
//public class BenefitResponse extends SimpleBenefitResponse{
//
//    private String summary;
//    private String sectionCode;
//
//
//    private boolean storeBenefit = false;
//
//    private int storeCount;
//
//    private long amount;
//    private Integer quantity;
//
//
//    private List<String> images;
//    private List<String> description;
//
//    public BenefitResponse(Benefit benefit) {
//        super(benefit);
//
//        this.storeBenefit = (benefit.getProductCount() < 1);
//        this.storeCount = benefit.getStoreCount();
//
//        this.amount = benefit.getAmount();
//        this.quantity = benefit.getQuantity();
//
//        this.images = benefit.getImages().stream().map(image -> image.getUrl()).collect(Collectors.toList());
//        this.description = Arrays.stream(benefit.getDescription().split(",")).map(s -> ImageUtil.fullUrl(s)).collect(Collectors.toList());
//
//        this.summary = benefit.getSummary();
//        this.sectionCode = benefit.getSection().getCode();
//    }
//}

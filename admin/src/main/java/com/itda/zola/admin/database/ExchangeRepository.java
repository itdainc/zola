package com.itda.zola.admin.database;

import com.itda.zola.domain.model.transaction.exchange.Exchange;
import com.itda.zola.domain.model.user.Region;
import com.itda.zola.domain.model.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ExchangeRepository extends JpaRepository<Exchange, Long> {

    List<Exchange> findByStatusAndRegion(Exchange.Status status, Region region);

    @EntityGraph(attributePaths = "user")
    Page<Exchange> findByStatusAndNameContainingAndReceiverContainingAndUserNameContaining(Exchange.Status status, String name, String receiver, String username, Pageable pageable);

    Long countByStatus(Exchange.Status status);
}

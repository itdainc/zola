package com.itda.zola.admin.domain.entity;

import lombok.Data;

/**
 * Created by dylan on 2016. 12. 12..
 */
@Data
public class ExchangeCreateRequest {

    private String value;
    private String message;
}

package com.itda.zola.admin.database;

import com.itda.zola.domain.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by dylan on 2016. 12. 23..
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Long countByExpiredAtIsNotNull();

    Long countByExpiredAtIsNull();
}

angular.module("app.controller", [])

    .controller("dashboardController", function ($scope, $location, $routeParams, $log, dashBoardService ) {

        $scope.find = function () {

            var promise;

            promise = dashBoardService.find();
            promise.then(function (r) {

                $scope.dashboard =r.data;
            });
        };

        $scope.find();
    })

    .controller("reportController", function ($scope, $routeParams, reportService, Pageable) {

        $scope.query = {
            status: 'REGISTERED',
            type: '1',
            value: '',
            filter: $routeParams.filter
        };

        $scope.pages = Pageable.basic();

        $scope.find = function () {

            var promise;

            promise = reportService.find($scope.query, $scope.pages);
            promise.then(function (r) {

                var data = r.data;

                $scope.pages.update(data);
                $scope.dailyReports = data.content;
            });

            render_graph("/admin/report/cp-pie/csv?page=" + ( $scope.pages.page - 1 ));
        };

        $scope.count = {
            total: 0
        };

        (function () {
            reportService.count($scope.query).then(function (r) {
                $scope.count = r.data;
            });
        })();

        $scope.find();

        function render_graph(url) {

            $("#graph-area").html("");

            var radius = 74,
                padding = 10;

            var color = d3.scale.ordinal()
                .range(["#98abc5", "#8a89a6", "#7b6888"]);

            var arc = d3.svg.arc()
                .outerRadius(radius)
                .innerRadius(radius - 30);

            var pie = d3.layout.pie()
                .sort(null)
                .value(function(d) {
                    console.log(d);
                    return d.revenue;
                });

            d3.csv(url, function(error, data) {
                if (error) throw error;

                color.domain(d3.keys(data[0]).filter(function(key) { return key !== "Date"; }));

                data.forEach(function(d) {
                    d.cps = color.domain().map(function(name) {
                        return {name: name, revenue: +d[name]};
                    });
                });

                var legend = d3.select("#graph-area").append("svg")
                    .attr("class", "legend")
                    .attr("width", radius)
                    .attr("height", radius * 2)
                    .selectAll("g")
                    .data(color.domain().slice().reverse())
                    .enter().append("g")
                    .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

                legend.append("rect")
                    .attr("width", 18)
                    .attr("height", 18)
                    .style("fill", color);

                legend.append("text")
                    .attr("x", 24)
                    .attr("y", 9)
                    .attr("dy", ".35em")
                    .text(function(d) { return d; });

                var svg = d3.select("#graph-area").selectAll(".pie")
                    .data(data)
                    .enter().append("svg")
                    .attr("class", "pie")
                    .attr("width", radius * 2.5)
                    .attr("height", radius * 2.5)
                    .append("g")
                    .attr("transform", "translate(" + radius + "," + radius + ")");

                svg.selectAll(".arc")
                    .data(function(d) { return pie(d.cps); })
                    .enter().append("path")
                    .attr("class", "arc")
                    .attr("d", arc)
                    .style("fill", function(d) { return color(d.data.name); });

                svg.append("text")
                    .attr("dy", ".35em")
                    .style("text-anchor", "middle")
                    .text(function(d) { return d.Date; });

            });
        }
    })

    .controller("releaseListController", function ($scope, $http, $route) {

        var currentVersion;

        $http.get("/admin/api/supplier/emo-adr/releases")
            .then(function(r) {
                $scope.releases = r.data;
                currentVersion = $scope.releases[0].version;
            }, function(){
                alert("배포 목록을 가져오는 중 오류가 발생했습니다.");
            });

        $scope.model = {
            version : "",
            mandatory : false,
            memo : "",
            supplierCode : "emo-adr"
        };

        $scope.post = function() {

            if($scope.model.version == "" || $scope.model.version <= currentVersion) {
                alert("최신버전 " + currentVersion + " 보다 낮은 버전을 등록하려고 합니다! 정신차리세요!");
                return;
            }

            var confirm = window.confirm("!!!!!!!! 버전 명 / 필수업데이트를 정확하게 확인해주세요. 심각한 문제를 초래할 수 있습니다!!!!!!!!!");

            if(confirm) {
                $http.post("/admin/api/supplier/emo-adr/releases", $scope.model)
                    .then(function(r) {
                        alert("성공");
                        $route.reload();
                    }, function() {
                        alert("실패");
                    })
            }
        }

    })

    .controller("exchangeController", function ($scope, $routeParams, exchangeService, Pageable, $uibModal) {

        $scope.query = {
            status: 'REGISTERED',
            type: '1',
            value: '',
            filter: $routeParams.filter
        };

        $scope.pages = Pageable.basic();

        $scope.find = function () {

            var promise;

            promise = exchangeService.find($scope.query, $scope.pages);
            promise.then(function (r) {

                var data = r.data;

                $scope.pages.update(data);
                $scope.exchanges = data.content;
            });
        };

        $scope.count = {
            total: 0
        };

        (function () {
            exchangeService.count($scope.query).then(function (r) {
                $scope.count = r.data;
            });
        })();

        $scope.find();

        $scope.success = function (id) {

            var promise;

            promise = exchangeService.findOne(id);
            promise.then(function (r) {

                $uibModal.open({
                    animation: true,
                    // ariaLabelledBy: 'modal-title',
                    // ariaDescribedBy: 'modal-body',
                    templateUrl: 'views/exchange/modal_success.html',
                    // controller: 'ModalInstanceCtrl',
                    // controllerAs: '$ctrl',
                    size: '',
                    controller: function ($scope, $uibModalInstance) {
                        $scope.exchange = r.data;
                        $scope.close = function () {
                            $uibModalInstance.dismiss('cancel');
                        };
                    }
                    // appendTo: document.body
                });
            });

        };

        $scope.reject = function (id) {

            var promise;

            promise = exchangeService.findOne(id);
            promise.then(function (r) {

                $uibModal.open({
                    animation: true,
                    // ariaLabelledBy: 'modal-title',
                    // ariaDescribedBy: 'modal-body',
                    templateUrl: 'views/exchange/modal_reject.html',
                    // controller: 'ModalInstanceCtrl',
                    // controllerAs: '$ctrl',
                    size: '',
                    controller: function ($scope, $uibModalInstance) {
                        $scope.exchange = r.data;
                        $scope.close = function () {
                            $uibModalInstance.dismiss('cancel');
                        };
                    }
                    // appendTo: document.body
                });
            });

        };
    })

    .controller("exchangeModalController", function ($scope, $route, $log, exchangeService) {

        $scope.query = {
            value: '1',
            message: '',
        };

        $scope.complete = function (id) {
            var promise;

            promise = exchangeService.done(id);

            promise.then(
                function (data) {

                    if (data.status != 200) {
                        alert(data.message);
                        return;
                    }

                    alert("지급 완료");
                    $scope.close();
                    $route.reload();
                }
            );
        };

        $scope.reject = function (id) {

            var promise;

            promise = exchangeService.reject(id, angular.copy($scope.query));

            promise.then(
                function (data) {

                    if (data.status != 200) {
                        alert(data.message);
                        return;
                    }

                    alert("반려 완료");
                    $scope.close();
                    $route.reload();
                }
            );
        }

    })

    .controller("noticeController", function ($scope, $routeParams, noticeService, Pageable, $log) {

        $scope.query = {
            title: '',
            filter: $routeParams.filter
        };

        $scope.pages = Pageable.basic();

        $scope.find = function () {

            var promise;

            promise = noticeService.find($scope.query, $scope.pages);
            promise.then(function (r) {

                var data = r.data;

                $scope.pages.update(data);
                $scope.notices = data.content;
            });
        };

        $scope.count = {
            total: 0
        };

        (function () {
            noticeService.count($scope.query).then(function (r) {
                $scope.count = r.data;
            });
        })();

        $scope.find();

    })

    .controller("noticeRegisterController", function ($scope, $route, noticeService, $log) {

        $scope.notice = {
            title: "",
            url: ""
        };

        $scope.register = function () {

            var promise;

            promise = noticeService.register(angular.copy($scope.notice));
            promise.then(
                function (data) {

                    if (data.status != 200) {
                        alert(data.message);
                        return;
                    }

                    alert("등록완료");

                    $route.reload();
                }
            );
        };

    })

    .controller("noticeDetailController", function ($scope, $window, $routeParams, noticeService, $log) {

        var id = $routeParams.id;

        noticeService.findOne(id).then(function (r) {

            if (r.status !== 200) {
                setTimeout(function () {
                    $window.history.back();
                }, 0);
            }

            $scope.notice = r.data;
        });

        $scope.expire = function () {

            alert("삭제 시도");

            noticeService.expire(id).then(function (r) {

                if (r.status !== 200) {
                    alert("삭제를 실패했습니다. 다시 시도해 주세요.");
                    return;
                }

                alert("삭제했습니다.");

                setTimeout(function () {
                    $window.history.back();
                }, 0);
            })
        };

        $scope.update = function () {

            var promise;

            promise = noticeService.update(id, angular.copy($scope.notice));
            promise.then(
                function (data) {

                    if (data.status != 200) {
                        alert(data.message);
                        return;
                    }

                    alert("수정완료");

                    setTimeout(function () {
                        $window.history.back();
                    }, 0);
                }
            );

        };

    })

    .controller("promotionController", function ($scope, $routeParams, promotionService, Pageable, $log) {

        $scope.query = {
            title: '',
            filter: $routeParams.filter
        };

        $scope.pages = Pageable.basic();

        $scope.find = function () {

            var promise;

            promise = promotionService.find($scope.query, $scope.pages);
            promise.then(function (r) {

                var data = r.data;

                $scope.pages.update(data);
                $scope.promotions = data.content;
            });
        };

        $scope.count = {
            total: 0
        };

        (function () {
            promotionService.count().then(function (r) {
                $scope.count = r.data;
            });
        })();

        $scope.find();

    })

    .controller("promotionDetailController", function ($scope, $window, $routeParams, promotionService, $log) {

        var id = $routeParams.id;

        promotionService.findOne(id).then(function (r) {

            if (r.status !== 200) {
                setTimeout(function () {
                    $window.history.back();
                }, 0);
            }

            $scope.promotion = r.data;
        });

        $scope.update = function () {

            var promise;

            promise = promotionService.update(id, angular.copy($scope.promotion));
            promise.then(
                function (data) {

                    if (data.status != 200) {
                        alert(data.message);
                        return;
                    }

                    alert("수정완료");

                    setTimeout(function () {
                        $window.history.back();
                    }, 0);
                }
            );

        };

    })

;

'use strict';

admin.controller('BenefitModalCtrl', function($scope, $http, $uibModalInstance, selectBenefit, section, $log, Uploader) {
        String.prototype.replaceAll = function(s,r){return this.split(s).join(r)}

        $scope.benefit = selectBenefit;

        if (section != null) {
            $scope.sections = section;
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.modify = function(id) {
            var benefit = angular.copy($scope.benefit);
            var desc = [];
            angular.forEach(benefit.description, function(value){
                if(value != undefined){
                    var result = value.replace("http://static.myitpo.com/", "")
                    if(result.length > 0){
                        desc.push(result);
                    }
                }
            });

            benefit.description = desc.toString();

            $http.put("/api/benefits/" + id, benefit).then(function() {
                alert("성공");
                $uibModalInstance.close();
            }, function() {
                alert("실패");
            })
        };

        var url = 'http://static.myitpo.com/';

        var uploader = Uploader.getInstance(fileKey);

        var date = moment().format("YYYYMMDD");

        function fileKey(file) {

            var type = file.type.replace("image/", "");
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            for (var i = 0; i < 5; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));

            return "images/scale/benefit/" + date + "/" + text + "." + type;
        }

        $scope.uploadDescription = function (files, seq) {

            if (files == null) {
                return;
            }

            var file = files[0];

            $log.debug("file", file);
            $log.debug("seq", seq);

            uploader.upload(file).then(function (key) {

                $scope.benefit.description[seq] = url+key;
            })
        };


        $scope.delete = function (index){
            $scope.benefit.description[index] = '';
        }
    })


    .controller('ThemeModalCtrl', function($scope, $route, $http, $uibModalInstance, theme, tbMap, Uploader) {
        $scope.addList = [];
        $scope.tbMap = tbMap;
        $scope.theme = theme;

        var benefits;
        $http.get("/api/benefits/active").then(function (r) {
            $scope.benefits = r.data;
        }, function () {
            alert("혜택 목록을 불러오는 도중 오류가 발생했습니다. 새로고침 해주세요/")
        });

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };


        $scope.add = function (id) {

            if ($scope.tbMap[id]) {
                alert("이미 등록된 혜택 입니다.");
                return;
            }
            $scope.tbMap[id] = true;
            $scope.addList.push(id);
        };

        $scope.ok = function () {

            $http.put("/api/themes/" + id, {
                ids: $scope.addList
            }).then(function () {
                alert("성공");
                $route.reload();
            }, function (r) {
                alert(r.message);
            });
        }
    });
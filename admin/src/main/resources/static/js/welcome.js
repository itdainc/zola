var admin = angular.module("admin", ["auth", "ngRoute", "common", "ui.bootstrap","app.controller","app.service"])
    .config(function ($routeProvider, $httpProvider, $logProvider) {

        function Handler(tpl, controller) {

            this.templateUrl = "/views/" + tpl;
            this.controller = controller;

            // this.controllerAs = "scr";

            // this.reloadOnSearch = false;
        }

        Handler.of = function (tpl, controller) {
            return new Handler(tpl, controller);
        };

        $httpProvider.interceptors.push("authenticateInterceptor");
        $httpProvider.interceptors.push("errorInterceptor");
        $httpProvider.defaults.timeout = 3000;

        // routes
        $routeProvider
            .when("/", Handler.of("dashboard.html", "dashboardController"))
            .when("/dashboard", Handler.of("dashboard.html", "dashboardController"))
            .when("/login", Handler.of("login.html", "loginController"))
            .when("/exchange", Handler.of("exchange/list.html", "exchangeController"))
            .when("/notice", Handler.of("notice/list.html", "noticeController"))
            .when("/notice/register/add", Handler.of("notice/register.html", "noticeRegisterController"))
            .when("/notice/:id", Handler.of("notice/detail.html", "noticeDetailController"))
            .when("/promotion", Handler.of("promotion/list.html", "promotionController"))
            .when("/promotion/:id", Handler.of("promotion/detail.html", "promotionDetailController"))
            .when("/releases", Handler.of("release/list.html", "releaseListController"))
            .when("/report", Handler.of("report/list.html", "reportController"))
            // .when("/login", Handler.of("login.html", "loginController"))
            .otherwise({
                templateUrl: "/views/404.html",
                redirectTo: "/404"
            });

        // logs
        $logProvider.debugEnabled(true);
    })

    .constant("patterns", {
        username: '[a-z\\d_-]{5,20}',
        password: '(?=.*\\d)(?=.*[a-zA-Z]).{6,16}',
        phone: '\\d{7,8}',
        company: '\\d{10}'
    })

    .controller("loginController", function ($scope, $location, $log, auth) {

        $scope.credentials = {};
        $scope.remember = false;

        $scope.login = function () {

            var promise;

            promise = auth.authenticate($scope.credentials, $scope.remember);
            promise.then(
                function () {

                    $location.path(auth.path().home);
                    $log.debug("authentication success");

                    // $log.debug("auth.getAuthentication().principal.status =[" + auth.getAuthentication().principal.username + "] " + $scope.login_state);

                    if (auth.getAuthentication().principal.status == 'REGISTERED') {
                        $location.path("/waiting");
                    }
                    if (auth.getAuthentication().principal.status == 'REJECTED') {
                        $location.path("/reject");
                    }

                },
                function (r) {

                    var data = r.data;

                    $log.debug("data =[" + data.message + "]");

                    if (data.message == 'L') {
                        $scope.lock = true;
                    } else if (data.message == 'B') {
                        $scope.error = '아이디, 비밀번호가 올바르지 않습니다. 확인후 다시 입력해주세요.';
                    } else {
                        $scope.error = data.message;
                    }

                    $log.debug("authentication failure")
                });
        };
    })

    .run(function ($log, $rootScope, $location, auth) {

        auth.init('/', '/login', '/logout');

        $rootScope.getAuthentication = auth.getAuthentication;

        $rootScope.isAuthenticated = auth.isAuthenticated;

        $rootScope.logout = function () {
            auth.logout();
        };

        $rootScope.$on("$routeChangeSuccess", function () {
            if ($location.path() != auth.path().login) {
                auth.authenticate();
            }
        });

        $log.debug("init module : welcome");

        $rootScope.logout = function () {

            auth.logout().then(function () {
                $location.path(auth.path().login);
            });
        };
    });
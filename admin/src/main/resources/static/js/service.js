'use strict';

angular.module("app.service", [])

    .factory("reportService", function ($http, $log) {

        return {

            find: function (query, pages) {

                var params = angular.extend({}, query, pages, {sort: 'id,desc'});

                $log.debug("find report. params = ", params);

                if (params.page !== undefined) {
                    params.page -= 1;
                }

                return $http.get("/admin/report", {params: params}).then(responseExtractor);
            },

            count: function (query) {

                var params = angular.extend({}, query);

                if (params.query !== undefined) {
                    params.query = '';
                }

                $log.debug("find count exchange. params = ", params);

                return $http.get("/admin/report/count", {params: params}).then(responseExtractor);
            }

        }
    })

    .factory("exchangeService", function ($http, $log) {

        return {

            find: function (query, pages) {

                var params = angular.extend({}, query, pages, {sort: 'id,desc'});

                $log.debug("find exchanges. params = ", params);

                if (params.page !== undefined) {
                    params.page -= 1;
                }

                return $http.get("/admin/exchanges", {params: params}).then(responseExtractor);
            },

            count: function (query) {

                var params = angular.extend({}, query);

                if (params.query !== undefined) {
                    params.query = '';
                }

                $log.debug("find count exchange. params = ", params);

                return $http.get("/admin/exchanges/count", {params: params}).then(responseExtractor);
            },

            findOne: function (id) {

                return $http.get("/admin/exchanges/" + id).then(responseExtractor);
            },

            done: function (id) {

                return $http.post("/admin/exchanges/" + id).then(responseExtractor);
            },
            reject: function (id, query) {

                return $http.post("/admin/exchanges/reject/" + id, query ).then(responseExtractor);
            }
        }
    })

    .factory("noticeService", function ($http, $log) {

        return {

            find: function (query, pages) {

                var params = angular.extend({}, query, pages, {sort: 'id,desc'});

                $log.debug("find notice. params = ", params);

                if (params.page !== undefined) {
                    params.page -= 1;
                }

                return $http.get("/admin/notices", {params: params}).then(responseExtractor);
            },

            count: function (query) {

                var params = angular.extend({}, query);

                if (params.query !== undefined) {
                    params.query = '';
                }

                $log.debug("find count notice. params = ", params);

                return $http.get("/admin/notices/count", {params: params}).then(responseExtractor);
            },

            register: function (notice) {

                return $http.post("/admin/notices", notice).then(responseExtractor);
            },

            findOne: function (id) {

                return $http.get("/admin/notices/" + id).then(responseExtractor);
            },

            update: function (id, notice) {

                return $http.post("/admin/notices/" + id, notice).then(responseExtractor);
            },

            expire: function (id) {

                return $http.delete("/admin/notices/" + id).then(responseExtractor);
            }
        }
    })

    .factory("promotionService", function ($http, $log) {

        return {

            find: function (query, pages) {

                var params = angular.extend({}, query, pages, {sort: 'id,desc'});

                $log.debug("find notice. params = ", params);

                if (params.page !== undefined) {
                    params.page -= 1;
                }

                return $http.get("/admin/promotions", {params: params}).then(responseExtractor);
            },

            count: function (query) {

                var params = angular.extend({}, query);

                if (params.query !== undefined) {
                    params.query = '';
                }

                $log.debug("find count notice. params = ", params);

                return $http.get("/admin/promotions/count", {params: params}).then(responseExtractor);
            },

            findOne: function (id) {

                return $http.get("/admin/promotions/" + id).then(responseExtractor);
            },

            update: function (id, promotion) {

                return $http.post("/admin/promotions/" + id, promotion).then(responseExtractor);
            }
        }
    })

    .factory("dashBoardService", function ($http, $log) {

        return {

            find: function () {

                return $http.get("/admin/dashboard/total").then(responseExtractor);
            }
        }
    })
;


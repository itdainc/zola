var admin = angular.module("admin", ["ngRoute", "common", "auth", "Uploader","ui.bootstrap"])

    .config(function ($routeProvider, $httpProvider, $logProvider) {

        $routeProvider

            .when("/seed", {

                templateUrl : "views/seed.html",
                controllerAs : "ctrl",
                controller: function($http, $log, $scope) {

                    var self = this;

                    $scope.benefitIds = [{
                        id: 55,
                        name:'[쉐어블링] 기모컬러PT'
                    },  {
                            id: 86,
                            name:'[쉐어블링] 여배우 무스탕'
                        }];

                    $scope.benefitId = $scope.benefitIds[0].id;

                    $scope.find = function () {

                        var params = $scope.benefitId;

                        $http.get("/seed?benefitId="+ params ).then( function(r) {

                            self.seeds = r.data;

                        }, function() {
                            alert("혜택 불러오는 중 오류발생.");
                        });
                    }

                    $scope.find();

                    var date = moment().format("YYYYMMDD");

                }
            })
            .when("/exchange", {

                templateUrl : "views/exchange/exchange.html",
                controllerAs : "ctrl",
                controller: function($http, $log, $scope) {

                    var self = this;

                    self.register = function() {

                        $http.post("/api/exchange", self.entity).then( function() {
                            alert("성공");
                            $route.reload();
                        }, function() {
                            alert("실패");
                        })
                    }

                }
            })
            .when("/notice", {

                templateUrl : "views/notice/notice.html",
                controllerAs : "ctrl",
                controller: function($http, $log, $scope) {

                    var self = this;

                    self.register = function() {

                        $http.post("/api/notice", self.entity).then( function() {
                            alert("성공");
                            $route.reload();
                        }, function() {
                            alert("실패");
                        })
                    }

                }
            })
            .when("/login", {

                templateUrl: "/login.html",
                controller: function ($scope, $location, $log, auth) {

                    $scope.credentials = {};
                    $scope.remember = false;

                    $scope.login = function () {

                        var promise;

                        promise = auth.authenticate($scope.credentials, $scope.remember);
                        promise.then(
                            function () {

                                $log.debug("username" + $scope.credentials.username + " " + $scope.credentials.password);

                                // $location.path(auth.path().home);
                                $log.debug("authentication success");

                                $location.path('/seed')
                                $log.debug("authentication success222");

                                if (auth.getAuthentication().principal.status == 'REGISTERED') {
                                    $location.path("/waiting");
                                }
                                if (auth.getAuthentication().principal.status == 'REJECTED') {
                                    $location.path("/reject");
                                }

                            },
                            function (r) {

                                var data = r.data;

                                if (data.message == 'L') {
                                    $scope.lock = true;
                                } else if (data.message == 'B') {
                                    $scope.error = '아이디, 비밀번호가 올바르지 않습니다. 확인후 다시 입력해주세요.';
                                } else {
                                    $scope.error = data.message;
                                }

                                $log.debug("authentication failure")
                            });

                    }
                },
                controllerAs: "ctrl"
            });

        $logProvider.debugEnabled(true);

        $httpProvider.interceptors.push("authenticateInterceptor");
    })
    .run(function (auth, $rootScope, $log) {

        auth.init('/', '/login', '/logout');

        $rootScope.getAuthentication = auth.getAuthentication;

        $rootScope.isAuthenticated = auth.isAuthenticated;


        $rootScope.logout = function () {
            auth.logout();
        };

        $log.debug("init module : welcome");
    });
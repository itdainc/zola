'use strict';

angular.module("Uploader", ["ngFileUpload"])

    .factory("Uploader", function ($http, $q, Upload) {

        var URL = "http://static2.myitpo.com.s3-ap-northeast-1.amazonaws.com/";

        var Uploader = function (pathGenerator) {

            this.pathGenerator = pathGenerator;
            this.key;

            var self = this;

            $http.get("/key").success(function (r) {

                self.key = r;
            });
        };

        Uploader.prototype.upload = function (file) {

            console.log(file);
            if (file == null) {
                return $q.reject()
            }

            if (!this.key) {
                alert("파일 업로더 초기화 중입니다. 잠시 후 다시 시도해주세요.");
                return $q.reject();

            }
            if (file.$error) {
                if(file.$error == 'maxSize') {
                    alert("이미지 크기는 " + file.$errorParam + "를 초과할 수 없습니다.");
                }
                return $q.reject();
            }

            return Upload.upload({
                url: URL,
                method: 'POST',
                data: {
                    "key": this.pathGenerator(file),
                    "acl": 'public-read',
                    "policy": this.key.stringToSign,
                    "Content-Type": file.type != '' ? file.type : 'application/octet-stream',
                    "x-amz-signature": this.key.signature,
                    "x-amz-date": this.key.date,
                    "x-amz-algorithm": 'AWS4-HMAC-SHA256',
                    "x-amz-credential": this.key.credential,
                    "file": file
                }
            }).then(
                function (r) {

                    return r.config.data.key;

                },
                function (r) {

                    alert("오류 발생");
                    file = null;
                }
            );
        };


        return {
            getInstance: function (pathGenerator) {

                if (!angular.isFunction(pathGenerator)) {
                    throw "logical error";
                }

                return new Uploader(pathGenerator);
            }
        }
    });



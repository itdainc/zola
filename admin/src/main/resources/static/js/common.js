angular.module('common', [])

    .factory('authenticateInterceptor', function ($q, $location, $injector, $log) {

        return {
            'responseError': function (rejection) {

                var status = rejection.status,
                    config = rejection.config,
                    method = config.method,
                    url = config.url;

                if (status == 401) {

                    $log.debug("common.js auth check");

                    var auth = $injector.get('auth');

                    auth.clear();

                    if ($location.path() != auth.path().login) {

                        auth.returnUrl = $location.path();
                        $location.path(auth.path().login);
                    }
                } else {
                    $log.error(method + ' on ' + url + ' failed with status ' + status);
                }

                return $q.reject(rejection);
            }
        };
    })

    .directive('copyToClipboard',  function ($window) {
        var body = angular.element($window.document.body);
        var textarea = angular.element('<textarea/>');
        textarea.css({
            position: 'fixed',
            opacity: '0'
        });

        function copy(toCopy) {
            textarea.val(toCopy);
            body.append(textarea);
            textarea[0].select();

            try {
                var successful = document.execCommand('copy');
                if (!successful) throw successful;
            } catch (err) {
                console.log("failed to copy", toCopy);
            }
            textarea.remove();
        }

        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.bind('click', function (e) {
                    copy(attrs.copyToClipboard);
                });
            }
        }
    })

    .factory('errorInterceptor', function ($q) {

        return {
            'responseError': function (rejection) {

                var status = rejection.status,
                    config = rejection.config,
                    method = config.method,
                    url = config.url;

                // if (status == 500) {
                //
                //     window.alert("시스템 오류입니다. 잠시 후 다시 시도해주세요. 문제가 지속되는 경우 관리자에게 연락 부탁드립니다.");
                // }

                return $q.reject(rejection);
            }
        }
    })

    .directive('autofocus', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            link: function ($scope, $element) {
                $timeout(function () {
                    $element[0].focus();
                });
            }
        }
    }])

    .directive('back', function () {

        return {
            restrict: 'A',

            link: function (scope, element) {

                element.bind('click', function () {

                    history.back();
                    scope.$apply();
                });
            }
        }
    })

    .directive('notSupported', function () {

        return {
            restrict: 'A',

            link: function (scope, element) {

                element.bind('click', function () {

                    alert("추후 지원 예정입니다.");
                });
            }
        }
    })

    .directive('numeric', function () {

        return {
            require: '?ngModel',
            link: function (scope, element, attr, ngModelCtrl) {
                if (!ngModelCtrl) {
                    return;
                }

                ngModelCtrl.$parsers.push(function (val) {
                    if (angular.isUndefined(val)) {
                        val = '';
                    }
                    var clean = val.replace(/[^0-9]+/g, '');
                    if (val !== clean) {
                        ngModelCtrl.$setViewValue(clean);
                        ngModelCtrl.$render();
                    }
                    return clean;
                });

                element.bind('keypress', function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        };
    })


    .factory('Pageable', function () {

        // SpringData aware pageable.
        function DefaultPageable(size) {

            this.page = 1; // current page
            this.size = size; // items per page

            this.maxSize = 10; // max pages
        }

        DefaultPageable.prototype.update = function (data) {

            // todo validation check
            this.page = data.number + 1;
            this.size = data.size;

            this.count = data.totalElements;
        };

        return {

            basic: function (size) {
                return new DefaultPageable(size || 10);
            }
        }
    })

    .directive('modal', function(){
        return {
            restrict: 'EA',
            scope: {
                title: '=modalTitle'
            },
            templateUrl: 'modal/modal_benefit.html',
            controller: function ($scope) {
                $scope.handler = 'pop';
            },
        };
    });

// .directive('numeric', function () {
//
//     return {
//         require: 'ngModel',
//         link: function (scope, elm, attrs, ctrl) {
//
//             ctrl.$asyncValidators.smsAuth = function (modelValue, viewValue) {
//
//                 if (ctrl.$isEmpty(modelValue)) {
//                     // consider empty model valid
//                     return $q.when();
//                 }
//
//                 return $http.post("/partners/sms-auth", {number: modelValue});
//             };
//         }
//     };
// });

// Global Function

function always() {
    return true;
}

function never() {
    return false;
}

function responseExtractor(r) {
    return r.data;
}


function runDaumPost() {
    var width = 500; //팝업의 너비
    var height = 600; //팝업의 높이

    new daum.Postcode({
        oncomplete: function(data) {
            width : width; //팝업의 너비
            height : height; //팝업의 높이

            // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

            // 도로명 주소의 노출 규칙에 따라 주소를 조합한다.
            // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
            var fullRoadAddr = data.roadAddress; // 도로명 주소 변수
            var extraRoadAddr = ''; // 도로명 조합형 주소 변수

            // 법정동명이 있을 경우 추가한다. (법정리는 제외)
            // 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
            if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
                extraRoadAddr += data.bname;
            }
            // 건물명이 있고, 공동주택일 경우 추가한다.
            if(data.buildingName !== '' && data.apartment === 'Y'){
                extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
            }
            // 도로명, 지번 조합형 주소가 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
            if(extraRoadAddr !== ''){
                extraRoadAddr = ' (' + extraRoadAddr + ')';
            }
            // 도로명, 지번 주소의 유무에 따라 해당 조합형 주소를 추가한다.
            if(fullRoadAddr !== ''){
                fullRoadAddr += extraRoadAddr;
            }

            // 우편번호와 주소 정보를 해당 필드에 넣는다.
            document.getElementById('exchange.post').value = data.zonecode; //5자리 새우편번호 사용
            document.getElementById('roadAddress').value = fullRoadAddr;
            document.getElementById('jibunAddress').value = data.jibunAddress;

            // 사용자가 '선택 안함'을 클릭한 경우, 예상 주소라는 표시를 해준다.
            if(data.autoRoadAddress) {
                //예상되는 도로명 주소에 조합형 주소를 추가한다.
                var expRoadAddr = data.autoRoadAddress + extraRoadAddr;
                document.getElementById('guide').innerHTML = '(예상 도로명 주소 : ' + expRoadAddr + ')';

            } else if(data.autoJibunAddress) {
                var expJibunAddr = data.autoJibunAddress;
                document.getElementById('guide').innerHTML = '(예상 지번 주소 : ' + expJibunAddr + ')';

            } else {
                document.getElementById('guide').innerHTML = '';
            }
        }
    }).open({
        left: (window.screen.width / 2) - (width / 2),
        top: (window.screen.height / 2) - (height / 2),
        autoClose: true
    });
}

/*

 <ul click-children='fun' selector='li'>
 <li ng-repeat="s in ss" data-index="{{$index}}">{{s}}</li>
 </ul>
 The function defined is defined in the controller and it expects to be passed an index

 $scope.fun = function(index){
 console.log('hi from controller', index, $scope.ss[index]);
 };
 The directive uses $parse to convert an expression into a function that will be called from the event listener.

 app.directive('clickChildren', function($parse){
 return {
 restrict: 'A',
 link: function(scope, element, attrs){
 var selector = attrs.selector;
 var fun = $parse(attrs.clickChildren);
 element.on('click', selector, function(e){
 // no need to create a jQuery object to get the attribute
 var idx = e.target.getAttribute('data-index');
 fun(scope)(idx);
 });
 }
 };
 });
* */
angular.module("auth", [])

    .factory("auth", function ($http, $rootScope, $q, $log) {

        var path = {
            home: "/",
            login: "/login",
            logout: "/logout"
        };

        var auth;

        var manager = {

            init: function (home, login, logout) {

                path.home = home;
                path.login = login;
                path.logout = logout;
                // manager.authenticate(); // routeChangeSuccess 로 이동
            },

            authenticate: function (credentials, remember, callback) {

                var headers = {},
                    params = {};

                if (!!credentials) {
                    headers.authorization = "Basic " + btoa(credentials.username + ":" + credentials.password);
                    params.remember = !!remember;
                }

                return $http.get("user", {headers: headers, params: params})
                    .then(
                        function (r) {
                            $rootScope.login_state = true;
                            return auth = r.data;
                        },
                        function (r) {
                            $rootScope.login_state = false;
                            manager.clear();
                            return $q.reject(r);
                        }
                    );
            },

            logout: function () {
                return $http.post(path.logout, {}).then(function () {
                    manager.clear();
                });
            },

            clear: function () {
                auth = null;
            },

            isAuthenticated: function () {
                return !!auth;
            },

            path: function () {
                return angular.copy(path);
            },

            getAuthentication : function() {
                return auth;
            }
        };

        return manager;
    });
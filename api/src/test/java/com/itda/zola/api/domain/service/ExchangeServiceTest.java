package com.itda.zola.api.domain.service;

import com.itda.zola.api.adaptor.kakao.KakaoSticker;
import com.itda.zola.domain.model.supplier.Supplier;
import com.itda.zola.domain.model.user.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Rollback(false)
public class ExchangeServiceTest {

    @Autowired
    private ExchangeService exchangeService;
    @Autowired
    private SupplierService supplierService;
    @Autowired
    private UserService userService;

    @Test
    public void exchangeTest() throws Exception {

        User user = userService.findActiveUser(1);
        Supplier supplier = supplierService.find(1);

        exchangeService.exchange(supplier, user, new KakaoSticker("아아아", 1000L, null), "01086442980", "http://daum.net");
    }
}
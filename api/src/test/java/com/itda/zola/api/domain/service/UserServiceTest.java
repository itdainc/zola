package com.itda.zola.api.domain.service;

import com.itda.zola.domain.model.ledger.LedgerType;
import com.itda.zola.domain.model.supplier.Supplier;
import com.itda.zola.domain.model.user.Provider;
import com.itda.zola.domain.model.user.ProviderToken;
import com.itda.zola.domain.model.user.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Rollback
public class UserServiceTest {


    @Autowired
    private UserService userService;
    @Autowired
    private SupplierService supplierService;

    @Test
    public void create() throws Exception {

        User user = new User(new ProviderToken(Provider.KAKAO, "1234233"), "kunsue");
//        userService.create(user, supplierService.findActiveUser(sId));
    }


    @Test
    public void getTotal() throws Exception {

        User user = userService.findActiveUser(5);
        Supplier supplier = supplierService.find("emo-adr");

        Map<LedgerType, Long> totals = userService.getTotal(user, supplier);

        totals.forEach((ledgerType, aLong) -> System.err.println(ledgerType  + " : " + String.valueOf(aLong)));
    }

    @Test
    public void exist_TEST() throws Exception {

        System.err.println(userService.exist("kkunsue"));
    }
}
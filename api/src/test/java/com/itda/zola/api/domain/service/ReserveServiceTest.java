package com.itda.zola.api.domain.service;

import com.itda.zola.api.adaptor.adn.TnkReserveParams;
import com.itda.zola.domain.model.ledger.Account;
import com.itda.zola.domain.model.transaction.reserve.ReserveParams;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
@Rollback(false)
public class ReserveServiceTest {

    @Autowired
    private CpService cpService;
    @Autowired
    private ReserveService reserveService;

    @Test
    public void saving() throws Exception {

        TnkReserveParams params = new TnkReserveParams();
        params.setSeq_id("test23012f3");
        params.setMd_user_nm("5:test");
        params.setPay_pnt(100L);
        params.setApp_id("기분 조아서 적립해줌");

        reserveService.revenueShare(cpService.find("tnk"), params);
    }

    @Test
    public void bonus() throws Exception {

        ReserveParams reserveParams = new ReserveParams() {

            @Override
            public Long getAmount() {
                return 3600L;
            }

            @Override
            public Long getUserId() {
                return 84L;
            }

            @Override
            public String getSupplierCode() {
                return "emo-adr";
            }

            @Override
            public Account getAccount() {
                return Account.R_MAN;
            }

            @Override
            public String getTransactionId() {
                return "manual-" + UUID.randomUUID().toString().substring(0,6);
            }

            @Override
            public String getMemo() {
                return "적립금 빵빵";
            }
        };

        reserveService.revenueShare(cpService.find("internal"), reserveParams);
    }
}
package com.itda.zola.api.presentation.controller;

import com.itda.zola.api.presentation.response.UserResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class HomeControllerTest {

    private static final String sid = "test";
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void check_auth_test() {

        Map<String, String> a = new HashMap<>();
        a.put("provider", "KAKAO");
        a.put("providedId", "14124");
        ResponseEntity<UserResponse> r = this.restTemplate.postForEntity("/SUPPLIER/" + sid + "/CHECK-AUTH/V1", a, UserResponse.class);

        System.err.println(r);
    }
}
package com.itda.zola.api.presentation.controller;

import com.itda.zola.api.presentation.response.JoinResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ExchangeControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void 지급_성공_test() throws Exception {

        final String TEST_API = String.format("/SUPPLIER/%s/USER/%d/EXCHANGE/V1", "test", 5);

        Map<String, String> params = new HashMap<>();
        params.put("type", "KAKAO_EMO");
        params.put("name", "테스트 아이템!");
        params.put("price", "100");
        params.put("receiver", "010-8644-2980");
        params.put("url", "http://daum.net");

        restTemplate.postForEntity(TEST_API, params, JoinResponse.class);
    }
}
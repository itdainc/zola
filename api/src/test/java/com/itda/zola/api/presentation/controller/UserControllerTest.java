package com.itda.zola.api.presentation.controller;

import com.itda.zola.api.presentation.response.ErrorResponse;
import com.itda.zola.api.presentation.response.JoinResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerTest {

    private static final String sid = "test";

    @Autowired
    private TestRestTemplate restTemplate;


    @Test
    public void 가입여부_확인_성공_test() throws Exception {

        final String TEST_API = String.format("/SUPPLIER/%s/CHECK-AUTH/V1", sid);

        Map<String, String> params = new HashMap<>();
        params.put("provider", "KAKAO");
        params.put("providedId", "366735095");
        params.put("firebaseToken", "423874782374");

        restTemplate.postForEntity(TEST_API, params, JoinResponse.class);
    }

    // 이름이 유효하지 않은 경우
    @Test
    public void join_failure_test() throws Exception {

        String rand = UUID.randomUUID().toString().substring(0,6);

        Map<String, String> params = new HashMap<>();
        params.put("provider", "KAKAO");
        params.put("providedId", "test_id_" + rand);
        params.put("name", "123");
        params.put("firebaseToken", "132143243");

        ResponseEntity<ErrorResponse> res = restTemplate.postForEntity("/SUPPLIER/" + sid + "/JOIN/V1", params, ErrorResponse.class);

        System.err.println(res);
    }


    // 레퍼러 없이 가입한 경우
    @Test
    public void join_test() throws Exception {

        String rand = UUID.randomUUID().toString().substring(0,6);

        Map<String, String> params = new HashMap<>();
        params.put("provider", "KAKAO");
        params.put("providedId", "test_id_" + rand);
        params.put("name", "test_id" + rand);
        params.put("firebaseToken", "132143243");

        ResponseEntity<JoinResponse> res = restTemplate.postForEntity("/SUPPLIER/" + sid + "/JOIN/V1", params, JoinResponse.class);

        System.err.println(res.getBody().getWelcomeMessage());
    }


    // 정상적인 레퍼러를 통해 가입한 경우.
    @Test
    public void join_test2() throws Exception {

        String rand = UUID.randomUUID().toString().substring(0,6);

        Map<String, String> params = new HashMap<>();
        params.put("provider", "KAKAO");
        params.put("providedId", "test_id_" + rand);
        params.put("name", "test_id" + rand);
        params.put("referrer", "kkuns");
        params.put("firebaseToken", "132143243");

        ResponseEntity<JoinResponse> res = restTemplate.postForEntity("/SUPPLIER/" + sid + "/JOIN/V1", params, JoinResponse.class);

        System.err.println(res.getBody().getWelcomeMessage());
    }

    // 잘못된 레퍼러로 가입하는 경우.
    // expect -> 가입한 사람에게만 referrer 프로모션 적립.

    @Test
    public void join_test3() throws Exception {

        String rand = UUID.randomUUID().toString().substring(0,6);

        Map<String, String> params = new HashMap<>();
        params.put("provider", "KAKAO");
        params.put("providedId", "test_id_" + rand);
        params.put("name", "test_id" + rand);
        params.put("referrer", "non_exist_name");
        params.put("firebaseToken", "132143243");

        ResponseEntity<JoinResponse> res = restTemplate.postForEntity("/SUPPLIER/" + sid + "/JOIN/V1", params, JoinResponse.class);

        System.err.println(res.getBody().getWelcomeMessage());

    }
}
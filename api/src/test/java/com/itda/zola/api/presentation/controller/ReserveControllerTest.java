package com.itda.zola.api.presentation.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;


@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ReserveControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void TNK_적립_test() throws Exception {

        final String TEST_API = String.format("/CPS/%s/SAVING/V1", "tnk");

        Map<String, String> params = new HashMap<>();
        params.put("seq_id", "1");
        params.put("md_user_nm", "1:test");
        params.put("pay_pnt", "10");
        params.put("md_chk", "1423423423");
        params.put("app_id", "13213");

        restTemplate.postForEntity(TEST_API, params, Void.class);
    }

}
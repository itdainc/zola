package com.itda.zola.api.adaptor.kakao;

import com.itda.zola.domain.model.transaction.exchange.Exchangeable;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

@Slf4j
public class KakaoStickerExchangeableParserTest {

    private final KakaoStickerExchangeableParser parser = new KakaoStickerExchangeableParser();

    @Test
    public void get() throws Exception {

        Exchangeable exchangeable = parser.parse("https://emoticon.kakao.com/items/0ekzvf4ckUj7W9rRKfxe_XwqqBg=?lang=ko&referer=share_etc");
        log.info("name : {}", exchangeable.getName());
        log.info("price : {}", exchangeable.getPrice());
    }

}
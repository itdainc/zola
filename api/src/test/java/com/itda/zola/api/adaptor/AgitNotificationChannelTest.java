package com.itda.zola.api.adaptor;

import com.itda.zola.api.infrastructure.notification.Message;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AgitNotificationChannelTest {

    private AgitNotificationChannel notificationChannel = new AgitNotificationChannel();

    @Test
    public void sendNotiTest() {

        notificationChannel.send(new Message("테스트 메시지"));
    }

}
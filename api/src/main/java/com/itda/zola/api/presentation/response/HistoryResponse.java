package com.itda.zola.api.presentation.response;

import com.itda.zola.domain.model.ledger.Ledger;
import com.itda.zola.domain.model.ledger.LedgerType;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
public class HistoryResponse {

    private LocalDateTime dt;

    private LedgerType type;
    private String typeStr;

    private long price;
    private String name;


    public HistoryResponse(Ledger ledger) {
        this.dt = ledger.getDt();
        this.type = ledger.getType();
        this.typeStr = this.type.getDesc();
        this.price = ledger.getPrice();
        this.name = ledger.getMemo();
    }
}
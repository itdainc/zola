package com.itda.zola.api.presentation.controller;

import com.itda.zola.api.domain.service.SupplierService;
import com.itda.zola.api.presentation.response.SupplierInitResponse;
import com.itda.zola.domain.model.supplier.Supplier;
import com.itda.zola.domain.model.supplier.SupplierRelease;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SupplierController {


    @Autowired
    private SupplierService supplierService;


    //TODO 수정!
    @RequestMapping(path = "/SUPPLIER/{sId}/INIT", method = RequestMethod.GET)
    public ResponseEntity<SupplierInitResponse> FFF(Supplier supplier, @RequestParam Integer version) {

        SupplierRelease latestRelease = supplierService.findLatestReleaseTag(supplier);
        if(latestRelease == null) {
            return ResponseEntity.ok(new SupplierInitResponse(true, false, 0));
        }

        if (latestRelease.getVersion() > version) {
            if (latestRelease.isMandatory()) {
                return ResponseEntity.ok(new SupplierInitResponse(false, true, latestRelease.getVersion()));
            } else {
                return ResponseEntity.ok(new SupplierInitResponse(false, false, latestRelease.getVersion()));
            }
        }

        return ResponseEntity.ok(new SupplierInitResponse(true, false, latestRelease.getVersion()));
    }
}
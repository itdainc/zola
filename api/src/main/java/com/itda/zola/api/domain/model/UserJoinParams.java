package com.itda.zola.api.domain.model;

import com.itda.zola.domain.model.supplier.Supplier;
import com.itda.zola.domain.model.user.ProviderToken;
import com.itda.zola.domain.model.user.User;
import lombok.Getter;

@Getter
public class UserJoinParams {

    private ProviderToken providerToken;

    private Supplier supplier;

    private String name;

    private User referrer;

    private String firebaseToken;

    public UserJoinParams(ProviderToken providerToken, String name, Supplier supplier, String firebaseToken) {
        this.providerToken = providerToken;
        this.supplier = supplier;
        this.name = name;

        this.firebaseToken = firebaseToken;
    }

    public void setReferrer(User referrer) {
        this.referrer = referrer;
    }
}

package com.itda.zola.api;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.itda.zola.CoreApplication;
import com.itda.zola.api.infrastructure.lock.RedisResourceLockManager;
import com.itda.zola.api.infrastructure.lock.ResourceLockManager;
import com.itda.zola.api.infrastructure.lock.user.UserLock;
import com.itda.zola.api.presentation.support.argument.SupplierArgumentResolver;
import com.itda.zola.api.presentation.support.argument.UserArgumentResolver;
import com.itda.zola.api.presentation.support.log.LoggingFilter;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

@ComponentScan(
        includeFilters = @ComponentScan.Filter(Aspect.class)
)
@EntityScan(basePackageClasses = CoreApplication.class)
@SpringBootApplication
public class ApiApplication {

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(ApiApplication.class);
        application.addListeners(new ApplicationPidFileWriter());
        application.run(args);
    }

    // Jackson modules
    @Bean
    public Module hibernate5Module() {
        return new Hibernate5Module();
    }
    @Bean
    public Module javaTimeModule() {
        return new JavaTimeModule();
    }


    // UserLock manager
    @Bean
    public ResourceLockManager<UserLock> userLockManager(StringRedisTemplate redis) {
        return new RedisResourceLockManager<>(redis);
    }

    // Logging Filter
    @Bean
    public FilterRegistrationBean filterRegistrationBean() {

        // http://meetup.toast.com/posts/44
        // https://github.com/librucha/servlet-logging-filter
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();

        LoggingFilter.Builder builder = LoggingFilter.Builder.create();
        builder.loggerName("ioLogger");
        builder.excludedPaths("/health_check.html", "/favicon.ico");

        registrationBean.addUrlPatterns("/*");
        registrationBean.setFilter(new LoggingFilter(builder));

        return registrationBean;
    }

    // MVC Config
    @Bean
    public WebMvcConfigurerAdapter mvcConfig(SupplierArgumentResolver sar, UserArgumentResolver uar) {

        return new WebMvcConfigurerAdapter() {

            @Override
            public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
                argumentResolvers.add(sar);
                argumentResolvers.add(uar);
                super.addArgumentResolvers(argumentResolvers);
            }
        };
    }
}
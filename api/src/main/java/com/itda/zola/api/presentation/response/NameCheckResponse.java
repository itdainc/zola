package com.itda.zola.api.presentation.response;

public class NameCheckResponse {

    private boolean usable;
    private String message;

    private NameCheckResponse(boolean usable, String message) {
        this.usable = usable;
        this.message = message;
    }

    public static NameCheckResponse ok() {
        return new NameCheckResponse(true, null);
    }

    public static NameCheckResponse notUsable(String message) {
        return new NameCheckResponse(false, message);
    }


    // ----------
    // getter

    public boolean isUsable() {
        return usable;
    }

    public String getMessage() {
        return message;
    }
}
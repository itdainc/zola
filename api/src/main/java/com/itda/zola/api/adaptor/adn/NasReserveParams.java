package com.itda.zola.api.adaptor.adn;

import com.itda.zola.domain.model.ledger.Account;
import com.itda.zola.domain.model.transaction.reserve.ReserveParams;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

@Setter
public class NasReserveParams implements ReserveParams {

    @NotBlank
    private String SEQ_ID;

    @NotBlank
    private String USER_DATA;   // userId:supplier

    @NotNull
    private Long REWARD;

    private String AD_ID;
    private String AD_NAME;
    private String AD_TYPE;     // 광고구분 (CPI, CPE, CPA, CPC, FACEBOOK)


    // ----------

    @Override
    public Long getAmount() {
        return REWARD;
    }

    @Override
    public Long getUserId() {
        return Long.valueOf(USER_DATA.split(":")[0]);
    }

    @Override
    public String getSupplierCode() {
        return USER_DATA.split(":")[1];
    }

    @Override
    public Account getAccount() {
        return Account.R_RS;
    }

    @Override
    public String getTransactionId() {
        return SEQ_ID;
    }

    @Override
    public String getMemo() {
        return AD_NAME;
    }
}
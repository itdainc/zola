package com.itda.zola.api.presentation.controller;

import com.itda.zola.api.adaptor.kakao.KakaoSticker;
import com.itda.zola.api.domain.service.ExchangeService;
import com.itda.zola.api.domain.service.ParserMatcher;
import com.itda.zola.api.domain.service.UserService;
import com.itda.zola.api.presentation.request.ExchangeRequest;
import com.itda.zola.api.presentation.response.PendingExchangeResponse;
import com.itda.zola.api.presentation.response.PreExchangeResponse;
import com.itda.zola.domain.model.supplier.Supplier;
import com.itda.zola.domain.model.transaction.exchange.Exchange;
import com.itda.zola.domain.model.transaction.exchange.Exchangeable;
import com.itda.zola.domain.model.user.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j

@RestController
public class ExchangeController {

    private final UserService userService;
    private final ExchangeService exchangeService;

    @Autowired
    public ExchangeController(UserService userService, ExchangeService exchangeService) {
        this.userService = userService;
        this.exchangeService = exchangeService;
    }

    /**
     * 구매 대기중인 건 조회
     *
     * @return PendingExchangeResponse
     */
//    @OpenApi
    @RequestMapping(path = "/SUPPLIER/{sId}/USER/{userId:\\d+}/PENDING-EXCHANGE/V1", method = RequestMethod.GET)
    public ResponseEntity<PendingExchangeResponse> getPendingExchange(Supplier supplier, User user) {

        List<Exchange> exchanges = exchangeService.findPendingExchange(supplier, user);
        return ResponseEntity.ok(new PendingExchangeResponse(exchanges));
    }

    /**
     * 구매 - 예비거래, 정보파싱
     *
     * @return PreExchangeResponse
     */
//    @OpenApi(sign = false)
    @RequestMapping(path = "/SUPPLIER/{sId}/USER/{userId:\\d+}/PRE-EXCHANGE/V1", method = RequestMethod.GET)
    public ResponseEntity<PreExchangeResponse> preExchange(Supplier supplier, User user, @RequestParam String item) {

        Exchangeable exchangeable = ParserMatcher.match(item).parse(item);
        Long balance = userService.getBalance(user, supplier.getRegion());

        return ResponseEntity.ok(new PreExchangeResponse(exchangeable, balance));
    }

    /**
     * 구매 - 본거래
     *
     * @return Void
     */
//    @OpenApi
    @RequestMapping(path = "/SUPPLIER/{sId}/USER/{userId:\\d+}/EXCHANGE/V1", method = RequestMethod.POST)
    public ResponseEntity<Void> exchange(Supplier supplier, User user, @RequestBody ExchangeRequest request) {

        exchangeService.exchange(supplier, user, new KakaoSticker(request.getName(), request.getPrice()), request.getReceiver(), request.getUrl());

        return ResponseEntity.ok().build();
    }
}
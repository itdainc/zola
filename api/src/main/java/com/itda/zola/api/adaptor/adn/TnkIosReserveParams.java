package com.itda.zola.api.adaptor.adn;

import com.google.common.base.Charsets;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;
import com.itda.zola.api.infrastructure.exception.security.UnauthorizedException;
import com.itda.zola.domain.model.ledger.Account;
import com.itda.zola.domain.model.transaction.reserve.ReserveParams;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

@Slf4j

@Setter
public class TnkIosReserveParams implements ReserveParams {

    private static final String APP_KEY = "fe7be1fe7d5809fedd835166552bc153";

    @NotBlank
    private String seq_id;

    @NotBlank
    private String md_user_nm;  // userId:supplier

    @NotNull
    private Long pay_pnt;

    private String md_chk;      // app_key + md_user_nm + seq_id 의 MD5 Hash
    private String app_id;


    // ----------

    @Override
    public Long getAmount() {
        return pay_pnt;
    }

    @Override
    public Long getUserId() {
        return Long.valueOf(md_user_nm.split(":")[0]);
    }

    @Override
    public String getSupplierCode() {
        return md_user_nm.split(":")[1];
    }

    @Override
    public Account getAccount() {
        return Account.R_RS;
    }

    @Override
    public String getTransactionId() {
        return seq_id;
    }

    @Override
    public String getMemo() {
        return "충전소1 광고참여";
    }


    public void checkSignature() {

        String verifyCode = generateHash(APP_KEY + md_user_nm + seq_id);

        if (md_chk == null || !md_chk.equals(verifyCode)) {
            // 오류
            log.error("tnkad() check error : " + verifyCode + " != " + md_chk);
            throw new UnauthorizedException("signature failure");
        }

        log.info("signature success");
    }

    private static String generateHash(String string) {
        HashCode hc = Hashing.md5().newHasher().putString(string, Charsets.UTF_8).hash();
        return hc.toString();
    }
}
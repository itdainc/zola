package com.itda.zola.api.presentation;

import com.itda.zola.api.infrastructure.notification.ErrorReportingChannel;
import com.itda.zola.api.infrastructure.exception.client.AbstractClientException;
import com.itda.zola.api.infrastructure.exception.resource.AbstractResourceException;
import com.itda.zola.api.infrastructure.exception.security.AbstractSecurityException;
import com.itda.zola.api.infrastructure.notification.Message;
import com.itda.zola.api.presentation.response.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

@Slf4j
@ControllerAdvice
public class RestControllerAdvice {

    @Autowired
    private ErrorReportingChannel channel;

    @ExceptionHandler(AbstractClientException.class)
    public ResponseEntity<ErrorResponse> handleClientException(AbstractClientException e) {

        log.error("client exception : {}", e.getMessage());
        return ResponseEntity.badRequest().body(new ErrorResponse(e.getMessage()));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponse> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {

        log.error("binding exception : {}", e.getMessage());
        channel.send(new Message("PARAMETER BINDING EXCEPTION \n" + makeStackTrace(e)));

        String message = "요청에 올바르지 않은 입력값이 포함되어 있습니다.";
        FieldError firstError = e.getBindingResult().getFieldError();
        if (firstError != null) {
            message = firstError.getDefaultMessage();
        }
        return ResponseEntity.badRequest().body(new ErrorResponse(message));
    }

    @ExceptionHandler(AbstractResourceException.class)
    public ResponseEntity<ErrorResponse> handleResourceException(AbstractResourceException e) {

        log.error("resource exception : {}", e.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(e.getMessage()));
    }

    @ExceptionHandler(AbstractSecurityException.class)
    public ResponseEntity<ErrorResponse> handleSecurityException(AbstractSecurityException e) {

        log.error("security exception :", e);
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new ErrorResponse(e.getMessage()));
    }


    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> handleException(Exception e) {
        log.error("handle exception : ", e);
        channel.send(new Message("SYSTEM FATAL EXCEPTION \n" + makeStackTrace(e)));
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse("시스템 오류입니다. 잠시 후 다시 시도해주세요."));
    }

    public static String makeStackTrace(Throwable t) {
        if (t == null) return "";
        try {
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            t.printStackTrace(new PrintStream(bout));
            bout.flush();
            return new String(bout.toByteArray());
        } catch (Exception ex) {
            return "";
        }
    }
}
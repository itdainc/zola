package com.itda.zola.api.presentation;

import com.google.common.base.Strings;
import com.itda.zola.domain.model.cp.Cp;
import com.itda.zola.api.domain.service.CpService;
import com.itda.zola.api.infrastructure.exception.client.BadRequestException;
import com.itda.zola.api.infrastructure.exception.security.UnauthorizedException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Slf4j

@Component
public class IpFilter implements Filter {

    private final CpService CP_SERVICE;

    @Autowired
    public IpFilter(CpService cpService) {
        this.CP_SERVICE = cpService;
    }

    @Override
    public void check(HttpServletRequest request) {

        log.info("IP FILTER CALLED");

        String remoteIp = getRemoteIp(request);
        String cpId = extractPathVariables(request).get("cpId");

        log.info("cpId = {}, remoteIp = {}", cpId, remoteIp);

        if (Strings.isNullOrEmpty(cpId)) {
            throw new BadRequestException("cpId 값이 잘못되었습니다.");
        }

        Cp cp;
        try {
            cp = CP_SERVICE.find(cpId);
        } catch (RuntimeException e) {
            throw new BadRequestException("정상적인 cp 가 아닙니다.");
        }

        if (!cp.isAccessible(remoteIp)) {
            throw new UnauthorizedException("");//"권한없음!"
        }

        log.info("Access granted for cpId = {}, remoteIp = {}", cpId, remoteIp);
    }

    @Override
    public int getOrder() {
        return 2;
    }


    public static String getRemoteIp(HttpServletRequest request) {

        String addr = request.getHeader("x-forwarded-for");
        if(!Strings.isNullOrEmpty(addr)) {
            addr = addr.split(",")[0].trim();
        } else {
            addr = request.getRemoteAddr();
        }

        log.info("getHostAddress addr : {}", addr);
        return addr;
    }

    @SuppressWarnings("unchecked")
    public static Map<String, String> extractPathVariables(HttpServletRequest request) {

        return (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
    }
}
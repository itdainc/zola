package com.itda.zola.api.presentation;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.common.base.Strings;
import com.itda.zola.api.infrastructure.lock.ResourceLockManager;
import com.itda.zola.api.infrastructure.lock.user.UserLock;
import com.itda.zola.api.infrastructure.util.HttpUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@Aspect
public class ApiAspect {

    private static final ObjectMapper mapper;

    static {
        mapper = new ObjectMapper();
        mapper.registerModule(new Hibernate5Module())
                .registerModule(new JavaTimeModule());
    }

    private final HttpServletRequest httpServletRequest;

    private final CpFilter cpFilter;
    private final IpFilter ipFilter;
    private final SupplierFilter supplierFilter;

    private final ResourceLockManager<UserLock> resourceLockManager;

    @Autowired
    public ApiAspect(CpFilter cpFilter, IpFilter ipFilter, SupplierFilter supplierFilter, HttpServletRequest httpServletRequest, ResourceLockManager<UserLock> resourceLockManager) {
        this.cpFilter = cpFilter;
        this.ipFilter = ipFilter;
        this.supplierFilter = supplierFilter;
        this.httpServletRequest = httpServletRequest;
        this.resourceLockManager = resourceLockManager;
    }

    @Before(value = "@annotation(closed) && @annotation(requestMapping)", argNames = "closed,requestMapping")
    public void doLogging(Closed closed, RequestMapping requestMapping) throws Throwable {

        if (closed.ipFiltering()) {
            ipFilter.check(httpServletRequest);
        }
    }

    @Before(value = "@annotation(requestMapping)", argNames = "requestMapping")
    public void auth(RequestMapping requestMapping) throws Throwable {

        String supplierId = HttpUtil.getPathVariable(httpServletRequest, "sId");
        String userId = HttpUtil.getPathVariable(httpServletRequest, "userId");

        if (!Strings.isNullOrEmpty(userId) && !"GET".equalsIgnoreCase(httpServletRequest.getMethod())) {

//            .info("lock(before request) : cpId=" + cpId + ", userId=" + userId);

            UserLock newLock = new UserLock(supplierId, Long.valueOf(userId));

            resourceLockManager.acquire(newLock);

            httpServletRequest.setAttribute(UserLock.class.getName(), newLock );
        }
    }

    @AfterReturning(value = "@annotation(requestMapping)")
    public void afterReturning(RequestMapping requestMapping) {

        this.releaseLock();
    }

    @AfterThrowing(pointcut = "@annotation(requestMapping)")
    public void afterThrowing(RequestMapping requestMapping) {

        this.releaseLock();
    }

    private Object proceedWithLogging(ProceedingJoinPoint joinPoint, String... url) throws Throwable {

//        logRequest(joinPoint, url);

        //        logResponse(response);
        return joinPoint.proceed();
    }

    private void releaseLock() {

        UserLock lock = (UserLock) httpServletRequest.getAttribute(UserLock.class.getName());

        if (lock != null) {

            resourceLockManager.release(lock);
            httpServletRequest.removeAttribute(UserLock.class.getName());
        }
    }
}
package com.itda.zola.api.presentation.request;

import com.itda.zola.domain.model.user.Provider;
import com.itda.zola.domain.model.user.ProviderToken;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

public class JoinRequest {

    @NotNull
    private Provider provider;
    @NotBlank(message = "카카오 로그인이 정상적으로 이루어지지 않았습니다.")
    private String providedId;

    @Length(min = 4, max = 16, message = "이름은 4~16자의 영문, 숫자를 입력해주세요.")
    @NotBlank(message = "이름은 필수값 입니다.")
    private String name;

    @Length(min = 4, max = 16, message = "추천인 아이디는 4~16자의 영문 숫자를 입력해주세요.")
    private String referrer;

    @NotBlank(message = "push token 이 정상적으로 설정되지 않았습니다.")
    private String firebaseToken;

    // ----------

    public ProviderToken getProviderToken() {
        return new ProviderToken(provider, providedId);
    }

    public String getName() {
        return name;
    }

    public String getReferrer() {
        return referrer;
    }

    // ----------

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public void setProvidedId(String providedId) {
        this.providedId = providedId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setReferrer(String referrer) {
        this.referrer = referrer;
    }

    public String getFirebaseToken() {
        return firebaseToken;
    }

    public void setFirebaseToken(String firebaseToken) {
        this.firebaseToken = firebaseToken;
    }
}
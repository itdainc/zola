package com.itda.zola.api.adaptor.database;

import com.itda.zola.domain.model.supplier.Supplier;
import com.itda.zola.domain.model.supplier.SupplierRelease;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SupplierReleaseRepository extends JpaRepository<SupplierRelease, Long>{

    SupplierRelease findTopBySupplierOrderByIdDesc(Supplier supplier);
}

package com.itda.zola.api.infrastructure.lock;

public interface ResourceLockManager<L extends Lock> {

    void acquire(L lock);

    void release(L lock);
}
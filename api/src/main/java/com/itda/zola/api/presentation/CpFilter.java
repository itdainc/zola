package com.itda.zola.api.presentation;

import com.google.common.base.Strings;
import com.itda.zola.domain.model.cp.Cp;
import com.itda.zola.api.domain.service.CpService;
import com.itda.zola.api.infrastructure.exception.client.BadRequestException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Slf4j

@Component
public class CpFilter implements Filter {

    private final CpService CP_SERVICE;

    @Autowired
    public CpFilter(CpService cpService) {
        this.CP_SERVICE = cpService;
    }

    @Override
    public void check(HttpServletRequest request) {

        log.info("CP FILTER CALLED");

        String cpId = extractPathVariables(request).get("cpId");

        log.info("CPID = {}", cpId);

        if (Strings.isNullOrEmpty(cpId)) {
            throw new BadRequestException("cpId 값이 잘못되었습니다.");
        }

        try {
            Cp cp = CP_SERVICE.find(cpId);
            // TODO validate cp
        } catch (RuntimeException e) {
            throw new BadRequestException("정상적인 cp 가 아닙니다.");
        }
    }


    @SuppressWarnings("unchecked")
    public static Map<String, String> extractPathVariables(HttpServletRequest request) {

        return (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
    }


    @Override
    public int getOrder() {
        return 1;
    }
}
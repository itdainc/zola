package com.itda.zola.api.domain.model;

import com.itda.zola.domain.model.ledger.Account;
import com.itda.zola.domain.model.transaction.reserve.ReserveParams;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class RecommenderReserveParams implements ReserveParams {

    private Long amount;

    private Long userId;
    private String supplierCode;

    private String transactionId;

    private String memo;

    public RecommenderReserveParams(Long userId, Long amount, String supplierCode, Long recommendedId, String recommededName) {
        this.userId = userId;
        this.amount = amount;
        this.supplierCode = supplierCode;
        this.transactionId = "rcmder-" + userId + "-" + recommendedId;

        this.memo = recommededName + "님의 신규가입" ;
    }

    @Override
    public Long getAmount() {
        return amount;
    }

    @Override
    public Long getUserId() {
        return userId;
    }

    @Override
    public String getSupplierCode() {
        return supplierCode;
    }

    @Override
    public Account getAccount() {
        return Account.R_RECM;
    }

    @Override
    public String getTransactionId() {
        return transactionId;
    }

    @Override
    public String getMemo() {
        return memo;
    }
}

package com.itda.zola.api.domain.service;

import com.itda.zola.api.adaptor.database.UserRepository;
import com.itda.zola.api.domain.model.UserJoinParams;
import com.itda.zola.api.infrastructure.exception.resource.ResourceDuplicatedException;
import com.itda.zola.api.infrastructure.exception.resource.ResourceNotFoundException;
import com.itda.zola.domain.model.ledger.Ledger;
import com.itda.zola.domain.model.ledger.LedgerType;
import com.itda.zola.domain.model.supplier.Supplier;
import com.itda.zola.domain.model.user.ProviderToken;
import com.itda.zola.domain.model.user.Region;
import com.itda.zola.domain.model.user.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

@Slf4j

@Service
public class UserService {

    private final UserRepository userRepository;
    private final LedgerService ledgerService;
    private final ApplicationEventPublisher publisher;


    @Autowired
    public UserService(UserRepository userRepository, LedgerService ledgerService, ApplicationEventPublisher publisher) {
        this.userRepository = userRepository;
        this.ledgerService = ledgerService;
        this.publisher = publisher;
    }

    @Transactional
    public User join(final UserJoinParams params) {

        log.info("attempt to create a user.");

        // 기가입 체크
        User user = userRepository.findTopByProviderTokenOrderByRegisteredAtDesc(params.getProviderToken());

        // 가입 상태이거나, 탈퇴 하고 일정기간이 지나지 않은 유저이면 오류
        if (user != null && (user.getExpiredAt() == null || user.getExpiredAt().plusDays(User.REJOIN_INTERVAL_DAYS).isAfter(LocalDateTime.now()))) {

            log.error("이미 가입된 계정이거나 탈퇴 후 일정기간이 지나지 않았습니다. token = {}", params.getProviderToken());
            String message = "이미 가입된 계정이거나 탈퇴 후 " + User.REJOIN_INTERVAL_DAYS + "일이 지나지 않았습니다.";
            if (user.hasExpired()) {
                message += ("\n탈퇴 일시 = " + user.getExpiredAt().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
            }

            throw new ResourceDuplicatedException(message);
        }

        user = new User(params.getProviderToken(), params.getName(), params.getReferrer());
        user.setFirebaseToken(params.getFirebaseToken());

        userRepository.save(user);

        log.info("user successfully created.");

//        publisher.publishEvent(new UserCreationEvent(user, params.getSupplier()));
        return user;
    }

    @Transactional
    public void update(User user) {
        userRepository.save(user);
    }

    @Transactional
    public void leave(final long id) {

        log.info("leave user. id = {}", id);

        User user = findActiveUser(id);
        user.leave();

        log.info("expire user = {}", user);
    }

    public User findActiveUser(final long id) {

        log.info("find active user by id. id = {}", id);

        User user = userRepository.findByIdAndExpiredAtIsNull(id);
        if (user == null) {
            log.error("user not found. id = {}", id);
            throw new ResourceNotFoundException("user not found. id = " + id);
        }

        log.info("user = {}", user);

        return user;
    }

    public User findActiveUser(final String name) {

        log.info("find active user by name. name = {}", name);

        User user = userRepository.findByNameAndExpiredAtIsNull(name);
        if (user == null) {
            log.error("user not found. name = {}", name);
            throw new ResourceNotFoundException("user not found. name = " + name);
        }

        log.info("user = {}", user);

        return user;
    }

    public User findActiveUser(final ProviderToken token) {

        log.info("find active user by token. token = {}", token);

        User user = userRepository.findByProviderTokenAndExpiredAtIsNull(token);
        if (user == null) {
            log.error("user not found. token = {}", token);
            throw new ResourceNotFoundException("user not found. token = " + token);
        }

        log.info("user = {}", user);

        return user;
    }

    public boolean exist(final String name) {

//        Example<User> eu = Example.of(new User(null, name), ExampleMatcher.matching().withIgnorePaths("registeredAt"));
        return userRepository.findByName(name).isPresent();
    }

    public List<Ledger> getLedgers(User user, Region region) {
        return ledgerService.getLedgers(user, region);
    }

    public Long getBalance(User user, Region region) {

        return ledgerService.calculateBalance(user, region);
    }

    public Map<LedgerType, Long> getTotal(User user, Supplier supplier) {

        return ledgerService.getTotal(user, supplier);
    }
}
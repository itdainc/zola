package com.itda.zola.api.infrastructure.exception.security;

import com.itda.zola.api.infrastructure.exception.CoreException;

public class AbstractSecurityException extends CoreException {

    public AbstractSecurityException(String message) {
        super(message);
    }

    public AbstractSecurityException(String message, Throwable cause) {
        super(message, cause);
    }
}
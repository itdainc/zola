package com.itda.zola.api.presentation.controller;

import com.itda.zola.api.domain.service.*;
import com.itda.zola.api.infrastructure.exception.client.BadRequestException;
import com.itda.zola.api.infrastructure.exception.resource.ResourceDuplicatedException;
import com.itda.zola.api.infrastructure.exception.resource.ResourceNotFoundException;
import com.itda.zola.api.presentation.OpenApi;
import com.itda.zola.api.domain.model.AttendanceReserveParams;
import com.itda.zola.api.presentation.response.AttendanceResponse;
import com.itda.zola.api.presentation.response.ErrorResponse;
import com.itda.zola.domain.model.promotion.Promotion;
import com.itda.zola.domain.model.supplier.Supplier;
import com.itda.zola.domain.model.transaction.reserve.Reserve;
import com.itda.zola.domain.model.transaction.reserve.ReserveParams;
import com.itda.zola.domain.model.user.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@Slf4j

@RestController
public class AttendanceController {

    private final PromotionService promotionService;
    private final CpService cpService;
    private final ReserveService reserveService;

    @Autowired
    public AttendanceController(PromotionService ps, CpService cs, ReserveService rs) {

        this.promotionService = ps;
        this.cpService = cs;
        this.reserveService = rs;
    }

    /**
     * 출석체크
     *
     * @return AttendanceResponse
     */
    @OpenApi
    @RequestMapping(path = "/SUPPLIER/{sId}/USER/{userId:\\d+}/ATTENDANCE/V1", method = RequestMethod.POST)
    public ResponseEntity<AttendanceResponse> attendance(Supplier supplier, User user) {

        Promotion promotion = promotionService.findOngoingAttendancePromotion(supplier);

        if (user.getRegisteredAt().toLocalDate().isEqual(LocalDate.now())) {
            throw new BadRequestException("가입 당일에는 출석이벤트에서 제외 됩니다.");
        }

        ReserveParams params = AttendanceReserveParams.of(user.getId(), supplier.getCode(), promotion.getAmount());

        Reserve reserve = reserveService.bonus(params);

        return ResponseEntity.ok(new AttendanceResponse(reserve));
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleResourceNotFoundException(ResourceNotFoundException e) {
        return ResponseEntity.badRequest().body(new ErrorResponse(e.getMessage()));
    }

    @ExceptionHandler(ResourceDuplicatedException.class)
    public ResponseEntity<ErrorResponse> handleResourceDuplicatedException(ResourceDuplicatedException e) {
        return ResponseEntity.badRequest().body(new ErrorResponse("오늘은 이미 출석체크를 완료했습니다."));
    }
}
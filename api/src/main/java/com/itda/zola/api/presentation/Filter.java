package com.itda.zola.api.presentation;

import javax.servlet.http.HttpServletRequest;

public interface Filter {

    void check(HttpServletRequest request);

    int getOrder();
}
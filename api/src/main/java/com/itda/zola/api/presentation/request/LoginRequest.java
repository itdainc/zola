package com.itda.zola.api.presentation.request;

import com.itda.zola.domain.model.user.Provider;
import com.itda.zola.domain.model.user.ProviderToken;
import lombok.Getter;
import org.hibernate.validator.constraints.NotBlank;

@Getter
public class LoginRequest {

    private Provider provider;
    private String providedId;
    @NotBlank
    private String firebaseToken;

    public ProviderToken getProviderToken() {
        return new ProviderToken(provider, providedId);
    }
}
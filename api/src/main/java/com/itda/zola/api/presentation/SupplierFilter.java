package com.itda.zola.api.presentation;

import com.google.common.base.Strings;
import com.itda.zola.api.infrastructure.util.HttpUtil;
import com.itda.zola.domain.model.supplier.Supplier;
import com.itda.zola.api.domain.service.SupplierService;
import com.itda.zola.api.infrastructure.exception.client.BadRequestException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Slf4j

@Component
public class SupplierFilter implements Filter {

    private final SupplierService supplierService;

    @Autowired
    public SupplierFilter(SupplierService cpService) {
        this.supplierService = cpService;
    }

    @Override
    public void check(HttpServletRequest request) {

        log.info("SUPPLIER FILTER CALLED");

        String sId = HttpUtil.getPathVariable(request, "sId");

        log.info("SID = {}", sId);

        if (Strings.isNullOrEmpty(sId)) {
            throw new BadRequestException("sId 값이 잘못되었습니다.");
        }

        try {
            Supplier supplier = supplierService.find(sId);
            // TODO validate cp
        } catch (RuntimeException e) {
            throw new BadRequestException("정상적인 supplier 가 아닙니다.");
        }
    }

    @Override
    public int getOrder() {
        return 0;
    }

}

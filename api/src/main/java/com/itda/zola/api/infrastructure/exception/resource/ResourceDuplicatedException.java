package com.itda.zola.api.infrastructure.exception.resource;

public class ResourceDuplicatedException extends AbstractResourceException {

    public ResourceDuplicatedException(String message) {
        super(message);
    }

    public ResourceDuplicatedException(String message, Throwable cause) {
        super(message, cause);
    }
}
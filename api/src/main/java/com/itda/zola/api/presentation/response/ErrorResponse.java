package com.itda.zola.api.presentation.response;

import com.google.common.base.MoreObjects;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor

@Getter
public class ErrorResponse {

    private String code;
    private String message;

    public ErrorResponse(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("code", code)
                .add("message", message)
                .toString();
    }
}
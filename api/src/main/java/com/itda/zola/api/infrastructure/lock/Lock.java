package com.itda.zola.api.infrastructure.lock;

public interface Lock<K, V> {

    K getKey();

    V getValue();

    int lockTimeout();
}
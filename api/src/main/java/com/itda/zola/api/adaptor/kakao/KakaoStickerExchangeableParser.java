package com.itda.zola.api.adaptor.kakao;

import com.itda.zola.domain.model.transaction.exchange.Exchangeable;
import com.itda.zola.domain.service.ExchangeableParser;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class KakaoStickerExchangeableParser implements ExchangeableParser {

    private static final int CONNECTION_TIMEOUT = 5000;

    private static final String FAKE_USER_AGENT = "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Mobile Safari/537.36";
    private static final String DESTINATION_URL = "https://e.kakao.com/store/detail?item_code=";

    private static final Pattern PATTERN_OBTAIN_CODE = Pattern.compile("(emoticon|package)/(?<code>\\d+)");

    @Override
    public boolean support(final String url) {
        return url.contains("//emoticon.kakao.com/items/");
    }

    @Override
    public Exchangeable parse(final String url) {

        try {
            log.info("parse kakao sticker");

            // PHASE 1
            log.debug("connect to bridge. url = [{}]", url);

            Document bridge = Jsoup.connect(url).timeout(CONNECTION_TIMEOUT).userAgent(FAKE_USER_AGENT).get();

            log.debug("connection successful");
            log.debug("find a app scheme");

            Element elem = bridge.getElementById("app_scheme_link");
            if (elem == null) {
                log.error("app scheme element not found.");
                throw new Exception("app scheme element not found.");
            }

            String appScheme = elem.attr("data-url");

            log.debug("app scheme = [{}]", appScheme);

            log.debug("obtain item code from the app scheme");

            Matcher matcher = PATTERN_OBTAIN_CODE.matcher(appScheme);
            if (!matcher.find()) {
                log.error("cannot obtain code from the app scheme");
                throw new Exception("cannot obtain code from the app scheme");
            }
            String code = matcher.group("code");

            log.debug("obtained code = {}", code);

            // PHASE 2
            log.debug("connect to destination. url = [{}]", DESTINATION_URL + code);

            Document destination = Jsoup.connect(DESTINATION_URL + code).timeout(CONNECTION_TIMEOUT).get();

            log.debug("connection successful");

            // remove unnecessary info
            destination.getElementsByClass("txt_author").remove();

            log.debug("obtain exchangeable");

            Element nameElem = destination.getElementsByClass("tit_product").first();
            if (nameElem == null) {
                log.error("name element not found.");
                throw new Exception("name element not found.");
            }
            String name = nameElem.text();

            Element priceElem = destination.getElementsByClass("txt_price").first();
            if (priceElem == null) {
                log.error("price element not found.");
                throw new Exception("price element not found.");
            }

            String priceStr = priceElem.text();
            Long price = Long.valueOf(priceStr.replaceAll("\\D", ""));

            Element imageElem = destination.getElementsByClass("title_img").first();
            if (imageElem == null) {
                log.error("image element not found.");
                throw new Exception("image element not found.");
            }
            String image = imageElem.attr("src");

            Exchangeable exchangeable = new KakaoSticker(name, price, image);

            log.info("parsed kakao sticker = {}", exchangeable);

            return exchangeable;

        } catch (Exception e) {
            log.error("parse error. exception.", e);
            throw new RuntimeException("상품정보를 가져오는 중 오류가 발생했습니다.");
        }
    }

    @Override
    public String toString() {
        return "KAKAO STICKER PARSER";
    }
}
package com.itda.zola.api.domain.service;

import com.itda.zola.api.adaptor.database.ReserveRepository;
import com.itda.zola.api.infrastructure.exception.resource.ResourceDuplicatedException;
import com.itda.zola.domain.model.cp.Cp;
import com.itda.zola.domain.model.transaction.reserve.Reserve;
import com.itda.zola.domain.model.transaction.reserve.ReserveParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Slf4j

@Service
public class ReserveService {

    private final ReserveRepository reserveRepository;

    private final CpService cpService;
    private final UserService USER_SERVICE;
    private final SupplierService SUPPLIER_SERVICE;


    @Autowired
    public ReserveService(SupplierService ss, CpService cpService, UserService us, ReserveRepository rr) {

        this.reserveRepository = rr;

        this.USER_SERVICE = us;
        this.SUPPLIER_SERVICE = ss;

        this.cpService = cpService;
    }

    @Transactional
    public Reserve revenueShare(Cp source, ReserveParams params) {

        Reserve reserve = Reserve.revenueShare(
                source,
                params.getTransactionId(),
                USER_SERVICE.findActiveUser(params.getUserId()),
                SUPPLIER_SERVICE.find(params.getSupplierCode()),
                params.getAccount(),
                params.getAmount(),
                params.getMemo());

        return reserve(reserve);
    }

    @Transactional
    public Reserve bonus(ReserveParams params) {

        Reserve reserve = Reserve.bonus(
                cpService.find("internal"),
                params.getTransactionId(),
                USER_SERVICE.findActiveUser(params.getUserId()),
                SUPPLIER_SERVICE.find(params.getSupplierCode()),
                params.getAccount(),
                params.getAmount(),
                params.getMemo());

        return reserve(reserve);
    }

    private Reserve reserve(Reserve reserve) {

        // FIXME cp 비교할때 id 만 비교하도록 해야한다.
        if (reserveRepository.exists(reserve.getUniqueCheckExample())) {
            log.warn("이미 적립된 transaction id 입니다. reserve = {}", reserve);
            throw new ResourceDuplicatedException("이미 적립된 건 입니다.");
        }

        try {
            return reserveRepository.save(reserve);
        } catch (DataIntegrityViolationException e) {
            // FIXME 다른 제약조건에 걸렸을때는 제외하고 uindex 에 걸린것만 해야한다.
            log.error("이미 적립된 transaction id 입니다. reserve = {}", reserve);
            throw new ResourceDuplicatedException("이미 적립된 건 입니다.");
        }

    }
}
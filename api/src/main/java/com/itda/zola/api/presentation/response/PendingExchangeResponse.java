package com.itda.zola.api.presentation.response;

import com.itda.zola.domain.model.transaction.exchange.Exchange;
import lombok.Getter;

import java.util.List;

@Getter
public class PendingExchangeResponse {

    private int count;

    public PendingExchangeResponse(List<Exchange> exchanges) {

        this.count = exchanges.size();
    }
}

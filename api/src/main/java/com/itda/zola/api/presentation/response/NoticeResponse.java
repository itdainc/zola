package com.itda.zola.api.presentation.response;

import com.itda.zola.domain.model.notice.Notice;
import lombok.Getter;

@Getter
public class NoticeResponse {

    private final long id;

    private final String title;

    private final String url;

    public NoticeResponse(Notice notice) {
        this.id = notice.getId();
        this.title = notice.getTitle();
        this.url = notice.getUrl();
    }
}
package com.itda.zola.api.infrastructure.exception.resource;

import com.itda.zola.api.infrastructure.exception.CoreException;

public abstract class AbstractResourceException extends CoreException {

    public AbstractResourceException(String message) {
        super(message);
    }

    public AbstractResourceException(String message, Throwable cause) {
        super(message, cause);
    }
}
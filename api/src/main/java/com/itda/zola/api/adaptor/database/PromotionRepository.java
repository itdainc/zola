package com.itda.zola.api.adaptor.database;

import com.itda.zola.domain.model.promotion.Promotion;
import com.itda.zola.domain.model.promotion.PromotionType;
import com.itda.zola.domain.model.supplier.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PromotionRepository extends JpaRepository<Promotion, Long> {

    @Query("SELECT p FROM Promotion p WHERE CURRENT_TIMESTAMP BETWEEN p.since AND p.until AND p.supplier = ?1 AND p.type = ?2")
    Promotion findOngoingPromotionBySupplierAndType(Supplier supplier, PromotionType type);
}
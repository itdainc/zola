package com.itda.zola.api.domain.service;

import com.itda.zola.api.adaptor.database.CpRepository;
import com.itda.zola.api.infrastructure.exception.resource.ResourceNotFoundException;
import com.itda.zola.domain.model.cp.Cp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CpService {

    private final CpRepository cpRepository;

    @Autowired
    public CpService(CpRepository cpRepository) {
        this.cpRepository = cpRepository;
    }

    public Cp find(String code) {

        Optional<Cp> optional = cpRepository.findByCode(code);
        if (!optional.isPresent()) {
            throw new ResourceNotFoundException("cp not found. code = " + code);
        }
        return optional.get();
    }
}
package com.itda.zola.api.presentation;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface OpenApi {

    boolean sign() default true;

    Log log() default Log.ALL;


    enum Log {
        ALL, REQUEST, RESPONSE
    }
}

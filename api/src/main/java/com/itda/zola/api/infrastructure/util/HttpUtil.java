package com.itda.zola.api.infrastructure.util;

import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public class HttpUtil {

    @SuppressWarnings("unchecked")
    public static Map<String, String> extractPathVariables(HttpServletRequest request) {

        return (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
    }

    public static String getPathVariable(HttpServletRequest request, String name) {

        return extractPathVariables(request).get(name);
    }
}
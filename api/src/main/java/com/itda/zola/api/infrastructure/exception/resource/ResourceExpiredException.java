package com.itda.zola.api.infrastructure.exception.resource;

public class ResourceExpiredException extends AbstractResourceException {

    public ResourceExpiredException(String message) {
        super(message);
    }

    public ResourceExpiredException(String message, Throwable cause) {
        super(message, cause);
    }
}
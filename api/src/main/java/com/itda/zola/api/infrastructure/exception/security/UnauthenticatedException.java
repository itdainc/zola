package com.itda.zola.api.infrastructure.exception.security;

public class UnauthenticatedException extends AbstractSecurityException {

    public UnauthenticatedException(String message) {
        super(message);
    }

    public UnauthenticatedException(String message, Throwable cause) {
        super(message, cause);
    }
}
package com.itda.zola.api.domain.model;

import com.itda.zola.domain.model.ledger.Account;
import com.itda.zola.domain.model.transaction.reserve.ReserveParams;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class AttendanceReserveParams implements ReserveParams {

    private Long amount;

    private Long userId;
    private String supplierCode;

    private String transactionId;

    private String memo;

    private AttendanceReserveParams(Long userId, Long amount, String supplierCode) {
        this.userId = userId;
        this.amount = amount;
        this.supplierCode = supplierCode;
        this.transactionId = "att-" + userId + "-" + LocalDate.now().format(DateTimeFormatter.BASIC_ISO_DATE);

        this.memo = LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE) + " 출석";
    }

    public static AttendanceReserveParams of(Long userId, String supplierCode, Long amount) {
        return new AttendanceReserveParams(userId, amount, supplierCode);
    }

    @Override
    public Long getAmount() {
        return amount;
    }

    @Override
    public Long getUserId() {
        return userId;
    }

    @Override
    public String getSupplierCode() {
        return supplierCode;
    }

    @Override
    public Account getAccount() {
        return Account.R_ATT;
    }

    @Override
    public String getTransactionId() {
        return transactionId;
    }

    @Override
    public String getMemo() {
        return memo;
    }
}

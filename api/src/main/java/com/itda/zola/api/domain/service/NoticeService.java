package com.itda.zola.api.domain.service;

import com.itda.zola.api.adaptor.database.NoticeRepository;
import com.itda.zola.api.infrastructure.exception.resource.ResourceNotFoundException;
import com.itda.zola.domain.model.notice.Notice;
import com.itda.zola.domain.model.supplier.Supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NoticeService {

    private final NoticeRepository noticeRepository;

    @Autowired
    public NoticeService(NoticeRepository noticeRepository) {
        this.noticeRepository = noticeRepository;
    }

    public Notice findLatest(Supplier supplier) {

        Notice notice = noticeRepository.findFirstBySupplierOrderByIdDesc(supplier);
        if( notice == null) {
            throw new ResourceNotFoundException("there is no notice");
        }
        return notice;
    }
}

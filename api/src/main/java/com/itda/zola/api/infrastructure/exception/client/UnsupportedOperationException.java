package com.itda.zola.api.infrastructure.exception.client;

public class UnsupportedOperationException extends AbstractClientException {

    public UnsupportedOperationException(String message) {
        super(message);
    }

    public UnsupportedOperationException(String message, Throwable cause) {
        super(message, cause);
    }
}

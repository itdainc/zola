package com.itda.zola.api.adaptor.database;

import com.itda.zola.domain.model.notice.Notice;
import com.itda.zola.domain.model.supplier.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NoticeRepository extends JpaRepository<Notice, Long> {

    Notice findFirstBySupplierOrderByIdDesc(Supplier supplier);
}

package com.itda.zola.api.infrastructure.notification;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class ErrorReportingChannel implements NotificationChannel {

    private final RestTemplate restTemplate = new RestTemplate();

    @Value("${zola.notification.error}")
    private boolean errorReport;

    @Override
    public void send(Message message) {

        if (errorReport) {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity<Message> entity = new HttpEntity<>(message, headers);
            restTemplate.postForObject("https://agit.io/webhook/3bd5d02a-c5c5-4d5b-95ae-f5d6aa08fd8d", entity, Void.class);
        }
    }
}

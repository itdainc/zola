package com.itda.zola.api.infrastructure.exception.client;

import com.itda.zola.api.infrastructure.exception.CoreException;

public abstract class AbstractClientException extends CoreException {

    public AbstractClientException(String message) {
        super(message);
    }

    public AbstractClientException(String message, Throwable cause) {
        super(message, cause);
    }
}
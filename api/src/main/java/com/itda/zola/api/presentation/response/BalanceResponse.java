package com.itda.zola.api.presentation.response;

public class BalanceResponse {

    private long usable;

    public BalanceResponse(long usable) {
        this.usable = usable;
    }

    public long getUsable() {
        return usable;
    }
}
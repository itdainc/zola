package com.itda.zola.api.infrastructure.event;

import com.itda.zola.api.adaptor.AgitNotificationChannel;
import com.itda.zola.api.domain.model.JoinReserveParams;
import com.itda.zola.api.domain.service.CpService;
import com.itda.zola.api.domain.service.PromotionService;
import com.itda.zola.api.domain.service.ReserveService;
import com.itda.zola.api.infrastructure.notification.Message;
import com.itda.zola.domain.model.cp.Cp;
import com.itda.zola.domain.model.promotion.Promotion;
import com.itda.zola.domain.model.user.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Slf4j

@Component
public class MyEventListener {

    @Autowired
    private PromotionService PROMOTION_SERVICE;
    @Autowired
    private ReserveService reserveService;
    @Autowired
    private CpService cpService;


    private static final AgitNotificationChannel NOTIFICATION_CHANNEL = new AgitNotificationChannel();
//    @EventListener
//    public void handleUserCreationEvent(UserCreationEvent event) {
//
//        log.info("event[ UserCreationEvent ] occurred.");
//        Promotion promotion = PROMOTION_SERVICE.findOngoingEntryPromotion(event.getSupplier());
//
//        User user = event.getUser();
//
//        if(user.getReferrer() == null) {
//            log.info("no referrer.");
//            return;
//        }
//
//        Cp cp = cpService.find("internal");
//
//
//        reserveService.bonus(new JoinReserveParams(user.getId(), promotion.getAmount(), event.getSupplier().getCode()));
//
//        reserveService.bonus(new JoinReserveParams(user.getReferrer().getId(), promotion.getAmount(), event.getSupplier().getCode()));
//    }


    @EventListener
    public void handleExchangeRequestedEvent(ExchangeRequestedEvent event) {

       log.info("event[ ExchangeRequestedEvent ] occurred.");

       try {
           NOTIFICATION_CHANNEL.send(new Message("새로운 이모티콘 구매신청이 발생했습니다!"));
       } catch (Exception e) {
           log.error("notification failed");
       }
    }
}
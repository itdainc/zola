package com.itda.zola.api.infrastructure.lock;

import com.itda.zola.api.infrastructure.exception.SystemInternalException;
import com.itda.zola.api.infrastructure.lock.user.UserLock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;

@Slf4j
public class RedisResourceLockManager<L extends Lock<String, String>> implements ResourceLockManager<L> {

    private final RedisTemplate<String, String> redis;

    public RedisResourceLockManager(RedisTemplate<String, String> redis) {
        this.redis = redis;
    }


    @Override
    public void acquire(final L lock) {

        log.info("acquire lock = {}", lock);

        boolean acquired = redis.execute(
                (RedisCallback<Boolean>) connection -> {
                    boolean success = connection.setNX(lock.getKey().getBytes(), lock.getValue().getBytes());
                    if (success) {
                        connection.expire(lock.getKey().getBytes(), lock.lockTimeout());
                    }

                    return success;
                }
        );

        if (!acquired) {
            log.error("acquire lock failure");
            throw new SystemInternalException("해당 유저에 대해 진행중인 거래가 있습니다.잠시 후 다시 해 주십시오.");
        }
    }

    @Override
    public void release(final L lock) {

        log.info("release lock = {}", lock);

        redis.execute(
                (RedisCallback<Void>) connection -> {
                    byte[] ctn = connection.get(lock.getKey().getBytes());
                    if (ctn != null && lock.getValue().equals(redis.getStringSerializer().deserialize(ctn))) {
                        connection.del(lock.getKey().getBytes());
                    } else {
                        log.error("release lock failure");
                    }
                    return null;
                }
        );
    }

    //TODO https://redis.io/topics/distlock
    // Redlock 알고리즘 고려해보기
//    private RedissonClient redisson;
//
//    @PostConstruct
//    public void init() {
//
//        Config config = new Config();
//        config.useSingleServer().setAddress("52.78.168.234:6379");
//
//
//        // 2. Create Redisson instance
//        this.redisson = Redisson.create(config);
//    }
}
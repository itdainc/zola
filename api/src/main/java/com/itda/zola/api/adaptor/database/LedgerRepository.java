package com.itda.zola.api.adaptor.database;

import com.itda.zola.domain.model.Total;
import com.itda.zola.domain.model.ledger.Ledger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;

public interface LedgerRepository extends JpaRepository<Ledger, Long> {

    @Query("SELECT COALESCE(SUM(l.price), 0) FROM Ledger l WHERE l.userId = ?1 AND l.regionId = ?2")
    long SumPriceByUserIdAndRegionId(long userId, long regionId);

    List<Ledger> findByUserIdAndRegionIdAndDtGreaterThanOrderByDtDesc(long userId, long regionId, LocalDateTime lowerBound);


    @Query("SELECT new com.itda.zola.domain.model.Total(l.type, SUM(l.amount)) FROM Ledger l WHERE l.userId = ?1 AND l.supplierId = ?2 GROUP BY l.type")
    List<Total> findByA(long user, long supplier);
}
package com.itda.zola.api.domain.service;

import com.itda.zola.api.adaptor.database.SupplierReleaseRepository;
import com.itda.zola.api.adaptor.database.SupplierRepository;
import com.itda.zola.domain.model.supplier.SupplierRelease;
import com.itda.zola.domain.model.supplier.Supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SupplierService {

    private final SupplierRepository supplierRepository;
private final SupplierReleaseRepository supplierReleaseRepository;
    @Autowired
    public SupplierService(SupplierRepository supplierRepository, SupplierReleaseRepository supplierReleaseRepository) {
        this.supplierRepository = supplierRepository;
        this.supplierReleaseRepository = supplierReleaseRepository;
    }

    public Supplier find(long id) {

        return supplierRepository.findOne(id);
    }


    public Supplier find(String code) {

        return supplierRepository.findByCode(code);
    }


    public SupplierRelease findLatestReleaseTag(Supplier supplier) {

        return supplierReleaseRepository.findTopBySupplierOrderByIdDesc(supplier);
    }
}

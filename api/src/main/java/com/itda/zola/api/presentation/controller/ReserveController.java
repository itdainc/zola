package com.itda.zola.api.presentation.controller;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.itda.zola.api.adaptor.adn.*;
import com.itda.zola.api.infrastructure.exception.resource.ResourceDuplicatedException;
import com.itda.zola.api.infrastructure.exception.security.UnauthorizedException;
import com.itda.zola.api.infrastructure.notification.ErrorReportingChannel;
import com.itda.zola.api.infrastructure.notification.Message;
import com.itda.zola.domain.model.cp.Cp;
import com.itda.zola.api.domain.service.CpService;
import com.itda.zola.api.domain.service.ReserveService;
import com.itda.zola.api.presentation.Closed;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

@Slf4j

@RestController
public class ReserveController {

    @Autowired
    private ErrorReportingChannel channel;

    private final CpService cpService;
    private final ReserveService reserveService;

    @Autowired
    public ReserveController(CpService cs, ReserveService rs) {
        this.cpService = cs;
        this.reserveService = rs;
    }


    @Closed(ipFiltering = false)
    @RequestMapping(path = "/CPS/{cpId:tnk}/SAVING/V1", method = RequestMethod.POST)
    public ResponseEntity<Void> reserveForTnk(@PathVariable String cpId, @Valid TnkReserveParams params) {

        params.checkSignature();

        Cp cp = cpService.find(cpId);

        try {
            reserveService.revenueShare(cp, params);
        } catch (ResourceDuplicatedException e ) {
            log.info("이미 적립된 건 입니다. 오류를 무시하고 정상응답을 보냅니다.");
        }

        return ResponseEntity.ok().build();
    }

    @Closed(ipFiltering = false)
    @RequestMapping(path = "/CPS/{cpId:tnk-ios}/SAVING/V1", method = RequestMethod.POST)
    public ResponseEntity<Void> reserveForTnkIos(@PathVariable String cpId, @Valid TnkIosReserveParams params) {

        params.checkSignature();

        Cp cp = cpService.find(cpId);

        try {
            reserveService.revenueShare(cp, params);
        } catch (ResourceDuplicatedException e ) {
            log.info("이미 적립된 건 입니다. 오류를 무시하고 정상응답을 보냅니다.");
        }

        return ResponseEntity.ok().build();
    }

    //❈ 적립금 중복 지급 방지를 위한 처리
    //1. 콜백 중복 호출 시, 적립금 중복 지급을 방지하기 위해, [SEQ_ID]를 기준으로 중복지급을 막아주시기 바랍니다.
    //2. TODO 동일 사용자에게 적립금 중복 지급을 방지하기 위해, 개발사의 회원ID 와 [AD_ID] 값을 기준으로 중복지급을 막아주시기 바랍니다.
    //
    //
    //❈ 콜백 호출 서버의 IP는 192.168.49.171입니다. 어뷰징 방지를 위해, 해당 IP에서 호출된 콜백만 처리해주시기 바랍니다.

    @Closed(ipFiltering = true)
    @RequestMapping(path = "/CPS/{cpId:nas}/RESERVE/V1")
    public ResponseEntity<Void> reserveForNas(@PathVariable String cpId, @Valid NasReserveParams params) {

        Cp cp = cpService.find(cpId);

        try {
            reserveService.revenueShare(cp, params);
        } catch (ResourceDuplicatedException e) {
            log.info("이미 적립된 건 입니다. 오류를 무시하고 정상응답을 보냅니다.");
        }

        return ResponseEntity.ok().build();
    }

    @Closed(ipFiltering = false)
    @RequestMapping(path = "/CPS/{cpId:iga}/RESERVE/V1")
    public ResponseEntity<IgaWorksResponse> reserveForIgaWorks(@PathVariable String cpId, @Valid IgaReserveParams params) {

        try {
            params.checkSignature();
        } catch (UnauthorizedException e) {
            channel.send(new Message(makeStackTrace(e)));
            return ResponseEntity.ok(IgaWorksResponse.invalidSign());
        }

        Cp cp = cpService.find(cpId);

        try {
            reserveService.revenueShare(cp, params);
        } catch (ResourceDuplicatedException e) {
            channel.send(new Message(makeStackTrace(e)));
            return ResponseEntity.ok(IgaWorksResponse.duplicate());
        } catch (Exception e) {
            channel.send(new Message(makeStackTrace(e)));
            return ResponseEntity.ok(IgaWorksResponse.internalError());
        }

        return ResponseEntity.ok(IgaWorksResponse.success());
    }

    @Closed(ipFiltering = false)
    @RequestMapping(path = "/CPS/{cpId:iga-ios}/RESERVE/V1")
    public ResponseEntity<IgaWorksResponse> reserveForIgaWorksIos(@PathVariable String cpId, @Valid IgaIosReserveParams params) {

        try {
            params.checkSignature();
        } catch (UnauthorizedException e) {
            channel.send(new Message(makeStackTrace(e)));
            return ResponseEntity.ok(IgaWorksResponse.invalidSign());
        }

        Cp cp = cpService.find(cpId);

        try {
            reserveService.revenueShare(cp, params);
        } catch (ResourceDuplicatedException e) {
            channel.send(new Message(makeStackTrace(e)));
            return ResponseEntity.ok(IgaWorksResponse.duplicate());
        } catch (Exception e) {
            channel.send(new Message(makeStackTrace(e)));
            return ResponseEntity.ok(IgaWorksResponse.internalError());
        }

        return ResponseEntity.ok(IgaWorksResponse.success());
    }

    @Getter
    private static class IgaWorksResponse {

        @JsonProperty("Result")
        private boolean Result;
        @JsonProperty("ResultCode")
        private int ResultCode;
        @JsonProperty("ResultMsg")
        private String ResultMsg;

        IgaWorksResponse(boolean result, int resultCode, String resultMsg) {
            Result = result;
            ResultCode = resultCode;
            ResultMsg = resultMsg;
        }

        static IgaWorksResponse success() {
            return new IgaWorksResponse(true, 1, "success");
        }

        static IgaWorksResponse invalidSign() {
            return new IgaWorksResponse(false, 1100, "invalid signed value");
        }

        static IgaWorksResponse duplicate() {
            return new IgaWorksResponse(false, 3100, "duplicate transaction");
        }

        static IgaWorksResponse invalidUser() {
            return new IgaWorksResponse(false, 3200, "invalid user");
        }

        static IgaWorksResponse internalError() {
            return new IgaWorksResponse(false, 4000, "internal error");
        }
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Void> handleException(Exception e) {
        log.error("handle exception : ", e);
        channel.send(new Message(makeStackTrace(e)));
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    public static String makeStackTrace(Throwable t){
        if(t == null) return "";
        try{
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            t.printStackTrace(new PrintStream(bout));
            bout.flush();
            return new String(bout.toByteArray());
        }catch(Exception ex){
            return "";
        }
    }

}
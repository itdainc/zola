package com.itda.zola.api.infrastructure.exception.security;

public class UnauthorizedException extends AbstractSecurityException {

    public UnauthorizedException(String message) {
        super(message);
    }

    public UnauthorizedException(String message, Throwable cause) {
        super(message, cause);
    }
}
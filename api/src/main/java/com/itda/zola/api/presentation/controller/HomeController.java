package com.itda.zola.api.presentation.controller;

import com.itda.zola.api.domain.service.UserService;
import com.itda.zola.api.presentation.OpenApi;
import com.itda.zola.api.presentation.response.BalanceResponse;
import com.itda.zola.api.presentation.response.HistoryResponse;
import com.itda.zola.domain.model.ledger.Ledger;
import com.itda.zola.domain.model.ledger.LedgerType;
import com.itda.zola.domain.model.supplier.Supplier;
import com.itda.zola.domain.model.user.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j

@RestController
public class HomeController {

    private final UserService userService;

    @Autowired
    public HomeController(UserService userService) {
        this.userService = userService;
    }

    /**
     * 잔액 조회
     *
     * @return BalanceResponse
     */
//    @OpenApi
    @RequestMapping(path = "/SUPPLIER/{sId}/USER/{userId:\\d+}/BALANCE/V1", method = RequestMethod.GET)
    public ResponseEntity<BalanceResponse> getBalance(Supplier supplier, User user) {

        long balance = userService.getBalance(user, supplier.getRegion());

        return ResponseEntity.ok(new BalanceResponse(balance));
    }

    /**
     * 누적 거래금액 조회
     *
     * @return Map of LedgerType, Long
     */
//    @OpenApi
    @RequestMapping(path = "/SUPPLIER/{sId}/USER/{userId:\\d+}/TOTAL/V1", method = RequestMethod.GET)
    public ResponseEntity<Map<LedgerType, Long>> calculateTotal(Supplier supplier, User user) {

        return ResponseEntity.ok(userService.getTotal(user, supplier));
    }

    /**
     * 이용내역 조회
     *
     * @return List of HistoryResponse
     */
//    @OpenApi
    @RequestMapping(path = "/SUPPLIER/{sId}/USER/{userId:\\d+}/HISTORY/V1", method = RequestMethod.GET)
    public ResponseEntity<List<HistoryResponse>> getHistory(Supplier supplier, User user) {

        List<Ledger> ledgers = userService.getLedgers(user, supplier.getRegion());

        return ResponseEntity.ok(ledgers.stream().map(HistoryResponse::new).collect(Collectors.toList()));
    }
}
package com.itda.zola.api.presentation;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Closed {

    boolean ipFiltering();
}
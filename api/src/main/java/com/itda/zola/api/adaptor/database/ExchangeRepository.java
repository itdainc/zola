package com.itda.zola.api.adaptor.database;

import com.itda.zola.domain.model.supplier.Supplier;
import com.itda.zola.domain.model.transaction.exchange.Exchange;
import com.itda.zola.domain.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ExchangeRepository extends JpaRepository<Exchange, Long> {

    List<Exchange> findBySupplierAndUserAndStatus(Supplier supplier, User user, Exchange.Status status);
}
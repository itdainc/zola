package com.itda.zola.api.domain.service;

import com.itda.zola.api.adaptor.database.RegionRepository;
import com.itda.zola.domain.model.user.Region;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RegionService {

    private final RegionRepository regionRepository;

    @Autowired
    public RegionService(RegionRepository regionRepository) {
        this.regionRepository = regionRepository;
    }

    public Region find(String code) {
        return regionRepository.findByCode(code);
    }
}
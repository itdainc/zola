package com.itda.zola.api.adaptor.database;

import com.itda.zola.domain.model.user.ProviderToken;
import com.itda.zola.domain.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByIdAndExpiredAtIsNull(long id);

    User findByProviderTokenAndExpiredAtIsNull(ProviderToken providerToken);

    User findByNameAndExpiredAtIsNull(String name);

    User findTopByProviderTokenOrderByRegisteredAtDesc(ProviderToken token);

    Optional<User> findByName(String name);
}
package com.itda.zola.api.domain.service;

import com.itda.zola.api.adaptor.database.LedgerRepository;
import com.itda.zola.domain.model.Total;
import com.itda.zola.domain.model.ledger.Ledger;
import com.itda.zola.domain.model.ledger.LedgerType;
import com.itda.zola.domain.model.supplier.Supplier;
import com.itda.zola.domain.model.user.Region;
import com.itda.zola.domain.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class LedgerService {

    private static final int HISTORY_EXPOSE_DAYS = 7;
    private final LedgerRepository ledgerRepository;

    @Autowired
    public LedgerService(LedgerRepository ledgerRepository) {
        this.ledgerRepository = ledgerRepository;
    }

    public Long calculateBalance(User user, Region region) {
        return ledgerRepository.SumPriceByUserIdAndRegionId(user.getId(), region.getId());
    }

    public List<Ledger> getLedgers(User user, Region region) {
        return ledgerRepository.findByUserIdAndRegionIdAndDtGreaterThanOrderByDtDesc(user.getId(), region.getId(), LocalDateTime.now().minusDays(HISTORY_EXPOSE_DAYS).truncatedTo(ChronoUnit.DAYS));
    }

    public Map<LedgerType, Long> getTotal(User user, Supplier supplier) {

        List<Total> totals = ledgerRepository.findByA(user.getId(), supplier.getId());

        Map<LedgerType, Long> ret = new HashMap<>();
        for (LedgerType t : LedgerType.values()) {
            ret.put(t, 0L);
        }

        totals.forEach(total -> ret.put(total.getType(), total.getAmount()));

        return ret;
    }
}

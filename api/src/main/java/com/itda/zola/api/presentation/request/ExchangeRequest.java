package com.itda.zola.api.presentation.request;

import com.itda.zola.domain.model.transaction.exchange.Exchangeable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExchangeRequest {

    private Exchangeable.Type type;
    private String name;
    private Long price;

    private String receiver;
    private String url;
}

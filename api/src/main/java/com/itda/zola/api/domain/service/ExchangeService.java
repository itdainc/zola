package com.itda.zola.api.domain.service;

import com.itda.zola.api.adaptor.database.ExchangeRepository;
import com.itda.zola.api.infrastructure.event.ExchangeRequestedEvent;
import com.itda.zola.api.infrastructure.exception.client.BadRequestException;
import com.itda.zola.domain.model.supplier.Supplier;
import com.itda.zola.domain.model.transaction.exchange.Exchange;
import com.itda.zola.domain.model.transaction.exchange.Exchangeable;
import com.itda.zola.domain.model.user.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Slf4j

@Service
public class ExchangeService {

    private final ExchangeRepository exchangeRepository;
    private final UserService userService;
    private final ApplicationEventPublisher PUBLISHER;

    @Autowired
    public ExchangeService(ExchangeRepository er, UserService us, ApplicationEventPublisher aep) {
        this.exchangeRepository = er;
        this.userService = us;
        this.PUBLISHER = aep;
    }


    @Transactional
    public void exchange(Supplier supplier, User user, Exchangeable exchangeable, String receiver, String url) {

        Long balance = userService.getBalance(user, supplier.getRegion());

        if (exchangeable.getPrice() > balance) {
            log.error("not enough balance. user = {}", user.getName());
            throw new BadRequestException("캐시가 부족하여 구매 신청을 할 수 없습니다.");
        }

        log.info("attempt to exchange.");

        Exchange exchange = new Exchange(supplier, user, supplier.getRegion(), exchangeable, receiver, url);

        exchangeRepository.save(exchange);

        log.info("succeed. publish a event");

        PUBLISHER.publishEvent(new ExchangeRequestedEvent());
    }

    @Transactional
    public List<Exchange> findPendingExchange(Supplier supplier, User user) {

        return exchangeRepository.findBySupplierAndUserAndStatus(supplier, user, Exchange.Status.REGISTERED);
    }
}
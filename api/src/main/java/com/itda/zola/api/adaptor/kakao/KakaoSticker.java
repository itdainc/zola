package com.itda.zola.api.adaptor.kakao;

import com.google.common.base.MoreObjects;
import com.itda.zola.domain.model.transaction.exchange.Exchangeable;

public class KakaoSticker implements Exchangeable {

    private String name;
    private Long price;

    private String image;


    public KakaoSticker(String name, Long price, String image) {
        this(name, price);
        this.image = image;
    }

    public KakaoSticker(String name, Long price) {
        this.name = name;
        this.price = price;
    }


    // ----------

    @Override
    public Type getType() {
        return Type.KAKAO_EMO;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Long getPrice() {
        return price;
    }

    public String getImage() {
        return image;
    }


    // ----------

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", name)
                .add("price", price)
                .add("image", image)
                .toString();
    }
}
package com.itda.zola.api.presentation.response;

import com.itda.zola.domain.model.transaction.exchange.Exchangeable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PreExchangeResponse {

    private Exchangeable item;
    private BalanceResponse balance;
    private boolean payable;


    public PreExchangeResponse(Exchangeable exchangeable, Long balance) {
        this.item = exchangeable;
        this.payable = exchangeable.getPrice() < balance;

        this.balance = new BalanceResponse(balance);
    }
}

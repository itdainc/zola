package com.itda.zola.api.infrastructure.notification;

public interface NotificationChannel {

    void send(Message message);
}
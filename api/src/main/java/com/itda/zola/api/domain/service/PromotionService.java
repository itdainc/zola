package com.itda.zola.api.domain.service;

import com.itda.zola.api.adaptor.database.PromotionRepository;
import com.itda.zola.api.domain.model.AttendanceReserveParams;
import com.itda.zola.api.domain.model.JoinReserveParams;
import com.itda.zola.api.domain.model.RecommendedReserveParams;
import com.itda.zola.api.infrastructure.exception.resource.ResourceNotFoundException;
import com.itda.zola.domain.model.promotion.Promotion;
import com.itda.zola.domain.model.promotion.PromotionType;
import com.itda.zola.domain.model.supplier.Supplier;
import com.itda.zola.domain.model.transaction.reserve.ReserveParams;
import com.itda.zola.domain.model.user.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Slf4j

@Service
public class PromotionService {

    private final PromotionRepository PROMOTION_REPOSITORY;
    private final ReserveService reserveService;

    @Autowired
    public PromotionService(PromotionRepository pr, ReserveService rr) {
        this.PROMOTION_REPOSITORY = pr;
        this.reserveService = rr;
    }


    public Promotion findOngoingAttendancePromotion(Supplier supplier) {

        log.info("find ongoing attendance promotion.");

        Promotion promotion = PROMOTION_REPOSITORY.findOngoingPromotionBySupplierAndType(supplier, PromotionType.ATTENDANCE);
        if (promotion == null) {
            log.warn("there is no ongoing attendance promotion.");
            throw new ResourceNotFoundException("there is no ongoing attendance promotion.");
        }

        log.info("promotion = {}", promotion);

        return promotion;
    }

    public Promotion findOngoingEntryPromotion(Supplier supplier) {

        log.info("find ongoing entry promotion.");

        Promotion promotion = PROMOTION_REPOSITORY.findOngoingPromotionBySupplierAndType(supplier, PromotionType.JOIN);
        if (promotion == null) {
            log.warn("there is no ongoing entry promotion.");
            throw new ResourceNotFoundException("there is no ongoing entry promotion.");
        }

        log.info("promotion = {}", promotion);

        return promotion;
    }

    public Promotion findOngoingReferrerPromotion(Supplier supplier) {

        log.info("find ongoing referrer promotion.");

        Promotion promotion = PROMOTION_REPOSITORY.findOngoingPromotionBySupplierAndType(supplier, PromotionType.REFERRER);
        if (promotion == null) {
            log.warn("there is no ongoing entry promotion.");
            throw new ResourceNotFoundException("there is no ongoing entry promotion.");
        }

        log.info("promotion = {}", promotion);

        return promotion;
    }

    public List<AppliedPromotion> promote(Supplier supplier, User user, PromotionType... types) {

        List<AppliedPromotion> appliedPromotions = new ArrayList<>(types.length);

        for (PromotionType type : types) {
                appliedPromotions.add(  promote(supplier, user, type));
        }

        return appliedPromotions;
    }

    @Transactional
    public AppliedPromotion promote(Supplier supplier, User user, PromotionType type) {

        Promotion promotion = PROMOTION_REPOSITORY.findOngoingPromotionBySupplierAndType(supplier, type);

        if(promotion == null) {
            return AppliedPromotion.NONE;
        }

        ReserveParams params;

        if(type == PromotionType.JOIN) {
            params = new JoinReserveParams(user.getId(), promotion.getAmount(), supplier.getCode());
        } else if(type == PromotionType.ATTENDANCE) {
            params = AttendanceReserveParams.of(user.getId(), supplier.getCode(), promotion.getAmount());
        } else if(type == PromotionType.REFERRER) {
            params = new RecommendedReserveParams(user.getId(), promotion.getAmount(), supplier.getCode(), "");

        }
//
//        reserveService.bonus(cpService.find("internal"), params);
//
//        } catch (ResourceNotFoundException e) {
//            log.warn("ignoring exception.");
//          }
    return null;


    }

    public static class AppliedPromotion {

        public static final AppliedPromotion NONE = null;

        private String name;
        private String amount;

    }
}
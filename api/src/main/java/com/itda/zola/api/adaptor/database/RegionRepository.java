package com.itda.zola.api.adaptor.database;

import com.itda.zola.domain.model.user.Region;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RegionRepository extends JpaRepository<Region, Long> {

    Region findByCode(String code);
}
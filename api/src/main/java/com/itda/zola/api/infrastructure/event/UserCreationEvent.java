package com.itda.zola.api.infrastructure.event;

import com.itda.zola.domain.model.supplier.Supplier;
import com.itda.zola.domain.model.user.User;
import lombok.Getter;

@Getter
public class UserCreationEvent {

   private User user;
   private Supplier supplier;

    public UserCreationEvent(User user, Supplier supplier) {
        this.user = user;
        this.supplier = supplier;
    }
}

package com.itda.zola.api.presentation.response;

import com.itda.zola.domain.model.user.User;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor

@Getter
public class JoinResponse {

    private String welcomeMessage;

    private UserResponse user;


    public JoinResponse(String welcomeMessage, User user) {
        this.welcomeMessage = welcomeMessage;
        this.user = new UserResponse(user);
    }
}

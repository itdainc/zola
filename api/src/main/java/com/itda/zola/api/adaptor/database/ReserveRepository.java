package com.itda.zola.api.adaptor.database;

import com.itda.zola.domain.model.transaction.reserve.Reserve;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReserveRepository extends JpaRepository<Reserve, Long> {

}
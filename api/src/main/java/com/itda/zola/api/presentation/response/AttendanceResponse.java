package com.itda.zola.api.presentation.response;

import com.itda.zola.domain.model.transaction.reserve.Reserve;
import lombok.Getter;

@Getter
public class AttendanceResponse {

    private long amount;

    public AttendanceResponse(Reserve reserve) {

        this.amount = reserve.getAmount();
    }
}

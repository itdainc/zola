package com.itda.zola.api.presentation.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
public class SupplierInitResponse {

    private int currentVersion;
    private boolean latestVersion;

    private boolean forceUpdateRequired;

//    private List<String> notableChanges;


    public SupplierInitResponse(boolean latestVersion, boolean forceUpdateRequired,int currentVersion) {
        this.currentVersion = currentVersion;
        this.latestVersion = latestVersion;
        this.forceUpdateRequired = forceUpdateRequired;
    }
}
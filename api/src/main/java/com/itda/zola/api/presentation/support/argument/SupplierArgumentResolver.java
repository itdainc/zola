package com.itda.zola.api.presentation.support.argument;

import com.google.common.base.Strings;
import com.itda.zola.api.domain.service.SupplierService;
import com.itda.zola.api.infrastructure.exception.client.BadRequestException;
import com.itda.zola.api.infrastructure.exception.resource.ResourceNotFoundException;
import com.itda.zola.api.infrastructure.util.HttpUtil;
import com.itda.zola.domain.model.supplier.Supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.servlet.http.HttpServletRequest;

@Component
public class SupplierArgumentResolver implements HandlerMethodArgumentResolver {

    private final SupplierService supplierService;

    @Autowired
    public SupplierArgumentResolver(SupplierService supplierService) {
        this.supplierService = supplierService;
    }

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return Supplier.class.isAssignableFrom(parameter.getParameterType());
    }

    @Override
    public Supplier resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {

        HttpServletRequest httpServletRequest = webRequest.getNativeRequest(HttpServletRequest.class);

        String supplierCode = HttpUtil.getPathVariable(httpServletRequest, "sId");

        if (Strings.isNullOrEmpty(supplierCode)) {
            throw new BadRequestException("sId 값이 잘못되었습니다.");
        }

        try {
            return supplierService.find(supplierCode);
        } catch (ResourceNotFoundException e) {
            throw new BadRequestException(e.getMessage());
        }
    }
}
package com.itda.zola.api.presentation.support.argument;

import com.google.common.base.Strings;
import com.itda.zola.api.domain.service.UserService;
import com.itda.zola.api.infrastructure.exception.client.BadRequestException;
import com.itda.zola.api.infrastructure.exception.resource.ResourceNotFoundException;
import com.itda.zola.api.infrastructure.util.HttpUtil;
import com.itda.zola.domain.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.servlet.http.HttpServletRequest;

@Component
public class UserArgumentResolver implements HandlerMethodArgumentResolver {

    private final UserService userService;

    @Autowired
    public UserArgumentResolver(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return User.class.isAssignableFrom(parameter.getParameterType());
    }

    @Override
    public User resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {

        HttpServletRequest httpServletRequest = webRequest.getNativeRequest(HttpServletRequest.class);

        String userId = HttpUtil.getPathVariable(httpServletRequest, "userId");

        if (Strings.isNullOrEmpty(userId)) {
            throw new BadRequestException("userId 값이 잘못되었습니다.");
        }

        try {
            return userService.findActiveUser(Long.valueOf(userId));
        } catch (ResourceNotFoundException e) {
            throw new BadRequestException(e.getMessage());
        }
    }
}

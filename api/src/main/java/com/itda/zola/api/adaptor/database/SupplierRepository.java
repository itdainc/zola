package com.itda.zola.api.adaptor.database;

import com.itda.zola.domain.model.supplier.SupplierRelease;
import com.itda.zola.domain.model.supplier.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface SupplierRepository extends JpaRepository<Supplier, Long> {

    Supplier findByCode(String code);

    @Query("SELECT rt FROM SupplierRelease rt WHERE rt.supplier = ?1")
    SupplierRelease findReleaseTag(Supplier supplier);

}
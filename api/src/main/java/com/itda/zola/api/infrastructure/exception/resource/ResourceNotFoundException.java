package com.itda.zola.api.infrastructure.exception.resource;

public class ResourceNotFoundException extends AbstractResourceException {

    public ResourceNotFoundException(String message) {
        super(message);
    }

    public ResourceNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
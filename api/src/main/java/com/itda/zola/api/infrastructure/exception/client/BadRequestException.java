package com.itda.zola.api.infrastructure.exception.client;

public class BadRequestException extends AbstractClientException {

    public BadRequestException(String message) {
        super(message);
    }

    public BadRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
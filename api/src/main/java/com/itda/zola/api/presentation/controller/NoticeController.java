package com.itda.zola.api.presentation.controller;

import com.itda.zola.api.domain.service.NoticeService;
import com.itda.zola.api.presentation.OpenApi;
import com.itda.zola.api.presentation.response.NoticeResponse;
import com.itda.zola.domain.model.supplier.Supplier;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j

@RestController
public class NoticeController {

    private final NoticeService noticeService;

    @Autowired
    public NoticeController(NoticeService noticeService) {
        this.noticeService = noticeService;
    }

    /**
     * 공지사항 조회
     *
     * @return NoticeResponse
     */
    @OpenApi
    @RequestMapping(path = "/SUPPLIER/{sId}/NOTICES/LATEST/V1", method = RequestMethod.GET)
    public ResponseEntity<NoticeResponse> getNotice(Supplier supplier) {

        return ResponseEntity.ok(new NoticeResponse(noticeService.findLatest(supplier)));
    }
}
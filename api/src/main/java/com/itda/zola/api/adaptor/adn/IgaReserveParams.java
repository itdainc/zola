package com.itda.zola.api.adaptor.adn;

import com.google.common.base.Charsets;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;
import com.itda.zola.api.infrastructure.exception.security.UnauthorizedException;
import com.itda.zola.domain.model.ledger.Account;
import com.itda.zola.domain.model.transaction.reserve.ReserveParams;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

@Slf4j

@Setter
public class IgaReserveParams implements ReserveParams {

    private static final String HASH_KEY = "b15a7ebf9e3b4f87";

    @NotBlank
    private String reward_key;

    @NotBlank
    private String usn; // userId:supplier

    @NotNull
    private Long quantity;

    private String signed_value; // signature
    private String campaign_key;
    private String campaign_name;


    // ----------

    @Override
    public Long getAmount() {
        return quantity;
    }

    @Override
    public Long getUserId() {
        return Long.valueOf(usn.split(":")[0]);
    }

    @Override
    public String getSupplierCode() {
        return usn.split(":")[1];
    }

    @Override
    public Account getAccount() {
        return Account.R_RS;
    }

    @Override
    public String getTransactionId() {
        return reward_key;
    }

    @Override
    public String getMemo() {
        return campaign_name;
    }

    public void checkSignature() {

        String verifyCode = generateHash(usn + reward_key + quantity + campaign_key, HASH_KEY);

        log.info("signature. iga : {}, gen : {}", signed_value, verifyCode);

        if (signed_value == null || !signed_value.equals(verifyCode)) {
            // 오류
            log.error("iga-works() check error : " + verifyCode + " != " + signed_value);
            throw new UnauthorizedException("signature failure");
        }

        log.info("signature success");
    }

    private static String generateHash(String string, String key) {
        HashCode hc = Hashing.hmacMd5(key.getBytes()).newHasher().putString(string, Charsets.UTF_8).hash();
        return hc.toString();
    }
}
package com.itda.zola.api.infrastructure.lock.user;

import com.google.common.base.MoreObjects;
import com.itda.zola.api.infrastructure.lock.Lock;
import lombok.Getter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Getter
public class UserLock implements Lock<String, String> {

    private static final DateTimeFormatter FORMAT = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
    private static final int LOCK_TIMEOUT = 600;

    private final LocalDateTime dt;

    private final String key;
    private final String value;


    public UserLock(String supplierCode, Long userId) {

        this.dt = LocalDateTime.now();

        this.key = String.format("%s::%d", supplierCode, userId);
        this.value = dt.format(FORMAT);
    }


    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public int lockTimeout() {
        return LOCK_TIMEOUT;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("dt", dt)
                .add("key", key)
                .add("value", value)
                .toString();
    }
}
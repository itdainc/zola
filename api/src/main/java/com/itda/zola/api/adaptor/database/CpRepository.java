package com.itda.zola.api.adaptor.database;

import com.itda.zola.domain.model.cp.Cp;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CpRepository extends JpaRepository<Cp, Long> {

    Optional<Cp> findByCode(String code);
}
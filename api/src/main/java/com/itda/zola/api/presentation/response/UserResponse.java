package com.itda.zola.api.presentation.response;

import com.itda.zola.domain.model.user.User;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


@NoArgsConstructor

@Getter
public class UserResponse {

    private long id;
    private String hash;
    private String name;
    private LocalDateTime registeredAt;

    private boolean isNewUser;


    public UserResponse(User user) {
        this(user, false);
    }

    public UserResponse(User user, boolean isNewUser) {
        this.id = user.getId();
        this.hash = user.getHash();
        this.name = user.getName();
        this.registeredAt = user.getRegisteredAt();

        this.isNewUser = isNewUser;
    }
}
package com.itda.zola.api.domain.model;

import com.itda.zola.domain.model.ledger.Account;
import com.itda.zola.domain.model.transaction.reserve.ReserveParams;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class RecommendedReserveParams implements ReserveParams {

    private Long amount;

    private Long userId;
    private String supplierCode;

    private String transactionId;

    private String memo;

    public RecommendedReserveParams(Long userId, Long amount, String supplierCode, String referrerName) {
        this.userId = userId;
        this.amount = amount;
        this.supplierCode = supplierCode;
        this.transactionId = "rcmded-" + userId + "-" + referrerName;

        this.memo = "추천인 등록(" + referrerName + ")";
    }

    @Override
    public Long getAmount() {
        return amount;
    }

    @Override
    public Long getUserId() {
        return userId;
    }

    @Override
    public String getSupplierCode() {
        return supplierCode;
    }

    @Override
    public Account getAccount() {
        return Account.R_RECM;
    }

    @Override
    public String getTransactionId() {
        return transactionId;
    }

    @Override
    public String getMemo() {
        return memo;
    }
}

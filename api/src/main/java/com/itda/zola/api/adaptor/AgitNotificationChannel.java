package com.itda.zola.api.adaptor;

import com.itda.zola.api.infrastructure.notification.Message;
import com.itda.zola.api.infrastructure.notification.NotificationChannel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

public class AgitNotificationChannel implements NotificationChannel{

    private RestTemplate restTemplate;

    public AgitNotificationChannel() {

        restTemplate = new RestTemplate();

    }

    @Override
    public void send(Message message) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<Message> entity = new HttpEntity<>(message, headers);
        restTemplate.postForObject("https://agit.io/webhook/2276973d-c68d-430f-a58a-707952504e4c", entity, Void.class);
    }
}
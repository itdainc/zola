package com.itda.zola.api.infrastructure.exception;

public class SystemInternalException extends CoreException {

    public SystemInternalException(String message) {
        super(message);
    }

    public SystemInternalException(String message, Throwable cause) {
        super(message, cause);
    }
}

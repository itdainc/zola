package com.itda.zola.api.presentation.controller;

import com.google.common.base.Strings;
import com.itda.zola.api.domain.model.JoinReserveParams;
import com.itda.zola.api.domain.model.RecommendedReserveParams;
import com.itda.zola.api.domain.model.RecommenderReserveParams;
import com.itda.zola.api.domain.model.UserJoinParams;
import com.itda.zola.api.domain.service.*;
import com.itda.zola.api.infrastructure.exception.resource.ResourceNotFoundException;
import com.itda.zola.api.presentation.OpenApi;
import com.itda.zola.api.presentation.request.JoinRequest;
import com.itda.zola.api.presentation.request.LoginRequest;
import com.itda.zola.api.presentation.response.JoinResponse;
import com.itda.zola.api.presentation.response.NameCheckResponse;
import com.itda.zola.api.presentation.response.UserResponse;
import com.itda.zola.domain.model.promotion.Promotion;
import com.itda.zola.domain.model.supplier.Supplier;
import com.itda.zola.domain.model.user.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j

@RestController
public class UserController {

    private final UserService userService;

    private final ReserveService reserveService;
    private final CpService cpService;
    private final PromotionService promotionService;

    @Autowired
    public UserController(UserService userService, CpService cpService, ReserveService reserveService, PromotionService ps) {
        this.userService = userService;
        this.reserveService = reserveService;
        this.cpService = cpService;
        this.promotionService = ps;
    }

    /**
     * 이름 중복 확인
     *
     * @return UserResponse
     */
    @OpenApi
    @RequestMapping(path = "/SUPPLIER/{sId}/CHECK-NAME/V1", method = RequestMethod.GET)
    public ResponseEntity<NameCheckResponse> checkName(@RequestParam String name) {

        if (!name.matches("[\\dA-Za-z]{4,16}")) {
            return ResponseEntity.ok(NameCheckResponse.notUsable("4~16자의 영문, 숫자를 입력하세요."));
        }

        if (userService.exist(name)) {
            return ResponseEntity.ok(NameCheckResponse.notUsable("이미 사용중인 이름입니다."));
        }

        return ResponseEntity.ok(NameCheckResponse.ok());
    }

    /**
     * 가입여부 확인
     *
     * @return UserResponse
     */
    @OpenApi
    @RequestMapping(path = "/SUPPLIER/{sId}/CHECK-AUTH/V1", method = RequestMethod.POST)
    public ResponseEntity<UserResponse> checkAuthenticate(@Valid @RequestBody LoginRequest request) {

        try {
            User user = userService.findActiveUser(request.getProviderToken());
            if (!request.getFirebaseToken().equals(user.getFirebaseToken())) {
                user.setFirebaseToken(request.getFirebaseToken());
                userService.update(user);
            }
            return ResponseEntity.ok(new UserResponse(user));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    /**
     * 가입
     *
     * @return UserResponse
     */
    @OpenApi
    @RequestMapping(path = "/SUPPLIER/{sId}/JOIN/V1", method = RequestMethod.POST)
    public ResponseEntity<JoinResponse> join(Supplier supplier, @RequestBody @Valid JoinRequest request) {

        UserJoinParams params = new UserJoinParams(request.getProviderToken(), request.getName(), supplier, request.getFirebaseToken());

        // setting referrer
        if (!Strings.isNullOrEmpty(request.getReferrer())) {
            try {
                params.setReferrer(userService.findActiveUser(request.getReferrer()));
            } catch (ResourceNotFoundException e) {
                log.warn("referrer not found. name={} ignore exception.", request.getReferrer());
            }
        }

        User user = userService.join(params);



        String welcomeMessage = "회원가입에 감사 드립니다.\n";

        //fixme 코드정리...
        Promotion entryPromotion;
        try {
            entryPromotion = promotionService.findOngoingEntryPromotion(supplier);
            reserveService.bonus(new JoinReserveParams(user.getId(), entryPromotion.getAmount(), supplier.getCode()));
            welcomeMessage += "회원가입 보너스 : " + entryPromotion.getAmount() + "\n";
        } catch (ResourceNotFoundException e) {
            log.warn("ignoring exception.");
        }

        Promotion referrerPromotion;
        try {
            referrerPromotion = promotionService.findOngoingReferrerPromotion(supplier);

            // 신규가입자에게 적립
            if (!Strings.isNullOrEmpty(request.getReferrer()) && !request.getName().equals(request.getReferrer())) {
                if (user.getReferrer() == null) {
                    log.warn("exceptional case. wrong referrer, reserve begin user only");
                }
                reserveService.bonus(new RecommendedReserveParams(user.getId(), referrerPromotion.getAmount(), supplier.getCode(), request.getReferrer()));
                welcomeMessage += "추천인 등록 보너스 : " + referrerPromotion.getAmount();
            } else {
                log.error("wrong referral");
            }

            // 추천인 적립
            if (user.getReferrer() != null) {
                reserveService.bonus(new RecommenderReserveParams(user.getReferrer().getId(), referrerPromotion.getAmount2(), supplier.getCode(), user.getId(), user.getName()));
            }
        } catch (ResourceNotFoundException e) {
            log.warn("ignoring exception.");
        }

        return ResponseEntity.ok(new JoinResponse(welcomeMessage, user));
    }

    /**
     * 탈퇴
     *
     * @return Void
     */
    @OpenApi
    @RequestMapping(path = "/SUPPLIER/{sId}/USER/{userId:\\d+}/LEAVE/V1", method = RequestMethod.POST)
    public ResponseEntity<Void> leave(@PathVariable String sId, @PathVariable Long userId) {

        userService.leave(userId);

        return ResponseEntity.ok().build();
    }
}
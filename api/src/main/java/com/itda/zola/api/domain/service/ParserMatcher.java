package com.itda.zola.api.domain.service;

import com.itda.zola.api.adaptor.kakao.KakaoStickerExchangeableParser;
import com.itda.zola.api.infrastructure.exception.client.UnsupportedOperationException;
import com.itda.zola.domain.service.ExchangeableParser;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class ParserMatcher {

    private static final List<ExchangeableParser> PARSERS = new ArrayList<>();

    static {
        PARSERS.add(new KakaoStickerExchangeableParser());
    }

    public static ExchangeableParser match(final String url) {

        log.info("parsing item = {}", url);

        for (ExchangeableParser parser : PARSERS) {
            if (parser.support(url)) {
                log.info("matched parser = {}", parser);
                return parser;
            }
        }

        // 스트림 으로 구현
//        Optional<ExchangeableParser> matched = PARSERS.stream().filter(parser -> parser.support(url)).findFirst();

//        if(!matched.isPresent()) {
//            log.error("");
//            throw new UnsupportedOperationException("unsupported pattern");
//        }


        log.error("unsupported pattern");
        throw new UnsupportedOperationException("unsupported pattern");
    }

    public static void addParser(ExchangeableParser parser) {
        PARSERS.add(parser);
    }
}